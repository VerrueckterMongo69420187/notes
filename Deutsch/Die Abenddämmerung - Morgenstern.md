---
Fach: Deutsch
Datum: Tuesday, 10.09.2024
Zeit: 09:47
Themen: https://www.gedichte7.de/abenddaemmerung-morgenstern.html
Seiten:
---
---
**Reimart:** Kreuzreim, Paarreim, ab und zu gar nicht
**Verse:** 64
**Strophen:** 9 (3 im 1. Teil, 6 im 2. Teil)
**Zeilen pro Strophe:** 
1. Teil: 6 pro Strophe
2. Teil: Unterschiedlich, zwischen 5-11 Zeilen pro Strophe

**Thematik:** Übergang vom Tag zur Nacht in zwei Teilen beschrieben. Im ersten Teil wird die Dämmerung als ein zögerlicher Moment beschrieben, in dem die Natur zwischen Wachsein und Schlafen schwebt, während die Nacht langsam beginnt. Im zweiten Teil wird die Dämmerung als alte Frau beschrieben, die das letzte Licht einsammelt. Dieses Licht verwandelt ihre Tochter, die als Nacht dargestellt wird, in Sterne und den Mond.
**Rhetorische Stilmittel:**
Anapher (2-2,3 3-2,3)
Metapher: die "großen Augen" -> der Himmel 
Alliteration: "sammelt und sammelt" -> wichtig für fließenden Rhythmus des Gedichts
**Rythmus:** unregelmäßig
**Wörter pro Zeile**: fast immer 5 pro Zeile


# Gedicht
### I.
Wie lang nun schon die Dämmrung wieder  
Die grossen Augen offen hält!
Ich fühle fast die schweren Lider,  
Die halber Schlummer schon befällt -  
Und doch, trotz allem Drosselschlag,  
Noch nicht zum Sinken bringen mag.
**Dämmerung zögert**

Du zagst, den Leuchter zu entzünden,  
Und wartest, grossgeäugt, auch du,  
Und starrst, wie sie, den bleichen Gründen,  
Darin ein Weltall schlummert, zu,  
Darin Besonnte mit Besonnern  
In noch verborgnem Taumel donnern.
**Die Sterne sind noch nicht zu sehen**

Doch bald ergrünt ein Stern im Hellen,  
**Nacht beginnt**
Und langsam lischt ihr Wachen hin ..  
Und hunderttausend Früchte schwellen  
**immer mehr Sterne tauchen auf**
Zu Häupten einer Träumerin:  
Als hätte sie ihr Blick gereift -  
Die Fülle, die kein Mass begreift.

### II. (In Phanta’s Schloß)

Eine runzelige Alte,  
schleicht die Abenddämmerung,  
gebückten Ganges  
durchs Gefild  
und sammelt und sammelt  
das letzte Licht  
in ihre Schürze.
**Dame sammelt das Licht == Dämmerung setzt ein. Heißt Dame == Dämmerung**

Vom Wiesenrain,  
von den Hüttendächern,  
von den Stämmen des Walds,  
nimmt sie es fort.  
Und dann  
humpelt sie mühsam  
den Berg hinauf  
und sammelt und sammelt  
die letzte Sonne  
in ihre Schürze.

Droben umschlingt ihr  
mit Halsen und Küssen  
ihr Töchterchen Nacht  
den Nacken  
und greift begierig  
ins ängstlich verschlossene  
Schurztuch.
**Tochter holt das Licht heraus aus Mutters Tasche**

Als es sein Händchen  
wieder herauszieht,  
ist es schneeweiß,  
als wär es mit Mehl  
rings überpudert.
**Licht "wurde in Sterne verwandelt"**

Und die Kleine,  
längst gewitzt,  
tupft mit dem  
niedlichen Zeigefinger  
den ganzen Himmel voll  
**zeichnet die Sterne**
und jauchzt laut auf  
in kindlicher Freude.  
Ganz unten aber  
macht sie einen großen,  
runden Tupfen -  
das ist der Mond.
**zeichnet den Mon

Mütterchen Dämmerung  
sieht ihr mit mildem  
Lächeln zu.  
Und dann geht es  
langsam  
zu Bette.
**Mutter geht schlafen == Dämmerung zu Ende**