# Weizsäcker-Rede zum Kriegsende: "Der 8. Mai war ein Tag der Befreiung"
**Richard von Weizsäcker war der erste Bundespräsident, der den 8. Mai 1945 einen "Tag der Befreiung" nannte. Seine Rede vom 8. Mai 1985 vor dem Deutschen Bundestag, die von Weizsäcker in neun Kapitel unterteilte, gilt als ein Meilenstein in der öffentlichen Aufarbeitung der NS-Zeit in Deutschland.**

Manche junge Menschen haben sich und uns in den letzten Monaten gefragt, warum es vierzig Jahre nach Ende des Krieges zu so lebhaften Auseinandersetzungen über die Vergangenheit gekommen ist. Warum lebhafter als nach fünfundzwanzig oder dreißig Jahren? Worin liegt die innere Notwendigkeit dafür?

Es ist nicht leicht, solche Fragen zu beantworten. Aber wir sollten die Gründe dafür nicht vornehmlich in äußeren Einflüssen suchen, obwohl es diese zweifellos auch gegeben hat.

Vierzig Jahre spielen in der Zeitspanne von Menschenleben und Völkerschicksalen eine große Rolle.

Auch hier erlauben Sie mir noch einmal einen Blick auf das Alte Testament, das für jeden Menschen unabhängig von seinem Glauben tiefe Einsichten aufbewahrt. Dort spielen vierzig Jahre eine häufig wiederkehrende, eine wesentliche Rolle.

Vierzig Jahre sollte Israel in der Wüste bleiben, bevor der neue Abschnitt in der Geschichte mit dem Einzug ins verheißene Land begann.

Vierzig Jahre waren notwendig für einen vollständigen Wechsel der damals verantwortlichen Vätergeneration.

An anderer Stelle aber (Buch der Richter) wird aufgezeichnet, wie oft die Erinnerung an erfahrene Hilfe und Rettung nur vierzig Jahre dauerte. Wenn die Erinnerung abriß, war die Ruhe zu Ende.

So bedeuten vierzig Jahre stets einen großen Einschnitt. Sie wirken sich aus im Bewusstsein der Menschen, sei es als Ende einer dunklen Zeit mit der Zuversicht auf eine neue und gute Zukunft, sei es als Gefahr des Vergessens und als Warnung vor den Folgen. Über beides lohnt es sich nachzudenken.

Bei uns ist eine neue Generation in die politische Verantwortung hereingewachsen. Die Jungen sind nicht verantwortlich für das, was damals geschah. Aber sie sind verantwortlich für das, was in der Geschichte daraus wird.

Wir Älteren schulden der Jugend nicht die Erfüllung von Träumen, sondern Aufrichtigkeit. Wir müssen den Jüngeren helfen zu verstehen, warum es lebenswichtig ist, die Erinnerung wachzuhalten. Wir wollen ihnen helfen, sich auf die geschichtliche Wahrheit nüchtern und ohne Einseitigkeit einzulassen, ohne Flucht in utopische Heilslehren, aber auch ohne moralische Überheblichkeit.

Wir lernen aus unserer eigenen Geschichte, wozu der Mensch fähig ist. Deshalb dürfen wir uns nicht einbilden, wir seien nun als Menschen anders und besser geworden.

**Es gibt keine endgültig errungene moralische Vollkommenheit - für niemanden und kein Land! (Litotes)**  Wir haben als Menschen gelernt, wir bleiben als Menschen gefährdet. Aber wir haben die Kraft, Gefährdungen immer von neuem zu überwinden.

Hitler hat stets damit gearbeitet, Vorurteile, Feindschaften und Hass zu schüren.

Die Bitte an die jungen Menschen lautet:

**Lassen Sie sich nicht hineintreiben in Feindschaft und Hass (Pars pro toto)**
gegen andere Menschen,  
gegen Russen oder Amerikaner,  
gegen Juden oder Türken,  
gegen Alternative oder Konservative,  
gegen Schwarz oder Weiß.

**Lernen Sie, miteinander zu leben, nicht gegeneinander.**

Lassen Sie auch uns als demokratisch gewählte Politiker dies immer wieder beherzigen und ein Beispiel geben.

Ehren wir die Freiheit.  
**Arbeiten wir für den Frieden.  
Halten wir uns an das Recht.  
Dienen wir unseren inneren Maßstäben der Gerechtigkeit.** 
Schauen wir am heutigen 8. Mai, so gut wir es können, der Wahrheit ins Auge.

– *Richard von Weizsäcker*