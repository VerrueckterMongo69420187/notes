# Klimax
nach jeder Stufe steigt die Intensität.

## Bsp
* Ich kam, sah und siegte
* I'm stronger, I'm smarter, I am better, I am better.

# Oxymoron
eine rhetorische Figur, bei der eine Formulierung aus zwei gegensätzlichen, einander widersprechenden oder sich gegenseitig ausschließenden Begriffen gebildet wird
## Bsp
* bittersüß
* eile mit Weile

# Litotes
Die Litotes ist die Stilfigur der doppelten Verneinung oder der Verneinung des Gegenteils. Damit kann zum Beispiel eine Behauptung vorsichtig ausgedrückt oder eine Aussage abgeschwächt werden.

## Bsp
* nicht der schlechteste (= ein guter) Lehrer
* nicht unwahrscheinlich = ziemlich wahrscheinlich

# Anapher
Die Anapher ist ein rhetorisches Stilmittel; sie bezeichnet die Wiederholung eines Wortes am Anfang aufeinander folgender Verse, Strophen, Sätze oder Satzteile.
## Bsp
* **Und jeder** sucht nach einem Schutz, **und jeder** wünscht sich Trost

# Alliteration
Die Alliteration ist eine literarische Stilfigur oder ein rhetorisches Schmuckelement, bei der die betonten Stammsilben benachbarter Wörter den gleichen Anfangslaut besitzen.

## Bsp
* **Milch macht müde Männer munter**

# Pars pro toto
Pars pro toto bedeutet übersetzt: „Ein Teil steht für das Ganze“. Das Prinzip des pars pro toto kann als sprachliches Phänomen eine rhetorische Figur bezeichnen, aber auch praktisch als gesellschaftliche oder psychologische Handlungsweise und insbesondere als Form des Fetischismus verstanden werden.

## Bsp
* Gegen Russen oder Amerikaner,  gegen Juden oder Türken.  

# Hyperbel
Eine Übertreibung

## Bsp
*  riesengroß; blitzschnell; eiskalt