# LernApp
Zu Schreiben ist eine Flutter App, welche erfassen soll, 
wieviel Zeit für welchen Gegenstandaufgewendet wird. 
Orientiere dich dabei an folgenden Designs:

![img.png](resources%2Fimg.png)

Funktionalität:

* Zu Beginn zeigt die **App** eine Aufforderung, Daten einzugeben 

* Über den **FloatingActionButton** sowie die action in der AppBar 
wird ein **modalBottomSheet** angezeigt, mit welchem Daten erfasst 
werden können. 

  * *Das Widget, welches den ModalBottomSheet füllen soll, 
  muss ein eigenes Widget sein, da sonst der State des verdeckten Screens 
  upgedated wird.*


* Dabei können nur Daten innerhalb des aktuellen Schuljahres  gewählt 
werden. Ist ein Datum ausgewählt worden 
wird dies irgendwie im UI angezeigt.

  * ***showDatePicker**; wir nehmen an, dass jedes Schuljahr vom **1.9** bis zum **1.7** geht*


* Daten können auch wieder gelöscht werden

* In einer Sektion des Screens werden Bars angezeigt, deren 
Füllstand anzeigt, wieviel derGesamtzeit in diesen Gegenstand 
geflossen ist. 
* Eine Legende gibt Aufschluss über dietatsächliche aufgewendete 
Gesamtzeitpro Gegenstand. Die Bars sind absteigend zu sortieren.

* **intl** ist zum Formatieren des Datums hilfreich

Einige verwendete Widgets:

* DropDownButton, DropdownMenuItem
* TextField, TextEditingController
* Stack, RotatedBox
* ListTile
* FloatingActionButton
* IconButton