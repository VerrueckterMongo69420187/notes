---
Fach: DBI
Datum: Thursday, 05.12.2024
Zeit: 09:11
Themen: RAID
Seiten:
---
---
Disk 1 = 1111
Disk 2 = 1110
Disk 3 = 1100
Disk 4 = 1000

PA=?

(XOR Disk 1 x Disk 2) = 0001
(x Disk 3) = 1101
(x Disk 4) = 0101 = PA