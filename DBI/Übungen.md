---
Fach: DBI
Datum: Monday, 19.02.2024
Zeit: 10:47
Themen: 
Seiten:
---
---
R(A,B,C,D) Prime Attribute: A,D,C,B
FD: {A=>B, B=>C, C=>A}
AD=>ck
|
v
CD
C+ = {CBA}
D+ = {D}
C+ und D+ keine sk => CD = ck
BD
B+ = {BAC}
D+ = {D}
B+ und D+ keine sk => BD = ck

3CK: AD, CD, BD

---
R(ABCDE) Prime Attribute: E
FD: {A=>B, B=>C, C=>D, D=>A}
R1(ABC)
R2(CDE)


# R1
A+ = {ABCD}
B+ = {BCDA}
C+ = {CDAB}
AB+ = {ABCD}
BC+ = {BCDA}
D streichen, trivialen streichen

A=>BC
B=>CA
C=>AB
(AB=>C) streichen
(BC=>A) streichen

F1 = A =>BC, B=>CA, C=>AB

# R2
C+ = {CDAB}
D+ = {DABC}
E+ = {E}
CD+ = {CDBA}
DE+ = {DABCE}
EC+ = {ECDAB}
offensichtlichen streichen, trivialen wie z.B.: A,B streichen

C=>D
D=>C
(DE=>C) streichen
(CE=>D) streichen

F2 = C=>D, D=>C
# Lösung
F1uF2 = {A=>BC, B=>CA, C=>AB, C=>D, D=>C}

alles außer D=>A existiert
D+ aus F1uF2 ermitteln
D+ = {DCAB}
man sieht dann das A funktional abhängig von D ist!

# Übung
R(A,B,C,D) Prime:
FD = {A=>B,C=>D}
BD+ = {B,D,A,C}
B+ = {B,A}
D+ = {D,C}

