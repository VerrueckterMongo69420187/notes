# 1. Bsp
R (A, B, C, D, E) ⇒ 16 Kombinationen mit A
FD: {A → B, B → C, C → D, D → E}

**A:**
A → C
A → A
A → D
A → E

A → A B C D E
A<sup>+</sup> = {A B C D E} ⇒ Super Key ⇒ Candidate Key


A → B
AD → BD
AD → B
AD → D

AD<sup>+</sup> = ??
AD<sup>+</sup> = {A D B...}
AD<sup>+</sup> = {A D B C ~~D~~ E} ⇒ Super Key ⇒ A Subset ebenfalls Super Key ⇒ kein Candidate Key
B<sup>+</sup> = {B C D E}
CD<sup>+</sup> = {C D E}


**B:**
B → D
B → E
B → B

B → B C D E


**C:**
C → E
C → C

C → C D E


BCDE<sup>+</sup> = {B C D E} ⇒ kein Super Key

---

# 2. Bsp
R (A, B, C, D, E)
FD: {A → B, D → E}


## 1. Variante
ABCDE<sup>+</sup> = {A, B, C, D, E} ⇒ Super Key ⇒ kein Candidate Key

ABDE<sup>+</sup> = {A, B, D, E} ⇒ kein Super Key

ACDE<sup>+</sup> = {A, C, D, E, B} ⇒ Super Key ⇒ kein Candidate Key

ACD<sup>+</sup> = {A, C, D, B, E} ⇒ Super Key ⇒ **Candidate Key**

AC<sup>+</sup> = {A, C, B} ⇒ kein Super Key

CD<sup>+</sup> = {C, D, E} ⇒ kein Super Key

AD<sup>+</sup> = {A, D, B, E} ⇒ kein Super Key

A<sup>+</sup> = {A, B} ⇒ kein Super Key

C<sup>+</sup> = {C} ⇒ kein Super Key

D<sup>+</sup> = {D, E} ⇒ kein Super Key




## 2. Variante
A~~B~~CDE<sup>+</sup> = {A, B, C, D, E} ⇒ Super Key
ACD~~E~~<sup>+</sup> = {A, B, C, D, E} ⇒ Super Key
ACD<sup>+</sup> = {A, C, D, B, E} ⇒ Super Key ⇒ **Candidate Key** ⇒ A, C, D Prime Attribute
AC<sup>+</sup> ⇒ kein Super Key
AD<sup>+</sup> ⇒ kein Super Key
CD<sup>+</sup> ⇒ kein Super Key
A<sup>+</sup> ⇒ kein Super Key
C<sup>+</sup> ⇒ kein Super Key
D<sup>+</sup> ⇒ kein Super Key


---

# 3. Bsp
R (A, B, C, D)
FD: {A → B, B → C, C → A}

Prime Attribute: A, D, C, B

A~~B~~CD<sup>+</sup> = {A, B, C, D}
A~~C~~D<sup>+</sup> = {A, B, C, D}
AD<sup>+</sup> = {A, D, B, C} ⇒ Super Key ⇒ **Candidate Key**
A<sup>+</sup> = {A, B, C} ⇒ kein Super Key
D<sup>+</sup> = {D} ⇒ kein Super Key

CD<sup>+</sup> = {C, D, A, B} ⇒ Super Key ⇒ **Candidate Key**
C<sup>+</sup> = {C, B, A} ⇒ kein Super Key
D<sup>+</sup>= {D} ⇒ kein Super Key

BD<sup>+</sup> = {B, D, A, C}⇒ Super Key ⇒ **Candidate Key**
B<sup>+</sup> = {B, A, C} ⇒ kein Super Key
D<sup>+</sup> = {D} ⇒ kein Super Key

3 Candidate Keys: AD, CD, BD

---

# 4. Bsp
R (A, B, C, D, E)
FD: {A → B, B → C, C → D, D → A}

R<sub>1</sub> = (A, B, C)
A<sup>+</sup> = {~~A~~, B, C, ~~D~~}     A → BC
B<sup>+</sup> = {~~B~~, C, ~~D~~, A}     B → CA
C<sup>+</sup> = {~~C~~, ~~D~~, A, B}     C → AB
AB<sup>+</sup> = {~~A~~, ~~B~~, C, ~~D~~}   AB → C
BC<sup>+</sup> = {~~B~~, ~~C~~, ~~D~~, A}   BC → A

<font color="#00b0f0">F<sub>1</sub> = A → BC, B → CA, C → AB</font>

R<sub>2</sub> = (C, D, E)
C<sup>+</sup> ={~~C~~, D, ~~A~~, ~~B~~}          C → D
D<sup>+</sup> = {~~D~~, ~~A~~, ~~B~~, C}         D → C
E<sup>+</sup> = {E}
CD<sup>+</sup> = {C, ~~D~~, ~~A~~, ~~B~~}      CD → D
DE<sup>+</sup> = {~~D~~, ~~E~~, ~~A~~, ~~B~~, C}   DE → C
CE<sup>+</sup> = {C, E, D, A, B}

<font color="#00b0f0">F<sub>2</sub> =C → D, D → C</font>

<font color="#00b0f0">F<sub>1</sub> ∪ F<sub>2</sub> = {A → BC, B → CA, C → AB, C → D, D → C}</font>

<font color="#ff0000">D<sup>+</sup> = {D, C, A, B}</font>