---
Fach: NSCS
Datum: Wednesday, 11.12.2024
Zeit: 09:51
Themen: DNS, VPN, PSK, WAN
Seiten:
---
---
# DNS (Domain Name System)
- Mehrere Dienste über eine IP ansprechen
- Namen hierachisch aufgeteilt
## Pros
- IPs leicht änderbar
- Namen leichter merken
- Lastverteilung auf DNS Ebene

## Struktur
- Baumförmig (Root-Punkt -> Top Level Domain -> Second Level Domain -> Third Level Domain …..)
![[Pasted image 20241211095552.png]]
### Top-Level Domains
#### Geografisch
- .at Österreich
- .ca Kanada
#### Organisatorisch
- .com Kommerzielle Unternehmen
- .edu Bildungseinrichtung
- .org Nicht kommerzielle Organisation, z.B. Wikipedia
- .net Netzwerkeinrichtungen
- .int Internationale Organisationen
- .gov Staatliche Institutionen
- .mil Militärische Einrichtung

## Namensauflösung
- Domains -> IP-Adressen
- Ergebnis wegen Performance zwischengespeichert
- Jeder Eintrag hat TTL (Time To Live)
### Schritte
1) Client -> Anfrage lokaler Server
2) Kennt Domain? -> direkte Antwort, wenn nicht -> Top-Level auf DNS Root Server abfragen -> Antwort = IP für zuständigen DNS-Server
3) Hierachie solange abgearbeitet, bis zuständiger DNS gefunden wurde
4) DNS Server -> gewünschte IP an Lokalen DNS
5) Lokal DNS cached Information und sendet zu Client

Wird auch eingesetzt bei:
- E-Mail
- Telefon (ENUM)

## DNS Server
- wird unterschieden zwischen Primary und Slave Server
- Slave kann Primary übernehmen und synchronisiert mit Primary
- Slave -> Lastausgleich, Redundanz
- Meist DNS Server = DNS Resolver oder Anfragen an Resolver weiterleiten, wenn Top-Level nicht in Zuständigkeitsbereich von Server liegt.

## DNS Resolver
- Client kann Anfragen stellen, aber nicht mit Hierachie und Auflösung arbeiten
- Verbindung meist unverschlüsselt über UDP

## Client
- Client such in */etc/hosts* nach IP zu Domain, sonst DNS
- *hosts* war Vorläufer von DNS, musste auf jedem Rechner aktualisiert werden
### SOA (Start Of Authority)
- **Server** Festlegung
- **E-Mail** Adresse von Administrator
- **Serial** von Eintrag
- **Refresh** (wie lange Informationen gültig sind)
- **Retry** (Abstände von Wiederversuchen im Fehlerfall)
- **Expire** (wann Eintrag ungültig wird)
- **Negative Cache TTL** (wie lange Information ohne Domain gefunden gültig ist)

### A
IPv4 Adresse zur Domain

### AAAA
IPv6 Adresse zur Domain

### NS
Name Server
- über Domain angegeben
- passender A Eintrag

### MX
E-Mail Eingangsserver
- über Domain angegeben
- passender A Eintrag

### CNAME
Alternativname für Domain

### TXT
Texteinträge
- für Mail und ENUM (siehe [[#Namensauflösung]])

### LOC
Positionsangabe

### NAPTR
Adressen von Server und Informationen wie Dienste und URIs ausgeliefert

### RP
Verantwortliche Person

## DNSSec (Domain Name System Security Extension)
- garantiert mit digitaler Signatur Echtheit (Authentizität) und Vollständigkeit (Integrität) von DNS-Antworten
- Lückenlose Signierung auf allen Ebenen des DNS.
- Signierte Ebenen: Root-Server (ICANN), Top-Level Domains (z. B. .at), individuelle Domains (Registrar/ISP).
- Signierung der Root-Zone durch ICANN seit 2010.
- Einführung von DNSSEC: Zunehmende Implementierung durch Top-Level Domain Registries.
- Regionale Unterschiede:
    - Nordamerika: Höhere DNSSEC-Durchdringung (wenige ccTLDs).
    - Asien: Niedrigere DNSSEC-Durchdringung (viele ccTLDs).

(ccTDL = Country Code Top-Level Domain)

### Sicherheitsrelevante Schwachstellen von DNSSEC
- Denial-of-Service-Attacks auf NS werden durch DNSSEC nur erleichtert
- DNS Amplification Attacks effektiver -> DNS-Antworten wegen DNSSEC länger
- public keys am besten nicht über DNS-System verteilen -> Angriffsmöglichkeiten ergeben sich
- durch DNSSEC Walking Inhalt von Zonen auslesbar
- Stubresolver validieren DNS-Antworten oft nicht selbst -> Angriff auf NS kann erfolgen, NS selbst kann auch kompromittiert werden.

## DOH / DOT
**DNS over HTTPS**:
- HTTPS Verbindung Client <--> DNS Resolver
- Integrität, Vertraulichkeit, Authentizität gegeben zwischen Resolver und Client
- Kommt dadurch leichter durch Firewalls, da es keine DNS Abfrage ist

**DNS over TLS**:
- DNS Anfrage mittels TLS verschlüsselt übertragen
- Port 853
- Integrität, Vertraulichkeit, Authentizität gegeben zwischen Resolver und Client

# PKI (Digitale Signatur)
Dient zu Sicherstellung von Authentizität und Integrität, nicht zu Geheimhaltung von Daten
## Struktur
![[Pasted image 20241213222533.png]]
- Public Key meist als Zertifikat weitergegeben

## Zertifikate
- Verbindlichkeit von Nachricht nicht kontrollierbar
- public key als "vertrauenswürdig" eingestuft für sichere Kommunikation
- **Certificate Authorities (CA)**, siehe [[#X.509 Standard]]
- zeitlich befristet
- alle unterschriebenen Zertifikate werden von CA veröffentlicht
- CA's können staatsnahe oder private Unternehmen sein

### X.509 Standard
- wichtigter Standard
- **muss aufweisen**:
	- Name Zertifikatseigentümer
	- Public Key Eigentümer
	- Name CA
	- digitale Signatur CA
	- Gültigkeitsdatum
	- Verwendungszweck

- viele verschiedene Algorithmen für digitale Signatur -> Algorithmus angeben
- Zertifikate bei Missbrauch von ausgestellter CA **widerrufbar**
- **Certificate Revocation List (CRL)** wird periodisch aktualisiert, jeder Teilnehmer von Kommunikation kann Liste auf **Gültigkeit von Zertifikaten** überprüfen

## PKI (Public Key Infrastructure)
### Struktur
![[Pasted image 20241213225322.png]]

- **Assymetrisches Kryptosystem** kann Nachrichten **signieren** und **verschlüsseln** in Netzwerk
- Bei richtiger **Parameterwahl** (z.B. Schlüssellänge) schwer bis gar nicht brechbar -> bald vllcht nicht mehr (Willow Chip)
- Sender benötigt Public Key von Empfänger (z.B. per Email, Website)
- Digitale Zertifikate dienen für **Authentizität** von Public Keys, bestätigen zulässigen Anwendungs- und Geltungsbereich
- Zertifikat selbst durch Signatur geschützt, Prüfung anhand Public Key von Aussteller
- Ausstellerschlüssel prüft man mit Zertifikat
- **Validierungspfad/Zertifizierungspfad** ist Kette von Zertifikaten, die jeweils Public Key überprüfen
- auf letztes Zertifikat ohne anderes Zertifikat verlassen

## Bestandteile von PKI (wichtig)
### Digitale Zertifikate
Digital Signierte elektronische Daten, die sich zum Nachweis von Echtheit von Objekten verwenden lassen.

### Zertifizierungsstelle (Certificate Authority, CA)
Organisation, die CA-Zertifikat bereitstellt und Signatur von Zertifikatsanträgen übernimmt.

### Registrierungsstelle (Registration Authority, RA)
- Organisation, wo Personen, Maschinen, untergeordnete Zertifizierungsstellen Zertifikate beantragen können
- Prüft **Richtigkeit der Daten** im gewünschten Zertifikat
- genehmigt Zertifikatsantrag
- [[#Zertifizierungsstelle (Certificate Authority, CA)]] signiert Antrag
- manuell Registration Authority Officer

### Zertifikatsperrliste (Certificate Revocation List, CRL)
- List von Zertifikaten, die vor Ablauf der Gültigkeit zurückgezogen wurden
- **Gründe**: Kompromittierung, Ungültigkeit der Zertifikatsdaten (z.B. E-Mail), Verlassen der Organisation
- definierte Laufzeit, bei Ablauf aktualisiert erzeugt
- Umgekehrt **Whitelist** (Positivliste)-> nur gültige Zertifikate
- **OCSP** (Online Certificate Status Protocol), **SCVP** (Server-based Certificate Validation Protocol) -> Online-Statusprüfung
- Online-Statusprüfung dann, wenn zeitgenaue prüfung wichtig (z.B. Finanzielle Transfers, ...)

### Verzeichnisdienst (Directory Service)
- durchsuchbares Verzeichnis
- enthält ausgestellte Zertifikate zusätzlich zu Usern/Maschinen
- meist LDAP-Server, seltener X.500-Server

### Dokumentationen
- Eine PKI führt 1 oder mehr Dokumente
- beinhalten Arbeitsprinzipien von PKI (**bla bla**)
	- Registrierungsprozess
	- Handhabung Private Key
	- zentrale/dezentrale Schlüsselerzeugung
	- technischer Schutz PKI-System

#### CP (Certificate Policy)
- Beschreibung Anforderungsprofil an eigene Arbeitsweise
- Dient zur Analyse der Vertrauenswürdigkeit, Aufnahme in den Browser

#### CPS (Certification Practice Statement)
- konkrete Umsetzung der Anforderungen in die PKI beschrieben
- Umsetzung der CP

## Vertrauensmodelle
### Streng hierachische PKI
- setzt **Existenz einer Wurzelinstanz** (Root-CA) voraus
- **alle Parteien vertrauen Root-CA**
- Root-CA existiert **auf globaler Ebene** in der Praxis **nicht**
- Länder, Regionen haben **eigene hierachische PKI** mit eigenem Root-CA für Kontrolle
- wichtige Root-CA's oft in Software integriert (Browser)
![[Pasted image 20241214005810.png]]
### Cross-Zertifizierung
- **Zertifikatsanwendung über Grenzen verschiedener hierachischer PKI's**
- Zwei CA's stellen sich gegenüber, stellen sich gegenseitig ein (Cross-)Zertifikat aus
- Crosszertifikat drückt Vertrauen **zweier gleich berechtigter Parteien** aus
- Regelungen von Wurzelinstanz sind für PKI anderer Wurzelinstanz nicht verbindlich
![[Pasted image 20241214013423.png]]

### Web of Trust
- Verschlüsselungssoftware PGP, Open-Source Variante
- kann von **jedem Benutzer** (Web-of-Trust-Mitglied) erzeugt werden
- Wenn Benutzer an Public Key von einer Person glaubt, die ihn veröffentlicht, macht Benutzer Zertifikat durch Public Key signieren
- Je mehr Zertifikate an Schlüssel hängen, desto sicherer gehört Schlüssel angegebenen Eigentümer
- **Nachteil**: man kann nicht zu 100% Gültigkeit von einem Zertifikat nachweisen
- **Vorteil**: keine zentrale Authorität verpflichtend vorhanden
![[Pasted image 20241214014936.png]]

# WAN Concepts
## Purpose of WANs
- telecommunication network spanning over large area for connecting beyond LAN boundaries

| LAN                                        | WAN                                                                      |
| ------------------------------------------ | ------------------------------------------------------------------------ |
| small geographic area                      | large geographic area                                                    |
| interconnect local computers, devices, ... | interconnect remote users, networks, sites                               |
| owned/managed by home user or organisation | owned/managed by internet service, telephone, cable, sattelite providers |
| no fees besides infrastructure             | fee                                                                      |
| high bandwith speed                        | low-high bandwith speeds, long distance                                  |
### Private & Public WANs
#### Private WAN
- dedicated to **single customer**
- provide:
	- guaranteed service level
	- consistent bandwith
	- security

#### Public WAN
- provided by ISP or telecommunication service provider
- service levels & bandwith vary
- **no guaranteed security** with shared connections

### WAN Topologies
- Point-To-Point Topology
- Hub-and-Spoke Topology
- Dual-homed Topology
- Fully Meshed Topology
- Partially Meshed Topology

Large Networks use **combinations**

#### Point-to-Point Topology
- P2P circuit **between 2 endpoints**
- Layer 2 transport service through service network provider
- connection transparent to customer network
- **expensive** for many P2P connections
![[Pasted image 20241214194503.png]]

#### Hub-and-Spoke Topology
- **single interface on hub router** shared by all spoke circuits
- Spoke routers **interconnected through hub router** with **virtual circuits & router subinterfaces**
- communication only through hub router
- hub router == single point of failure
![[Pasted image 20241214194513.png]]

#### Dual-homed Topology
- **network redundancy**, **load balancing**, distributed computing & processing, able to implement backup service provider connections
- **more expensive** than single-homed -> additional hardware (routers, etc.)
- **implementation difficult** -> additional & complex configurations
![[Pasted image 20241214195339.png]]

#### Fully Meshed Topology
- **multiple virtual circuits**
- **most fault-tolerant**
![[Pasted image 20241214195548.png]]
#### Partially Meshed Topology
- sites **partially** connected
![[Pasted image 20241214195553.png]]

### Carrier Connections
- organization signs **SLA** (Service Level Aggreement)
- **SLA** outlines expected services (reliability, availability of connection)
- service provider may / may not be carrier
- **carrier owns & maintains physical connection & equipment** between customer & provider
- organizations -> usually **single**- or **dual-carrier-WAN**

#### Single-Carrier-WAN
- organization connects to **1 service provider**
- SLA negotiated between organization & service provider
![[Pasted image 20241214201141.png]]

#### Dual-Carrier-WAN
- **redundancy**
- **network availability**
- **seperate** SLAs with 2 service providers 
![[Pasted image 20241214201145.png]]

### Evolving Networks (for company)
- **meet day2day operational needs** for business, etc.
- must be able to **adapt & grow**
- Network designers & administrators choose technologies, protocols, service providers
- Network optimization through design techniques

#### Small Network
- **single LAN connection**
- internet through **broadband** service through DSL
- IT-Support through DSL provider
![[Pasted image 20241214201157.png]]


#### Campus Network
- requires **CAN** (Campus Area Network)
- firewall secures access
- In-house IT staff
![[Pasted image 20241214201319.png]]

#### Branch Network
- branch site in city
- remote & regional sites
- **MAN** (Metropolitan Area Network) -> interconnecting sites
- branch offices -> private dedicated lines from local service provider
![[Pasted image 20241214201552.png]]

#### Distributed Network
- **Site2Site & remote access VPNs** for securly connecting with employees & facilities
![[Pasted image 20241214201752.png]]

## WAN Operation
### WAN Standards
- **TIA/EIA**: Telecommunications Industry Association and Electronic Industries Alliance
- **ISO**: International Organization for Standardization
- **IEEE**: Institute of Electrical and Electronics Engineers

### WANs in OSI Model
#### Layer 1 Protocols
- **SDH**
- **SONET**
- **DWDM**

#### Layer 2 Protocols
- **Broadband**
- **Wireless**
- **Ethernet WAN** (Metro Ethernet)
- **MPLS**: Multiprotocol Label Switching
- PPP: Point-to-Point Protocol (less used)
- HDLC: High-Level Data Link Control (less used)
- Frame Relay
- ATM: Asynchronous Transfer Mode

### Common WAN Terminology

| WAN Term                                | Description                                                                                    |
| --------------------------------------- | ---------------------------------------------------------------------------------------------- |
| **DTE** (Data Terminal Equipment)       | connects subscriber LANs to WAN communication device                                           |
| **DCE** (Data Communications Equipment) | Device for communicating with provider                                                         |
| **CPE** (Customer Premises Equipment)   | DTE & DCE devices on enterprise edge                                                           |
| **POP** (Point-of-Presence)             | Point, where subscriber connects to service provider network                                   |
| **Demarcation Point**                   | Physical location in building that seperates CPE from service provider Equipment               |
| **Local Loop (last mile)**              | Copper & Fiber cable connecting CPE to CO of service provider                                  |
| **CO** (Central Office)                 | Local service provider facility connecting CPE to provider network                             |
| **Toll network**                        | Includes Switches, fiber-optic communications lines, routers, etc. inside WAN provider network |
| **Backhaul network**                    | Connects multiple access nodes of service provider network                                     |
| **Backbone Network**                    | Large networks used to interconnect service provider networks & to create redundant network    |
### WAN Devices

| WAN Device                         | Description                                                                             |
| ---------------------------------- | --------------------------------------------------------------------------------------- |
| **Voiceband Modem**                | Dial-up modem using telephone lines Legacy device                                       |
| **DSL Modem / Cable Modem**        | known as broadband modems, connect to DTE router using Ethernet                         |
| **CSU/DSU**                        | Digital-leased lines require CSU & DSU, connects digital device to digital line         |
| **Optical Converter**              | connect fiber-optic media to copper media, convert optical signals to electronic pulses |
| **Wireless Router / Access Point** | Devices are used to wirelessly connect to a WAN provider                                |
| **WAN Core devices**               | WAN backbone consists of multiple high-speed routers and Layer 3 switches               |

### Serial Communication
- **transmits bits sequentially** over single channel
- longer cable length = more sensitive sync timing between multiple channels -> parallel communication = short distance

### Circuit-Switched Communication
- **establishes dedicated virtual connection** through service provider network before communication can start
- All communication uses same path
- most common:
	- **PSTN** (public switched telephone network)
	- **ISDN** (Integrated Services Digital Network)

### Packet-Switched Communication
- **data trafficed into packages** & routed over shared network
- less expensive & more reliable than circuit switching
- common types:
	- **Ethernet WAN** (Metro Ethernet)
	- **Multiprotocol Label Switching** (MPLS)
	- **Frame Delay**
	- **Asynchronous Transfer Mode** (ATM)

### SDH, SONET, DWDM
#### SDH
**Synchronous Digital Hierachy** is global standard for transporting data over fiber-optic cable.

#### SONET
**Synchronous Optical Networking** is North American standard that provides the same services as SDH.

#### DWDM
**Dense Wavelenght Division Multiplexing** increases data-carrying capacity of SDH & SONET by simultaneously sending multiple streams of data (multiplexing) using different wavelengths of light.

## Traditional WAN Connectivity (**eher unwichtig**)
### Traditional WAN Connectivity Options
- needed their networks to connect to local loop of service provider
- accomplished by using dedicated lines or using switched services from service provider
![[Pasted image 20241214221526.png]]

### Common WAN Terminology
- **Leased lines** generally prices based on required bandwidth and distance P2P
- two systems (**unwichtig**)
	- **T-carrier**: Used in North America, provides T1 links with bandwidth 1.533 Mbps and T3 links up to 43.7 Mbps.
	- **E-carrier**: Used in Europe, provides E1 links up to 2.048 Mbps and E3 links up to 34.368 Mbps

#### Advantages
- **Simplicity**: P2P communication lnks require minimal expertise to install & maintain
- **Quality**: P2P communication links offer high quality service if having adequate bandwidth
- **Availability**: P2P communication lnks provide permanent, dedicated capacity

#### Disadvantages
- **Cost**: P2P links are most expensive type of WAN access
- **Limited flexibility**: WAN traffic often variable, leased lines have fixed capacity

### Circuit-Switch Options (**unwichtig**)
#### PSTN (Public Service Telephone Network)
- Dialup WAN access uses PSTN as WAN connection
- Traditional local loops can transport binary computer data through voice telephone network using voiceband modem
- Physical characteristics of local loop and connection to PSTN limit rate of signal to <56 kbps

#### ISDN (integrated Services Digital Network)
- enables PSTN local loop to carry digital signals
- data rates from 45 Kbps to 2.048 Mbps

### Packet Switching Options
#### Frame Relay
- simple Layer 2 non-broadcast multi-access WAN technology used to interconnect enterprise LANs
- creates PVCs which are uniquely identified by data-link connection identifier (DLCI)

#### Asynchronous Transfer Mode (ATM)
- capable of transferring voice, video and data through private and public networks
- built on cell-based architecture
- ATM cells always fixed length of 53 bytes

## Modern WAN Connectivity
### Modern WANs
- Enterprises require **faster, more flexible** WAN connectivity options
- Traditional WAN options either no longer available, too expensive, limited bandwidth

### Modern WAN Connectivity Options
![[Pasted image 20241214224459.png]]

#### Dedicated broadband
- fiber installed to **connect remote locations directly together**
- dark fiber leasable or purchased from supplier

#### Packet-switched
- **Metro Ethernet** replacing many traditional options
- **MPLS** enables sites to connect to provider regardless of access technology

#### Internet-based broadband
- global internet infrastructure for WAN connectivity

### Ethernet WAN
- other names
	- Metropolitan Ethernet (Metro E)
	- Ethernet over MPLS (EoMPLS)
	- Virtual Private LAN Service (VPLS)
- **Benefits**
	- Reduced expenses and administration
	- Easy integration with existing networks
	- Enhanced business productivity

### MPLS
- MPLS supports **variety of client access methods** (Ethernet, DSL, ...)
- can encapsulate all types of protocols including IPv4 and IPv6 traffic
- MPLS router can be customer edge (CE) router, provider edge (PE) router or internal provider (P) router
- MPLS routers are label switched routers (LSRs), attach labels to packets for forwarding by other MPLS routers
![[Pasted image 20241214225353.png]]

## Internet-Based Connectivity
### Internet-Based Connectivity Options
Alternative to dedicated WAN options

#### Wired Options
- permanent cabling
- consistent bandwidth
- reduce error rates and latency

#### Wireless Options
- **less expensive** to implement because no cables
- signals may be negatively affected by factors

### DSL Technology
- **uses telephone lines to provide IP services**
- categorized as Asymmetric DSL (ADSL) or Symmetric DSL (SDSL)
	- ADSL and ADSL2+ provider higher downstream bandwidth than upload bandwidth
	- SDSL provides same capacity both directions
- transfer rates dependent on cable type and condition and length of local loop

### DSL Connections
- connection between DSL modem and DSL access multiplexer (DSLAM)
- **Modem converts Ethernet signals to DSL signal**, then transmitted to DSLAM at provider location
- DSLAM located at Central Office (CO) of provide, concentrates connections form multiple DSL subscribers
- not shared medium
- each user has seperate direct connection to DSLAM
![[Pasted image 20241214231718.png]]


### DSL and PPP
ISPs use PPP as Layer 2 protocol for broadband DSL connections

#### PPP
- can be used to authenticating subscriber
- assign public IPv4 address to subscriber
- provides link-quality management features

Two ways to deploy PPP over Ethernet
- **Host with PPoE Client**: PPPoE client software communicates with DSL mdoem using PPPoE, modem communicates with ISP using PPP
- **Router PPPoE Client**: router is PPPoE client, obtains configuration from provider

### Cable Technology
uses coaxial vable

**Data over Cable Service Interface Specification (DOCSIS)** is  **international standard** for adding **high bandwith data to existing cable system**

(**eher unwichtig**)
- optical node converts RF signals to light pulses over fiber-optic cable
- fiber media enables signals to travel over long distances to provider headend, where Cable Modem Termination System (CMTS) is
- headend contains databases needed to provide internet access while CMTS is responsible for communicating with cable modems
![[Pasted image 20241214232255.png]]

### Optical Fiber
**FTTx** can deliver **highest bandwidth of all broadband options**
- **Fiber to the Home (FTTH)**: Fiber reaching residence
- **Fiber to the Building (FTTB)**: Fiber reaches building, alternative means to living space
- **Fiber to the Node/Neighborhood (FTTN)**: Optical cabling reaches optical node converting optical signals to format acceptable for TP or COAX cable to premise

### Wireless Internet-Based Broadband
- **Municipal Wi-Fi**: high-speed internet access for free or much less thand other broadband services
- **Cellular**: connecting to internet using radio waves through phone tower (3G, 4G, 5G, ...)
- **Satellite Internet**: where DSL or cable not available, router through satellite dish pointed to provider satellite, trees and rain can impact signal
- **WiMAX**: Worldwide Interoperability for Microwave Access (WiMAX) described in IEEE standard 802.16, high-speed broadband service, wireless access, broad coverage

### VPN Technology
- **encrypted connection between private networks over public network**
- VPN tunnels routed through internet from private network of company to remote site or employee host
- **Benefits**
	- **Cost Savings**: no WAN links and modem banks
	- **Security**: Encryption and Authentication protocols protect data from unauthorized access
	- **Scalability**: no adding infrastructure
	- **Compatibility with broadband technology**: Supported by broadband service providers such as DSL and cable
- **Implementations**
	- **Site-to-site VPN**: settings configured on router, client unaware of encryption
	- **Remote Access**: Connect and authenticate through VPN client to destination device

### ISP Connectivity Options
- **Single-homed**: single connections to ISP, uses one link, no redundancy, least expensive
- **Dual-homed**: uses two links, redundancy, load balancing, ISP outage == no internet connectivity
- **Multihomed**: connect to 2 different ISPs, increased redundancy, load-balancing, expensive
- **Dual-multihomed**: most resilient, redundant links to multiple ISPs, most possible redundancy, most expensive

### Broadband Solution Comparison (**wichtig**)
- **Cable**: Bandwidth shared by many users, upstream data rates slow during high-usage hours in areas with over-subscription
- **DSL**: limited bandwidth, distance sensitive (to ISP CO), upload rate lower compared to download
- **Fiber-to-the-Home**: fiber installation directly to home
- **Cellular/Mobile**: coverage is issue, bandwidth limited
- **Municipal Wi-Fi**: no mesh Wi-Fi network deployed, else is ok
- **Satellite**: expensive, limited capacity per subscriber, only if no other option available

# VPN and IPSec
## VPN Technology
### VPN
- **end-to-end private network connections**
- **virtual** -> information within private network, but actually over public
- **private** -> traffic **encrypted**

### VPN Benefits
- **IPSec** and **Secure Sockets Layer (SSL)**
- **costs**: reduce connectivity costs and increase remote connection bandwidth
- **security**: encryption and authetication protocols
- **scalability**: allow organizations to use internet, easy to add users
- **compatibility**: can be implemented across wide variety of WAN link options

### Site-to-Site and Remote Access VPNs
#### Site-to-Site VPN
- terminated on **VPN gateways**
- traffic only encrypted between gateways
- internal host no knowledge of VPN usage
![[Pasted image 20241215133010.png]]

#### Remote Access VPN
- secure connection between client and VPN terminating device
![[Pasted image 20241215133117.png]]

### Enterprise and Service Provider VPNs
#### Enterprise VPNs
- secure enterprise traffic
- S2S and remote acces VPN created and managed by enterprise using IPsec and SSL VPNs

#### Service Provider VPNs
- created and managed by provider network
- provider uses MPLS at Layer 2 or 3
- segregating enterprise traffc from customer traffic

## Types of VPNs
### Remote Access VPNs
typically enabled dynamically **by user** when required using IPsec or SSL
![[Pasted image 20241215135013.png]]

#### Clientless VPN connection
secured by web browser SSL connection

#### Client-based VPN connection
VPN client on client side

### SSL VPNs
uses **public key infrastructure** and digital certificates to authenticate peers

| Feature                     | IPsec                                                                     | SSL                                                     |
| --------------------------- | ------------------------------------------------------------------------- | ------------------------------------------------------- |
| **Applications supported**  | **extensive** - all IP-based apps                                         | **limited** - only web based apps                       |
| **Authentication strength** | **strong** - 2Way authentication with shared keys or digital certificates | **morderate** - 1Way or 2Way authentication             |
| **Encryption strength**     | **strong** - key lengths 56 - 256 bits                                    | **moderate to strong** - key lengths 40 - 256 bits      |
| **Connection complexity**   | **medium** - requires VPN client on host                                  | **low** - requires web browser on host                  |
| **Connection option**       | **limited** - only specific devices with specific configs can connect     | **extensive** - any device with web browser can connect |

### Site2Site IPsec VPNs
- **connect networks** accross **untrusted network** (internet)
- end host receives **unencrypted** TCP/IP traffic through VPN gateway
- VPN gateways encrypt/decrypt traffic

### GRE over IPsec
- **Generic Routing Encapsulation (GRE)**: non-secure Site2Site VPN tunneling protocol
- GRE tunnel encapsulates network layer protocols and multicast and broadcast traffic
- **no default encryption support** -> no secure VPN tunnel
- GRE packet can be encapuslated to IPse packet for forwarding to destination gateway
- standard IPsec VPNs (non-GRE) only create secure tunnels for **unicast traffic**

#### Passenger protocol
original packet to be encapsulated by GRE

#### Carrier protocol
GRE is carrier protocol encapsulating original passenger packet

#### Transport protocol
protocol for actual forwarding (IPv4 or IPv6)

![[Pasted image 20241215135748.png]]

GRE over IPsec used to **support routing protocol traffic** over IPsec VPN
![[Pasted image 20241215135905.png]]

### Dynamic Multipoint VPNs (DMVPN) (Cisco Propaganda)
- better **scalabilty**
- **hub-and-spoke** config to establish full mesh topology
- spoke sites establish VPN tunnels with hub site
- site config uses **Multipoint GRE (mGRE)** -> allows single GRE interfeace to support multiple IPsec tunnels
- spoke-to-spoke tunnels available

###  IPsec Virtual Tunnel Interface (VTI)
**simplifies config process** required to support multiple sites and remote access
- configs are **applied to virtual interface** instead of physical interface
- capable of sending and receiving IP unicast and multicast encrypted traffic -> routing protocols automatically supported without GRE tunnel config
- config between sites or hub-and-spoke topology

### Service Provider MPLS VPNs
- **traffic forwarded through MPLS backbone using labels**
- secure because customers can't see each others traffic
- MPLS provides clients managed VPN solutions -> secure traffic responsibility = service provider

#### Layer 3 MPLS VPN
service provider participates in customer routing by peering between customer and provider router

#### Layer 2 MPLS VPN
service provider not involved in customer routing, instead deploys VPLS to emulate Ethernet multiaccess LAN segment over MPLS network

## IPsec
### IPsec Technologies
defines how VPN can be secured across IP networks
- **confidentiality**: encryption algorithms to prevent cybercriminals from reading packet contents
- **integrity**: hashing algorithms ensuring packets not altered between source and destination
- **origin authentication**: **Internet Key Exchange (IKE)** protocol to authenticate source and destination
- **diffie-Hellman**: secure key exchange
- not bound to any specific rules for secure connections
- easily integrate new security technologies without updating existing standards
- open slots to fill with choices available for function to create **unique security association (SA)**
![[Pasted image 20241215145224.png]]

### IPsec Protocol Encapsulation
- first building block of framework
- **Authentication Header (AH)** or **Encapsulation Security Protocol (ESP)**
- choice determines other building block availabilities
- AH appropriate when **no confidentiality required or permitted**
- ESP provides **both**

### Confidentiality
the shorter the key, the easier it is to break
![[Pasted image 20241215145603.png]]

- **DES**: 56-bit key
- **3DES**: 3 x 56-bit key per 64-bit block
- **AES**: 128 bits, 192 bits, 256 bits
- **SEAL**: stream cipher -> **encrypts data continuously**, 160-bit key

### Integrity
- Data integrity means **data not changed in transit**
- required
- **Hashed Message Authentication Code (HMAC)** guarantees integrity of message with hash value
- **Message-Digest 5 (MD5)** uses 128-bit shared-secret key
- **Secure Hash Algorithm (SHA)** uses 160-bit secet key

### Authentication
#### Pre-shared key (PSK
value entered into each peer manually
- easy manual config
- not good scaling
- must be config'd on every peer

#### Rivest, Shamir, and Adleman (RSA)
auth. uses digital certificates to auth. peers
- **peer must auth. opposite peer** before tunnel **considered secure**

### Secure Key Exchange with Diffie - Hellman
DH allows **2 peers to establish shared secret key over insecure channel**
- DH groups **1, 2, 5** should no longer be used
- DH groups **14, 15, 16** use larger key sizes with 2048 bits, 3072 bits, 4096 bits
- DH groups **19, 20, 21, 24** -> 256 bits, 384 bits, 521 bits, 2048 bits, support **Elliptical Curve Cryptography (ECC)** -> faster key generation