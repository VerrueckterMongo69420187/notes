---
Fach: NSCS
Datum: Tuesday, 04.06.2024
Zeit: 15:21
Themen: 
Seiten:
---
---
# Topology Diagrams
1) Physical Topology
2) Logical Topology

# Network Device Documentation (glaub nd so wichtig)
- Router Device Documentation (Model, Desc., Location, ...)
- Switch Device Documentation (Model, Desc., Mgt. IP, IOS, ...)
- End-System Documentation (Services, OS, MAC Address, Default Gateway)

# Network Baseline
**Baseline Data**
- reveals congested areas
- provides insight wether network design meets business requirements
**Data Determination**
- only select few variables
- too many = overwhelming -> difficult analysis
- good starting variables
	- interface utilization
	- CPU utilization
**Identify Devices and Ports of Interest**
**Determine Baseline Duration**
- \> 7 days 
- < 6 weeks (unless long term trend measures)
- 2 - 4 weeks is good
- Analyse regularly because of network affection by growth

# Troubleshooting Process
- structured troubleshooting methods shorten time
## Seven-Step Troubleshooting Process
1) Define the Problem
2) Gather Information
3) Analyze Information
4) Eliminate Possible Causes
5) Propose Hypothesis
6) Test Hypothesis
7) Solve the Problem

## Question End User
![[end-user-questions.png]]

## Troubleshooting with Layered Models
- OSI
- TCP/IP
- can isolate network problems
- ![[ISO.png]]
## Structured Troubleshooting Methods
![[troubleshooting-methods.png]]

# Troubleshooting Tools
## Software Troubleshooting Tools
### Network Management System Tools
- device level monitoring
- configuration
- fault-management tools
- Usage: investigating and correcting network problems
### Knowledge Bases
### Baselining Tools
- help with common documentation tasks
	- network diagrams
	- update network software/hardware documentation
	- measure bandwith usage cost-efficiently
## Hardware Troubleshooting Tools
### Digital Multimeter
measure voltage, current and resistance
### Cable Testers
test data communication cabling
### Cable Analyzers
test and certify copper/fiber cables
### Portable Network Analyzers
troubleshooting switched Networks and VLANs
### Cisco Prime NAM
displays device performance analysis in switched and routed environment


## Syslog Server as a Troubleshooting Tool
### Levels
- default level 6
- lower the level, the higher severity level
![[syslog-levels.png]]

# Symptoms and Causes of Network Problems
## Physical Layer Troubleshooting
### Performance lower than baseline
previous baselines for comparison needed
#### Reasons
- over/underpowered servers
- unsuitable switch/router configurations
- ...

### Loss of connectivity
ping test
#### Reasons
- disconnected cable

### Network bottlenecks or congestion
#### Reason
protocols direct traffic to sub-optimal routes if route fails

### High CPU utilization rates
device can shut down or fail
#### Reasons
- device operates at or over design limits

### Console error messages
- could indicate physical layer problem
- cli messages should be logged on syslog server


## Physical Layer Troubleshooting
### Power-related
check vents

### Hardware faults
- bad cabling
- grounding problems

### Cabling faults
- damaged cables
- improper cables
- poorly crimped connectors

### Attenuation
- when cable length exceeds design limit of media
- poor connection from loose cable
- dirty contacts

### Noise
Local electromagnetic interference (EMI) caused by electric cables, large e-motors, ...

### Interface configuration errors
- incorrect clock rate or clock source
- interface not turned on

### Exceeding design limits
component not performing well if used beyond specifications

### CPU overload
#### Symptoms
- high CPU utilization
- input queue drops
- slow performance
- ...

### Data Link Layer Troubleshooting
#### No functionality or connectivity at the network layer above
layer 2 problems can stop exchange of frames across a link

#### Network is operating below baseline performance levels
frames can take suboptimal path but still arrive -> high bandwith usage on links

#### Excessive broadcasts
- OS use broad/multicasts extensively
##### Reason
- poorly programmed application
- large layer 2 broadcast domains
- underlying network problem

#### Console messages
most common: line protocol down message

#### Encapsulation errors
recieved bits different than expected

#### Address mapping errors
when layer 2 and Layer addressing not available

#### Framing errors
##### Reasons
- noisy serial line
- improperly designed cable
- faulty NIC
- duplex mismatch

#### STP failures or loops
##### Reason
forwarding loops when no ports in redundant topology blocked, traffic forwarded in circles indefinitely

### Network Layer Troubleshooting
#### Network failure
##### Reasons
- network nearly/completely non-functional

#### Suboptimal performance
optimization issues hard to detect, because usually involve multiple layers or single host computer, so takes a lot of time

#### General network issues
##### Reasons
- change of network topology unknowingly

#### Connectivity issues
##### Reasons
- power problems
- environmental problems
- layer 1 problems (cabling, bad ports, ISP problems)

#### Routing table
check for unexpected things (missing routes, etc.)

#### Neighbor issues
check for routers forming neighbor adjaecencies (?)

#### Topology database
Check for unexpected things (missing/unexpected entries)

### Transport Layer Troubleshooting
#### ACLs
![[acl-troubleshooting.png]]

#### NAT for IPv4
![[nat-troubleshooting.png]]

### Application Layer Troubleshooting
![[application-layer-troubleshooting.png]]