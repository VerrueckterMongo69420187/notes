# Bsp 1
## Angabe
IP: 193.4.2.0/24
5 Subnetze -> 3 Bit > neue Netzmaske /27 -> $2^5 - 2 = 30$ Hosts/Subnetz

## Rechnung
193.4.2.NNNH HHHH
		   0 0 0 Netz0
		   0 0 1 Netz1
		   0 1 0 Netz2
		   0 1 1 Netz3
		   1 0 0 Netz4
## Lösungen
### Netz 0
NetID: 193.4.2.0/27 (alle Hostbits 0)
Broadcast: 193.4.2.31 (alle Hostbits 1)
Adressbereich: 193.4.2.1-30
### Netz 1
NetID: 193.4.2.32/27
Broadcast: 193.4.2.63
Adressbereich: 193.4.2.33-62



# Bsp 2
## Angabe
IP: 177.17.17.32
Subnetzmaske: 255.255.248.0 => /21

## Rechnung
NetID:
17        0001 0001
&
248      1111 0000 = 0001 0000

HostID:
17        0001 0001
oder
248      1111 0000 = 1111 0001 =249


# Neues Beispiel

Wien
	10.0000 0|000
	10.0000 0|001
	10.0000 0|010
	10.0000 0|011
	=> 10.0.0.0/21

Moskau
	10.0000 1|000
	10.0000 1|001
	10.0000 1|010
	=> 10.8.0.0/21