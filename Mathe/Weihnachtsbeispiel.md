---
Fach: Mathe
Datum: Thursday, 19.12.2024
Zeit: 10:11
Themen: Nr. 49 - Verteilung
Seiten:
---
---
Ein Rentierschlitten mit **30 Rentieren mit sehr roten Nasen** kommt zum Weihnachtskontrollpunkt. Der Zoll-Wichtel hat den Verdacht, dass einige Rentiere heimlich **Schnee schmuggeln**, um eine weiße Weihnacht zu garantieren.

Er wählt zufällig **3 Rentiere** aus. Wie groß ist die Wahrscheinlichkeit, dass der Zoll-Wichtel bei dieser Auswahl:

a) **kein Rentier** mit geschmuggeltem Schnee erwischt?  
b) **genau 1 Rentier** mit Schnee entdeckt?  
c) **genau 2 Rentiere** mit Schnee schnappt?  
d) **alle 3 Rentiere** mit Schnee erwischt?

Laut der geheimen Statistik des Weihnachtsmanns schmuggeln erfahrungsgemäß **10% der Rentiere** Schnee, um den Wintermagie-Level zu erhöhen.

Lösung auf Tablet