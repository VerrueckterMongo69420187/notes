---
Fach: Mathe
Datum: Thursday, 11.04.2024
Zeit: 07:52
Themen: 
Seiten:
---
---
- Differenzialgleichungen (alles)
	- Textbeispiele, wo man Gleichung finden muss
	- mit richtigen Verfahren ausrechnen, z.B. Trennung der Variablen, Variation der Konstanten, Exponentialansatz,...
- Funktionen in mehreren Variablen mit partiellen Ableitungen, aufstellen einer Tangentialebene
- Matrizenrechnung
	- Addition
	- Subtraktion
	- Multiplikation