---
Fach: NW
Datum: Monday, 22.04.2024
Zeit: 08:15
Themen: 
Seiten:
---
---
# Was ist Kernfusion
- Kernreaktion
- 2 Kerne werden zu 1 Großem
- Deuterium 2H, Tritium 3H
- 
# Wie kommt Kernfusion zu Stande?
- Kerne müssen dicht beieinander sein
- 100.000.000°C
- Sonne: 15.000.000°C -> Tunneleffekt
- Atomkerne sind positiv geladen
# Nutzen der Kernfusion
- Massendefekt
	- Atomkern nach Fusion leichter
	- Kerne Zerlegen benötigt Energie
	- Einstein $E=mc^2$
- Freigesetzte Energie
	- Masse Kern < Gesamtmasse
	- Differenz freigesetzt
# Kernfusion der Sonne
- 15.000.000°C reicht nicht zur Kernfusion alleine
- 71% Wasserstoff, 27% Helium
# Technische Umsetzung
- Auf Erde kann man nicht so viele Protonen wie auf Erde bereitstellen
- Plasma
	- Gemisch aus frei beweglichen positiven Ionen und Elektronen
# Vorteile
- Ressourcenmenge
- Gefahr
	- Gefahr einer unkontrollierten Kettenreaktion ist unwahrscheinlich
# Nachteile
- Technologie in Entwicklung
# Gefahren
- radioaktives Material
- Freisetzung von Neutronenstrahlung
# Tokamak
- magnetischer Plasmaeinfluss
- Ronald Richter
- Erster Tokamak: Sowjetunion
- Donut -> geformte Vakuumkammer
# 