---
Fach: NW
Datum: Monday, 08.04.2024
Zeit: 08:02
Themen: 
Seiten:
---
---
- Lebensdauer von 1,52 Mikrosekunden = $1,52 * 10^{-6}$ Sekunden
- Von uns aus gesehen: Zeitdilatation
- Von den Myonen aus gesehen: Längenkontraktion
- relative Geschwindigkeitsaddition$$v_{ges}=\frac{v_{1}+v_{2}}{1+\frac{v_1*v_2}{{c_0}^2}}$$
- $$v_{ges}=\frac{0,5*c_0+c_{0}}{1+\frac{0,5*c_0*c_0}{{c_0}^2}}=\frac{1,5c_0}{1,5}$$
# Relativistische Massenzunahme
![[Massenzunahme]]
$$p=m*v$$Ruhelage$$p=m_0*v=m*v^2=m*v*\sqrt{1-\frac{v^2}{{c_0}^2}}$$$$m_0=m*\sqrt{1-\frac{v^2}{{c_0}^2}}$$$$m=\frac{m}{\sqrt{1-\frac{v^2}{{c_0}^2}}}$$
-  -> $m>m_0$
- In jeder Masse steckt ein Energievorrat
- Ruheenergie: $E=m_0*c^2$
- "bewegte" Energie: $E=m*c^2$ mit m= bewegte Masse
- Nukleonen sind die Kernteilchen
- Hochenergiephysik, z.B. Beschleuniger
- Große Menge an Energie = Masse
- Aus Energie kann Masse erzeugt werden, z.B. Teilchenbeschleuniger
- Massendefekt
- c ist obere Schranke, Signale höchstens mit c
- Es gibt keinen absolut starren Körper
	- Stange wird irgendwo geschlagen und schwingt, Stoß muss durchwandern durch ganzen Körper
- Kausalitätsprinzip
	- Die Ursache muss der Wirkung stets zeitlich vorausgehen#
	- ![[Lichtkegel]]
	- Man könnte von A mit Überlichtgeschwindigkeit nach B
		- z.B. Radiosender berichtet über Unfall im Radio, Radiowellen kommen aber mit Überlichtgeschwindigkeit, heißt, die Nachricht kommt vor dem Unfall eigentlich an. Der Fahrer macht den Unfall wegen der Nachricht nicht, aber das heißt, dass die Nachricht nie gekommen wäre, heißt der Fahrer hätte doch den Unfall gemacht = Paradoxon.