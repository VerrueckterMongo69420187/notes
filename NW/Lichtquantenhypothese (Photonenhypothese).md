---
Fach: NW
Datum: Monday, 15.04.2024
Zeit: 08:54
Themen: 
Seiten: 327, 328
---
---
Das Licht besteht aus Lichtquanten (Photonen), welche sich bewegen, ohne sich zu teilen. Hat das Licht sie Frequenz f, so ist die Energie des Lichtquants: $E=h*f$ ($h=6.63*10^{-34}Js$...Planck'sches Wirkungsquantum in Joulesekunde)
