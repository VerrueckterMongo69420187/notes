---
Fach: NW
Datum: Monday, 22.04.2024
Zeit: 08:57
Themen: 
Seiten:
---
---
![[NW/Zeichnungen/Comptoneffekt|Comptoneffekt]]
# Welle-Teilchen-Dualismus

| Welleneigenschaft     | Teilcheneigenschaft               |
| --------------------- | --------------------------------- |
| Frequenz f            | Energie $E=h*f$                   |
| Wellenlänge $\lambda$ | Impuls $p=\frac{h}{\lambda}$      |
| Wellenfunktion $\psi$ | Wahrscheinlichkeit $P=\|\psi\|^2$ |
