---
Fach: NW
Datum: Monday, 29.04.2024
Zeit: 08:42
Themen: 
Seiten:
---
---
- Louis de Broglie
- Photonen
- Frequenz f  -> Energie $E=h*f$
- Wellenlänge $\lambda$ -> Impuls $p=\frac{h}{\lambda}$
- Können Teilchen Welleneigenschaften haben? (z.B. Elektronen)
- Teilchen:
	E                -> $f=\frac{E}{h}$
	$p=m*v$- -> $\lambda=\frac{h}{p}=\frac{h}{m*v}$...D De Broglie-Wellenlänge
