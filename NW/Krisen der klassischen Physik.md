---
Fach: NW
Datum: Monday, 26.02.2024
Zeit: 08:11
Themen: 
Seiten: "312"
---
---
# UV-Katastrophe
- ![[Strahlengraf]]
- Lösung durch Max Planck
- $E=h*f$
- Er konnte es nicht erklären
- führte zur Quantenmechanik

# Entdeckung der Radioaktivität
- Zufall spielt große Rolle -> QM
- Einstein sagte: *Gott würfelt nie*
- Wirklichkeit: Gott würfelt bei jeder Gelegenheit
- Widerspruch zum Energiehaltungssatz?
	- -> Relativitätstheorie

# Rutherford'sches Atommodell
- H-Atom müsste flach sein, wenn sein Modell stimmen würde
	- -> Schrödinger QM
- $e^-$ auf der Kreisbahn müssen ständig Energie verlieren und in den Kern stürzen
	- -> QM
- Linienspektren sind nicht erklärbar
	- -> QM

# Äther
- Eigenschaften?
	- Newton sagt, Äther muss sehr sehr dünn und sehr sehr elastisch sein
	- Huygens sagt, Äther muss sehr sehr dicht sein
- Relativitätstheorie
- ![[Aehter]]
- Addition der Geschwindigkeit
	- **Lichtgeschwindigkeit ist nicht in jede Richtung gleich**
- Interferometer-Experiment von Michelson und Morley
- ![[interferometer-experiment.png]]

# Relativitätstheorie
## Relativitätsprinzip
Die **Naturgesetze** nehmen in allen **Inertialsystemen** die gleiche mathematische
Form an. Alle **Inertialsysteme** sind gleichberechtigt.

## Prinzip von der Konstanz der Lichtgeschwindigkeit
Die **Geschwindigkeit des Lichts** hat im Vakuum unabhängig vom Bewegungs-
zustand von Quelle und Beobachter in allen Inertialsystemen immer denselben
Wert $c_0$.

## Relativistische Geschwindigkeitsaddition
Die Gesamtgeschwindigkeit (Relativgeschwindigkeit) für die beiden Einzelgeschwindigkeiten $v_1$ und $v_2$ ergibt sich zu
$$\huge v_{ges}=\frac{v_1+v_2}{1+\frac{v_1*v_2}{{c_0}^2}}$$
![[Buch-Relativitaet-derGleichzeitigkeit.jpg]]
