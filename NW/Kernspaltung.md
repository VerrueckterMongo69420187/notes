---
Fach: NW
Datum: Monday, 22.04.2024
Zeit: 08:35
Themen: 
Seiten:
---
---
# Was ist Kernspaltung?
- Spaltung eines Atomkerns in mehrere Kerne
- Energie wird freigesetzt
- Neutronen werden freigesetzt
- Fritz Straßmann, Otto Hahn
# Entstehung der Kernspaltung
- Spaltung ohne äußeren Einfluss
- Spaltung mit Neutroneneinfluss
# Kettenreaktion
- Kontrollierte Kettenreaktion
- unkontrollierte Kettenreaktion
# Energiegewinnung aus Kernspaltung
- Dampfturbinen werden durch die Erhitzung von Wasser angetrieben und damit wird Strom erzeugt, indem die Turbine an einen Generator angeschlossen ist
- Umsetzung der Kernenergie in nutzbare Energie
- Vor/Nachteile der Kernenergie
	- Erzeugung von Uran-235 sehr CO2-intensiv
	- CO2-arme Stromerzeugung
	- Entsorgung des radioaktiven Abfalls
	- Risiken von Unfällen
# Kernreaktor
- Reaktordruckbehälter -> massives Stahl
- Steuerstäbe -> absorbieren entstehende Neutronen
- Kühlwasserzulauf
- erhitzter Wasserauslauf
- Brennelemente -> Uran-235
# Risiken der Kernspaltung
- Radioaktive Strahlung
- Entsorgung radioaktiven Abfalls
- Unfälle in Kernkraftwerken
- Missbrauch der Kernspaltung durch Waffen