---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
c0 ^yN5mxQH4

c0 ^bJg0G10D

A ^oeqzuXsp

Zukunft von A ^WkMSr6ka

Vergangenheit von A ^iLCk7nAb

B ^fOFczlNS

c ^VEgVVbMy

C ^ecXAqhlJ

D ^3UCF7RLi

E ^wJSmlamL

F ^orMDPY6U

R ^qEmvrJjz

Lichtkegel ^oYFMC66n

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.25",
	"elements": [
		{
			"type": "arrow",
			"version": 24,
			"versionNonce": 576870785,
			"isDeleted": false,
			"id": "iDkSwtVDpqx-WH3dIjIOJ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -288.5,
			"y": 178.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 7,
			"height": 426,
			"seed": 107912751,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1712560361852,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					7,
					-426
				]
			]
		},
		{
			"type": "line",
			"version": 14,
			"versionNonce": 1063850671,
			"isDeleted": false,
			"id": "8lCf0CwvumVUrGqDUykOt",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -112.5,
			"y": -231.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 353,
			"height": 296,
			"seed": 851280225,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1712560364536,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					353,
					296
				]
			]
		},
		{
			"type": "line",
			"version": 27,
			"versionNonce": 1777811105,
			"isDeleted": false,
			"id": "5IrATOy-3k-tNrAtuxPpQ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -54.5,
			"y": 40.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 295,
			"height": 292,
			"seed": 922651169,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1712560565472,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					295,
					-292
				]
			]
		},
		{
			"type": "text",
			"version": 5,
			"versionNonce": 2032789304,
			"isDeleted": false,
			"id": "yN5mxQH4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 249.5,
			"y": -204.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 23.79998779296875,
			"height": 25,
			"seed": 178290831,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126953,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "c0",
			"rawText": "c0",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c0",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 5,
			"versionNonce": 1949648968,
			"isDeleted": false,
			"id": "bJg0G10D",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -131.5,
			"y": -196.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 23.79998779296875,
			"height": 25,
			"seed": 859848975,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126953,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "c0",
			"rawText": "c0",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c0",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 181944376,
			"isDeleted": false,
			"id": "oeqzuXsp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 18.5,
			"y": -76.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13.1199951171875,
			"height": 25,
			"seed": 935051663,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126954,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A",
			"rawText": "A",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "freedraw",
			"version": 3,
			"versionNonce": 1711352417,
			"isDeleted": false,
			"id": "3p7mACrgNwLBZL0HBVUet",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -106.5,
			"y": -236.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 699783151,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560407829,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 5,
			"versionNonce": 246779329,
			"isDeleted": false,
			"id": "fPAW3L3YN9O1LqfZ6t9zf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -106.5,
			"y": -236.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 3,
			"height": 1,
			"seed": 1951861327,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560407956,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-3,
					0
				],
				[
					-3,
					1
				],
				[
					-3,
					1
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 8,
			"versionNonce": 183172289,
			"isDeleted": false,
			"id": "8Wr6H5HUEPc6wv8G_OaSt",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -88.5,
			"y": -264.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 9,
			"height": 4,
			"seed": 1417394415,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560408342,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-1
				],
				[
					3,
					-1
				],
				[
					5,
					-3
				],
				[
					8,
					-4
				],
				[
					9,
					-4
				],
				[
					9,
					-4
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 7,
			"versionNonce": 552341473,
			"isDeleted": false,
			"id": "NW6eSiuabTT39Dm_7KTvy",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -25.5,
			"y": -271.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14,
			"height": 5,
			"seed": 1653643759,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560408538,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					5,
					-1
				],
				[
					7,
					-2
				],
				[
					11,
					-4
				],
				[
					14,
					-5
				],
				[
					14,
					-5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 8,
			"versionNonce": 451239649,
			"isDeleted": false,
			"id": "RJSV0lCoQG6CgOuOFLIBG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 49.5,
			"y": -281.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14,
			"height": 4,
			"seed": 1282206927,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560408773,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					-1
				],
				[
					6,
					-2
				],
				[
					7,
					-2
				],
				[
					9,
					-3
				],
				[
					14,
					-4
				],
				[
					14,
					-4
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 6,
			"versionNonce": 1970471457,
			"isDeleted": false,
			"id": "EltKyFlOEp-5WEDXV20c4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 121.5,
			"y": -291.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 8,
			"height": 0,
			"seed": 287829455,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560408953,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					0
				],
				[
					3,
					0
				],
				[
					8,
					0
				],
				[
					8,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 12,
			"versionNonce": 404224161,
			"isDeleted": false,
			"id": "qKqizL1jJ8LDbPfWYoNji",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 177.5,
			"y": -288.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 35,
			"height": 8,
			"seed": 2084898447,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560409226,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					1
				],
				[
					5,
					2
				],
				[
					10,
					2
				],
				[
					15,
					4
				],
				[
					21,
					5
				],
				[
					27,
					6
				],
				[
					30,
					7
				],
				[
					33,
					8
				],
				[
					35,
					8
				],
				[
					35,
					8
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 5,
			"versionNonce": 2038658049,
			"isDeleted": false,
			"id": "rLaCqJfb2vMCvXX0JHarj",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 229.5,
			"y": -269.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 3,
			"height": 1,
			"seed": 1308526607,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560409403,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					1
				],
				[
					3,
					1
				],
				[
					3,
					1
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 43,
			"versionNonce": 1319260833,
			"isDeleted": false,
			"id": "GrLTkrLRMPXhz2Ziv04vO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -104.5,
			"y": -231.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 328,
			"height": 13,
			"seed": 1492950703,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560413884,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4,
					0
				],
				[
					8,
					0
				],
				[
					12,
					0
				],
				[
					16,
					0
				],
				[
					21,
					-1
				],
				[
					27,
					-1
				],
				[
					31,
					-2
				],
				[
					36,
					-2
				],
				[
					41,
					-2
				],
				[
					46,
					-3
				],
				[
					54,
					-3
				],
				[
					68,
					-3
				],
				[
					85,
					-3
				],
				[
					104,
					-3
				],
				[
					122,
					-2
				],
				[
					138,
					-1
				],
				[
					151,
					0
				],
				[
					164,
					0
				],
				[
					176,
					0
				],
				[
					189,
					0
				],
				[
					202,
					0
				],
				[
					219,
					0
				],
				[
					238,
					-2
				],
				[
					255,
					-3
				],
				[
					271,
					-4
				],
				[
					285,
					-5
				],
				[
					295,
					-5
				],
				[
					300,
					-6
				],
				[
					301,
					-6
				],
				[
					303,
					-7
				],
				[
					306,
					-8
				],
				[
					309,
					-8
				],
				[
					313,
					-9
				],
				[
					315,
					-10
				],
				[
					318,
					-10
				],
				[
					322,
					-12
				],
				[
					324,
					-12
				],
				[
					326,
					-13
				],
				[
					327,
					-13
				],
				[
					328,
					-13
				],
				[
					328,
					-13
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 8,
			"versionNonce": 1984731983,
			"isDeleted": false,
			"id": "BZzXV7spa0bPU2ify30Lh",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -66.5,
			"y": -209.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 13,
			"seed": 2082490881,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560458410,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					3,
					-4
				],
				[
					11,
					-9
				],
				[
					17,
					-12
				],
				[
					19,
					-13
				],
				[
					19,
					-13
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 8,
			"versionNonce": 898702415,
			"isDeleted": false,
			"id": "tfJBXpcA5Frf5g57rZlLk",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -40.5,
			"y": -183.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 23,
			"height": 20,
			"seed": 1353021697,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560458783,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					-2
				],
				[
					9,
					-8
				],
				[
					14,
					-13
				],
				[
					20,
					-17
				],
				[
					23,
					-20
				],
				[
					23,
					-20
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 10,
			"versionNonce": 32167311,
			"isDeleted": false,
			"id": "3P4EFOX9cPBwFUKqVffBW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -20.5,
			"y": -175.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 65,
			"height": 28,
			"seed": 2043389953,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560459096,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1
				],
				[
					4,
					-2
				],
				[
					14,
					-9
				],
				[
					29,
					-16
				],
				[
					46,
					-23
				],
				[
					60,
					-27
				],
				[
					65,
					-27
				],
				[
					65,
					-27
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 10,
			"versionNonce": 296353487,
			"isDeleted": false,
			"id": "_TEXpq82KsgPhyNfxxJ9E",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 24.5,
			"y": -161.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 70,
			"height": 53,
			"seed": 1584908993,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560459410,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					6,
					-6
				],
				[
					7,
					-8
				],
				[
					18,
					-19
				],
				[
					32,
					-31
				],
				[
					48,
					-42
				],
				[
					67,
					-52
				],
				[
					70,
					-53
				],
				[
					70,
					-53
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 11,
			"versionNonce": 150126127,
			"isDeleted": false,
			"id": "V78IrgHTolvTU305RY3nR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 42.5,
			"y": -154.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 75,
			"height": 43,
			"seed": 1398504833,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560459792,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					0
				],
				[
					10,
					-3
				],
				[
					22,
					-12
				],
				[
					36,
					-20
				],
				[
					49,
					-29
				],
				[
					62,
					-36
				],
				[
					71,
					-40
				],
				[
					75,
					-43
				],
				[
					75,
					-43
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 12,
			"versionNonce": 933875631,
			"isDeleted": false,
			"id": "kl9gyS7CqYFMGYin5aRCA",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 76.5,
			"y": -125.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 109,
			"height": 89,
			"seed": 274927649,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560460209,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					8,
					-9
				],
				[
					18,
					-21
				],
				[
					35,
					-40
				],
				[
					58,
					-61
				],
				[
					77,
					-73
				],
				[
					92,
					-81
				],
				[
					102,
					-86
				],
				[
					108,
					-88
				],
				[
					109,
					-89
				],
				[
					109,
					-89
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 44,
			"versionNonce": 608118063,
			"isDeleted": false,
			"id": "uiYP1ZXcZ7KW3HbLsQRM4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 133.5,
			"y": -193.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 126,
			"height": 148,
			"seed": 388171425,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560463001,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					5,
					4
				],
				[
					13,
					11
				],
				[
					23,
					18
				],
				[
					35,
					25
				],
				[
					46,
					34
				],
				[
					60,
					47
				],
				[
					63,
					50
				],
				[
					68,
					56
				],
				[
					71,
					59
				],
				[
					73,
					61
				],
				[
					74,
					64
				],
				[
					76,
					68
				],
				[
					78,
					73
				],
				[
					80,
					79
				],
				[
					82,
					86
				],
				[
					85,
					94
				],
				[
					88,
					101
				],
				[
					91,
					106
				],
				[
					93,
					111
				],
				[
					96,
					115
				],
				[
					98,
					119
				],
				[
					100,
					124
				],
				[
					103,
					129
				],
				[
					104,
					132
				],
				[
					105,
					136
				],
				[
					107,
					140
				],
				[
					108,
					143
				],
				[
					108,
					145
				],
				[
					108,
					147
				],
				[
					109,
					147
				],
				[
					109,
					148
				],
				[
					111,
					143
				],
				[
					114,
					136
				],
				[
					117,
					129
				],
				[
					120,
					123
				],
				[
					122,
					120
				],
				[
					125,
					117
				],
				[
					125,
					116
				],
				[
					126,
					116
				],
				[
					126,
					118
				],
				[
					126,
					122
				],
				[
					126,
					122
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 14,
			"versionNonce": 1227560687,
			"isDeleted": false,
			"id": "xDA8e4-X0SYVWilCZyu2h",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 233.5,
			"y": -47.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 23,
			"height": 16,
			"seed": 2057009441,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560463460,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					0
				],
				[
					-3,
					-1
				],
				[
					-5,
					-2
				],
				[
					-7,
					-3
				],
				[
					-8,
					-5
				],
				[
					-11,
					-7
				],
				[
					-15,
					-11
				],
				[
					-19,
					-14
				],
				[
					-21,
					-16
				],
				[
					-22,
					-16
				],
				[
					-23,
					-16
				],
				[
					-23,
					-16
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 16,
			"versionNonce": 1576337224,
			"isDeleted": false,
			"id": "WkMSr6ka",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 259.5,
			"y": -16.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 142.7598876953125,
			"height": 25,
			"seed": 2102737761,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126955,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Zukunft von A",
			"rawText": "Zukunft von A",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Zukunft von A",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "freedraw",
			"version": 119,
			"versionNonce": 1342760879,
			"isDeleted": false,
			"id": "K4O5DIhGhnhysc7LoWBTe",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -52.5,
			"y": 49.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 292,
			"height": 69,
			"seed": 160543105,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560479886,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					4
				],
				[
					5,
					10
				],
				[
					10,
					16
				],
				[
					14,
					22
				],
				[
					19,
					28
				],
				[
					26,
					34
				],
				[
					32,
					38
				],
				[
					44,
					42
				],
				[
					57,
					45
				],
				[
					70,
					46
				],
				[
					84,
					48
				],
				[
					96,
					49
				],
				[
					106,
					50
				],
				[
					114,
					51
				],
				[
					119,
					51
				],
				[
					126,
					51
				],
				[
					135,
					52
				],
				[
					143,
					53
				],
				[
					152,
					54
				],
				[
					160,
					55
				],
				[
					168,
					56
				],
				[
					174,
					57
				],
				[
					181,
					58
				],
				[
					187,
					59
				],
				[
					194,
					59
				],
				[
					201,
					60
				],
				[
					209,
					60
				],
				[
					218,
					60
				],
				[
					226,
					60
				],
				[
					234,
					61
				],
				[
					242,
					61
				],
				[
					244,
					61
				],
				[
					247,
					61
				],
				[
					250,
					60
				],
				[
					254,
					58
				],
				[
					257,
					57
				],
				[
					260,
					56
				],
				[
					265,
					54
				],
				[
					268,
					52
				],
				[
					271,
					50
				],
				[
					273,
					49
				],
				[
					275,
					48
				],
				[
					277,
					46
				],
				[
					278,
					44
				],
				[
					280,
					41
				],
				[
					282,
					40
				],
				[
					285,
					38
				],
				[
					288,
					36
				],
				[
					289,
					35
				],
				[
					290,
					34
				],
				[
					290,
					33
				],
				[
					290,
					31
				],
				[
					291,
					29
				],
				[
					292,
					25
				],
				[
					292,
					24
				],
				[
					292,
					23
				],
				[
					291,
					21
				],
				[
					289,
					20
				],
				[
					287,
					19
				],
				[
					286,
					18
				],
				[
					285,
					17
				],
				[
					282,
					16
				],
				[
					278,
					15
				],
				[
					276,
					14
				],
				[
					272,
					13
				],
				[
					269,
					12
				],
				[
					264,
					11
				],
				[
					257,
					8
				],
				[
					249,
					7
				],
				[
					240,
					4
				],
				[
					229,
					2
				],
				[
					221,
					1
				],
				[
					214,
					-1
				],
				[
					208,
					-1
				],
				[
					202,
					-1
				],
				[
					196,
					-1
				],
				[
					190,
					-2
				],
				[
					182,
					-2
				],
				[
					173,
					-2
				],
				[
					162,
					-2
				],
				[
					159,
					-2
				],
				[
					153,
					-3
				],
				[
					147,
					-3
				],
				[
					142,
					-3
				],
				[
					137,
					-3
				],
				[
					133,
					-4
				],
				[
					128,
					-4
				],
				[
					122,
					-5
				],
				[
					116,
					-6
				],
				[
					109,
					-7
				],
				[
					101,
					-8
				],
				[
					92,
					-8
				],
				[
					84,
					-8
				],
				[
					78,
					-8
				],
				[
					72,
					-8
				],
				[
					68,
					-8
				],
				[
					64,
					-8
				],
				[
					60,
					-8
				],
				[
					56,
					-8
				],
				[
					49,
					-8
				],
				[
					47,
					-8
				],
				[
					41,
					-7
				],
				[
					35,
					-7
				],
				[
					28,
					-7
				],
				[
					23,
					-7
				],
				[
					19,
					-7
				],
				[
					17,
					-6
				],
				[
					15,
					-6
				],
				[
					14,
					-5
				],
				[
					13,
					-5
				],
				[
					11,
					-5
				],
				[
					9,
					-5
				],
				[
					8,
					-5
				],
				[
					6,
					-4
				],
				[
					5,
					-4
				],
				[
					4,
					-3
				],
				[
					4,
					-3
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 35,
			"versionNonce": 352335375,
			"isDeleted": false,
			"id": "BHzAUu2ozWni29D_bODXO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 7.5,
			"y": 17.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 57,
			"height": 73,
			"seed": 1976856225,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560482490,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-1
				],
				[
					8,
					-2
				],
				[
					13,
					-3
				],
				[
					18,
					-5
				],
				[
					22,
					-7
				],
				[
					25,
					-10
				],
				[
					27,
					-13
				],
				[
					28,
					-15
				],
				[
					28,
					-19
				],
				[
					27,
					-24
				],
				[
					25,
					-28
				],
				[
					23,
					-32
				],
				[
					21,
					-34
				],
				[
					21,
					-35
				],
				[
					20,
					-35
				],
				[
					22,
					-37
				],
				[
					29,
					-39
				],
				[
					38,
					-41
				],
				[
					46,
					-42
				],
				[
					53,
					-44
				],
				[
					56,
					-46
				],
				[
					57,
					-49
				],
				[
					57,
					-52
				],
				[
					55,
					-56
				],
				[
					54,
					-59
				],
				[
					53,
					-62
				],
				[
					53,
					-63
				],
				[
					53,
					-65
				],
				[
					54,
					-66
				],
				[
					54,
					-69
				],
				[
					54,
					-71
				],
				[
					54,
					-73
				],
				[
					54,
					-73
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 38,
			"versionNonce": 1385702095,
			"isDeleted": false,
			"id": "-39ikay6RNUM3u7yjh0Qe",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 157.5,
			"y": 30.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 79,
			"height": 102,
			"seed": 1056817729,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560484997,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					-1
				],
				[
					-6,
					-2
				],
				[
					-9,
					-4
				],
				[
					-14,
					-5
				],
				[
					-20,
					-9
				],
				[
					-28,
					-13
				],
				[
					-36,
					-18
				],
				[
					-42,
					-23
				],
				[
					-49,
					-29
				],
				[
					-53,
					-33
				],
				[
					-55,
					-36
				],
				[
					-56,
					-38
				],
				[
					-56,
					-42
				],
				[
					-55,
					-46
				],
				[
					-54,
					-50
				],
				[
					-52,
					-53
				],
				[
					-50,
					-56
				],
				[
					-49,
					-59
				],
				[
					-49,
					-63
				],
				[
					-49,
					-67
				],
				[
					-51,
					-72
				],
				[
					-54,
					-76
				],
				[
					-58,
					-80
				],
				[
					-62,
					-82
				],
				[
					-66,
					-84
				],
				[
					-68,
					-85
				],
				[
					-69,
					-86
				],
				[
					-70,
					-87
				],
				[
					-72,
					-88
				],
				[
					-74,
					-91
				],
				[
					-76,
					-94
				],
				[
					-77,
					-98
				],
				[
					-78,
					-100
				],
				[
					-78,
					-102
				],
				[
					-79,
					-102
				],
				[
					-79,
					-102
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 38,
			"versionNonce": 295993231,
			"isDeleted": false,
			"id": "91ojEnawL8LMHzniZ7kEi",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 13.5,
			"y": 14.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13,
			"height": 42,
			"seed": 1571690881,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560491675,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					0
				],
				[
					-3,
					0
				],
				[
					-5,
					0
				],
				[
					-7,
					1
				],
				[
					-8,
					1
				],
				[
					-8,
					3
				],
				[
					-8,
					6
				],
				[
					-8,
					9
				],
				[
					-8,
					10
				],
				[
					-8,
					11
				],
				[
					-8,
					14
				],
				[
					-8,
					15
				],
				[
					-8,
					17
				],
				[
					-7,
					18
				],
				[
					-6,
					18
				],
				[
					-5,
					19
				],
				[
					-3,
					20
				],
				[
					-2,
					21
				],
				[
					-1,
					22
				],
				[
					1,
					24
				],
				[
					2,
					27
				],
				[
					3,
					29
				],
				[
					4,
					31
				],
				[
					5,
					32
				],
				[
					5,
					33
				],
				[
					5,
					34
				],
				[
					5,
					36
				],
				[
					5,
					38
				],
				[
					4,
					39
				],
				[
					3,
					39
				],
				[
					2,
					40
				],
				[
					0,
					40
				],
				[
					-1,
					41
				],
				[
					-2,
					41
				],
				[
					-2,
					42
				],
				[
					-2,
					42
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 30,
			"versionNonce": 909953871,
			"isDeleted": false,
			"id": "S4kb41nn_phDtMwkS5sWq",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 16.5,
			"y": 63.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 13,
			"seed": 1188495553,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560492426,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					-2
				],
				[
					-4,
					-4
				],
				[
					-5,
					-6
				],
				[
					-7,
					-6
				],
				[
					-10,
					-6
				],
				[
					-13,
					-1
				],
				[
					-14,
					3
				],
				[
					-15,
					5
				],
				[
					-15,
					6
				],
				[
					-14,
					7
				],
				[
					-13,
					7
				],
				[
					-11,
					7
				],
				[
					-8,
					6
				],
				[
					-6,
					1
				],
				[
					-5,
					-2
				],
				[
					-5,
					-3
				],
				[
					-5,
					-5
				],
				[
					-6,
					-5
				],
				[
					-7,
					-5
				],
				[
					-8,
					-2
				],
				[
					-8,
					0
				],
				[
					-8,
					1
				],
				[
					-8,
					2
				],
				[
					-7,
					2
				],
				[
					-6,
					2
				],
				[
					-6,
					0
				],
				[
					-7,
					-2
				],
				[
					-7,
					-2
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 31,
			"versionNonce": 1546407215,
			"isDeleted": false,
			"id": "PFSgjdGGiaa53Egf21zGk",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 168.5,
			"y": 70.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 78,
			"height": 214,
			"seed": 1966491905,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560496921,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					3
				],
				[
					6,
					9
				],
				[
					14,
					19
				],
				[
					26,
					32
				],
				[
					36,
					46
				],
				[
					43,
					59
				],
				[
					46,
					72
				],
				[
					48,
					87
				],
				[
					52,
					115
				],
				[
					55,
					154
				],
				[
					60,
					180
				],
				[
					63,
					196
				],
				[
					66,
					204
				],
				[
					68,
					209
				],
				[
					70,
					213
				],
				[
					70,
					214
				],
				[
					70,
					213
				],
				[
					68,
					211
				],
				[
					67,
					210
				],
				[
					66,
					206
				],
				[
					66,
					200
				],
				[
					67,
					190
				],
				[
					70,
					180
				],
				[
					72,
					173
				],
				[
					74,
					169
				],
				[
					76,
					166
				],
				[
					77,
					165
				],
				[
					78,
					165
				],
				[
					78,
					165
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 17,
			"versionNonce": 855167311,
			"isDeleted": false,
			"id": "5dwvR0nb6AOFc8fnP5TNH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 231.5,
			"y": 282.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 29,
			"height": 33,
			"seed": 1820971297,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560497441,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-2,
					0
				],
				[
					-3,
					0
				],
				[
					-4,
					0
				],
				[
					-6,
					0
				],
				[
					-8,
					-1
				],
				[
					-12,
					-7
				],
				[
					-16,
					-13
				],
				[
					-21,
					-20
				],
				[
					-25,
					-25
				],
				[
					-27,
					-28
				],
				[
					-28,
					-30
				],
				[
					-28,
					-32
				],
				[
					-29,
					-33
				],
				[
					-29,
					-33
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 22,
			"versionNonce": 1733523768,
			"isDeleted": false,
			"id": "iLCk7nAb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 209.5,
			"y": 319.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 193.37985229492188,
			"height": 25,
			"seed": 1135239937,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126956,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Vergangenheit von A",
			"rawText": "Vergangenheit von A",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Vergangenheit von A",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "freedraw",
			"version": 20,
			"versionNonce": 1849013359,
			"isDeleted": false,
			"id": "32WhMiFf_-FvEbfmA7O2k",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -136.5,
			"y": -62.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10,
			"height": 9,
			"seed": 1957036129,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560509935,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					4
				],
				[
					1,
					7
				],
				[
					4,
					8
				],
				[
					6,
					9
				],
				[
					7,
					8
				],
				[
					6,
					5
				],
				[
					2,
					3
				],
				[
					0,
					2
				],
				[
					-2,
					1
				],
				[
					-2,
					2
				],
				[
					-2,
					4
				],
				[
					-1,
					7
				],
				[
					-1,
					8
				],
				[
					0,
					8
				],
				[
					0,
					7
				],
				[
					-2,
					5
				],
				[
					-3,
					4
				],
				[
					-3,
					4
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 26,
			"versionNonce": 77977160,
			"isDeleted": false,
			"id": "fOFczlNS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -167.5,
			"y": -89.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14.539993286132812,
			"height": 25,
			"seed": 1242162657,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126957,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "freedraw",
			"version": 84,
			"versionNonce": 55821423,
			"isDeleted": false,
			"id": "juXf4uNRdBF606jpEreIc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 71.5,
			"y": -59.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 30,
			"height": 31,
			"seed": 633895009,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560528960,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-3,
					0
				],
				[
					-5,
					0
				],
				[
					-7,
					0
				],
				[
					-8,
					0
				],
				[
					-9,
					0
				],
				[
					-10,
					-3
				],
				[
					-11,
					-8
				],
				[
					-11,
					-13
				],
				[
					-11,
					-17
				],
				[
					-9,
					-21
				],
				[
					-6,
					-24
				],
				[
					-4,
					-26
				],
				[
					-1,
					-27
				],
				[
					1,
					-28
				],
				[
					3,
					-29
				],
				[
					6,
					-30
				],
				[
					8,
					-30
				],
				[
					10,
					-31
				],
				[
					11,
					-31
				],
				[
					12,
					-31
				],
				[
					8,
					-29
				],
				[
					4,
					-27
				],
				[
					1,
					-26
				],
				[
					0,
					-25
				],
				[
					-1,
					-23
				],
				[
					-2,
					-22
				],
				[
					-2,
					-21
				],
				[
					-2,
					-19
				],
				[
					-2,
					-18
				],
				[
					-1,
					-17
				],
				[
					-1,
					-16
				],
				[
					2,
					-17
				],
				[
					3,
					-20
				],
				[
					4,
					-21
				],
				[
					4,
					-22
				],
				[
					3,
					-21
				],
				[
					3,
					-19
				],
				[
					3,
					-18
				],
				[
					3,
					-17
				],
				[
					4,
					-17
				],
				[
					6,
					-17
				],
				[
					8,
					-17
				],
				[
					9,
					-17
				],
				[
					10,
					-17
				],
				[
					11,
					-18
				],
				[
					11,
					-21
				],
				[
					11,
					-22
				],
				[
					10,
					-23
				],
				[
					9,
					-23
				],
				[
					8,
					-23
				],
				[
					5,
					-20
				],
				[
					3,
					-17
				],
				[
					1,
					-13
				],
				[
					0,
					-10
				],
				[
					0,
					-9
				],
				[
					6,
					-9
				],
				[
					12,
					-10
				],
				[
					16,
					-12
				],
				[
					19,
					-14
				],
				[
					19,
					-17
				],
				[
					19,
					-19
				],
				[
					18,
					-21
				],
				[
					16,
					-22
				],
				[
					14,
					-22
				],
				[
					12,
					-21
				],
				[
					11,
					-18
				],
				[
					10,
					-14
				],
				[
					10,
					-13
				],
				[
					10,
					-10
				],
				[
					11,
					-9
				],
				[
					12,
					-9
				],
				[
					14,
					-10
				],
				[
					14,
					-13
				],
				[
					13,
					-17
				],
				[
					9,
					-21
				],
				[
					7,
					-22
				],
				[
					5,
					-22
				],
				[
					3,
					-21
				],
				[
					3,
					-17
				],
				[
					3,
					-14
				],
				[
					3,
					-13
				],
				[
					3,
					-13
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 1644139064,
			"isDeleted": false,
			"id": "VEgVVbMy",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -277.5,
			"y": -293.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10.039993286132812,
			"height": 25,
			"seed": 1521931745,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126958,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "c",
			"rawText": "c",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 1831062856,
			"isDeleted": false,
			"id": "ecXAqhlJ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 29.5,
			"y": 75.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.879989624023438,
			"height": 25,
			"seed": 2005361967,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126959,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "C",
			"rawText": "C",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 1103137592,
			"isDeleted": false,
			"id": "3UCF7RLi",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 173.5,
			"y": 28.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 15.599990844726562,
			"height": 25,
			"seed": 507289487,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126960,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "D",
			"rawText": "D",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "D",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 550167624,
			"isDeleted": false,
			"id": "wJSmlamL",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 33.5,
			"y": -148.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13.479995727539062,
			"height": 25,
			"seed": 406549999,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126961,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "E",
			"rawText": "E",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "E",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 1358586936,
			"isDeleted": false,
			"id": "orMDPY6U",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 54.5,
			"y": -127.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 11.479995727539062,
			"height": 25,
			"seed": 672753743,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126962,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "F",
			"rawText": "F",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "F",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "freedraw",
			"version": 46,
			"versionNonce": 1180583329,
			"isDeleted": false,
			"id": "hzSW8Kotk832SggmtGei-",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -186.5,
			"y": -167.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 10,
			"height": 9,
			"seed": 2054483279,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712560579298,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					1
				],
				[
					-1,
					2
				],
				[
					-1,
					4
				],
				[
					1,
					4
				],
				[
					3,
					5
				],
				[
					5,
					5
				],
				[
					6,
					5
				],
				[
					7,
					5
				],
				[
					7,
					3
				],
				[
					7,
					2
				],
				[
					7,
					1
				],
				[
					6,
					1
				],
				[
					5,
					0
				],
				[
					4,
					0
				],
				[
					4,
					1
				],
				[
					5,
					4
				],
				[
					7,
					6
				],
				[
					8,
					7
				],
				[
					9,
					7
				],
				[
					9,
					6
				],
				[
					9,
					4
				],
				[
					8,
					2
				],
				[
					7,
					2
				],
				[
					6,
					2
				],
				[
					5,
					2
				],
				[
					5,
					4
				],
				[
					5,
					5
				],
				[
					5,
					6
				],
				[
					5,
					5
				],
				[
					4,
					4
				],
				[
					3,
					4
				],
				[
					2,
					4
				],
				[
					2,
					5
				],
				[
					2,
					6
				],
				[
					2,
					8
				],
				[
					4,
					9
				],
				[
					5,
					9
				],
				[
					6,
					7
				],
				[
					6,
					4
				],
				[
					6,
					2
				],
				[
					4,
					1
				],
				[
					2,
					1
				],
				[
					0,
					2
				],
				[
					0,
					2
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 4,
			"versionNonce": 494345032,
			"isDeleted": false,
			"id": "qEmvrJjz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -208.5,
			"y": -192.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13.55999755859375,
			"height": 25,
			"seed": 213330703,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126964,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "R",
			"rawText": "R",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "R",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 51,
			"versionNonce": 1838226744,
			"isDeleted": false,
			"id": "oYFMC66n",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -205.5,
			"y": -362.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 217.44564819335938,
			"height": 57.54660371279273,
			"seed": 1063760239,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1712562126965,
			"link": null,
			"locked": false,
			"fontSize": 46.03728297023419,
			"fontFamily": 1,
			"text": "Lichtkegel",
			"rawText": "Lichtkegel",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Lichtkegel",
			"lineHeight": 1.25,
			"baseline": 40
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 658.7102863551221,
		"scrollY": 390.9644097222222,
		"zoom": {
			"value": 1.35
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%