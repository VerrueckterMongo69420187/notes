---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Strahlungsintensivität ^r52CnULD

Frequenz ^aATHtyLW

Theorie ^JPHuWAR4

Experiment ^AApkucTg

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.20",
	"elements": [
		{
			"id": "TCbxD2TDNReg0c-vGiXWx",
			"type": "line",
			"x": -224,
			"y": -351.2421875,
			"width": 12,
			"height": 492,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1844236994,
			"version": 52,
			"versionNonce": 111689438,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931702195,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-12,
					492
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "Rz_6o2fZE84dAE4x3gkaA",
			"type": "line",
			"x": -289,
			"y": 81.7578125,
			"width": 628,
			"height": 6,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1938659394,
			"version": 52,
			"versionNonce": 1619980638,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931708903,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					628,
					6
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "8wBRE1EiApugHcFxLzvEX",
			"type": "freedraw",
			"x": -228,
			"y": 80.7578125,
			"width": 374,
			"height": 431,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1089885854,
			"version": 104,
			"versionNonce": 94222530,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931719280,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-2
				],
				[
					3,
					-3
				],
				[
					6,
					-4
				],
				[
					10,
					-7
				],
				[
					16,
					-9
				],
				[
					24,
					-12
				],
				[
					38,
					-15
				],
				[
					53,
					-19
				],
				[
					67,
					-22
				],
				[
					81,
					-26
				],
				[
					94,
					-29
				],
				[
					103,
					-31
				],
				[
					108,
					-32
				],
				[
					114,
					-33
				],
				[
					119,
					-34
				],
				[
					125,
					-36
				],
				[
					133,
					-40
				],
				[
					142,
					-45
				],
				[
					152,
					-50
				],
				[
					161,
					-54
				],
				[
					170,
					-58
				],
				[
					177,
					-61
				],
				[
					186,
					-66
				],
				[
					195,
					-70
				],
				[
					204,
					-75
				],
				[
					213,
					-80
				],
				[
					223,
					-86
				],
				[
					230,
					-90
				],
				[
					235,
					-94
				],
				[
					241,
					-98
				],
				[
					245,
					-102
				],
				[
					249,
					-106
				],
				[
					251,
					-111
				],
				[
					255,
					-114
				],
				[
					258,
					-117
				],
				[
					262,
					-121
				],
				[
					264,
					-124
				],
				[
					267,
					-128
				],
				[
					270,
					-131
				],
				[
					274,
					-135
				],
				[
					278,
					-140
				],
				[
					284,
					-147
				],
				[
					290,
					-153
				],
				[
					295,
					-159
				],
				[
					301,
					-164
				],
				[
					305,
					-168
				],
				[
					309,
					-172
				],
				[
					312,
					-178
				],
				[
					313,
					-180
				],
				[
					315,
					-184
				],
				[
					318,
					-190
				],
				[
					321,
					-195
				],
				[
					324,
					-201
				],
				[
					326,
					-207
				],
				[
					329,
					-214
				],
				[
					332,
					-220
				],
				[
					335,
					-225
				],
				[
					337,
					-229
				],
				[
					340,
					-234
				],
				[
					341,
					-238
				],
				[
					343,
					-243
				],
				[
					346,
					-248
				],
				[
					347,
					-254
				],
				[
					349,
					-260
				],
				[
					351,
					-267
				],
				[
					353,
					-274
				],
				[
					355,
					-280
				],
				[
					357,
					-287
				],
				[
					359,
					-292
				],
				[
					361,
					-297
				],
				[
					363,
					-303
				],
				[
					363,
					-307
				],
				[
					365,
					-313
				],
				[
					365,
					-318
				],
				[
					366,
					-329
				],
				[
					366,
					-333
				],
				[
					366,
					-342
				],
				[
					366,
					-350
				],
				[
					367,
					-356
				],
				[
					368,
					-363
				],
				[
					370,
					-371
				],
				[
					371,
					-377
				],
				[
					372,
					-384
				],
				[
					372,
					-389
				],
				[
					372,
					-394
				],
				[
					373,
					-397
				],
				[
					373,
					-400
				],
				[
					374,
					-402
				],
				[
					374,
					-405
				],
				[
					374,
					-406
				],
				[
					374,
					-408
				],
				[
					374,
					-409
				],
				[
					374,
					-410
				],
				[
					374,
					-412
				],
				[
					374,
					-414
				],
				[
					374,
					-417
				],
				[
					374,
					-421
				],
				[
					374,
					-425
				],
				[
					374,
					-428
				],
				[
					374,
					-430
				],
				[
					374,
					-431
				],
				[
					374,
					-431
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				374,
				-431
			]
		},
		{
			"id": "pJnvCU93Zn7kk8MBv7R5F",
			"type": "freedraw",
			"x": -230,
			"y": -355.2421875,
			"width": 20,
			"height": 20,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1877962910,
			"version": 10,
			"versionNonce": 338562626,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931721943,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1
				],
				[
					0,
					5
				],
				[
					3,
					9
				],
				[
					8,
					14
				],
				[
					14,
					17
				],
				[
					17,
					19
				],
				[
					20,
					20
				],
				[
					20,
					20
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				20,
				20
			]
		},
		{
			"id": "x8VWPnfQyJrZBXfX1SInj",
			"type": "freedraw",
			"x": -234,
			"y": -348.2421875,
			"width": 19,
			"height": 13,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1945953054,
			"version": 10,
			"versionNonce": 2136572866,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931722332,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4,
					2
				],
				[
					-8,
					4
				],
				[
					-11,
					5
				],
				[
					-14,
					8
				],
				[
					-16,
					10
				],
				[
					-18,
					12
				],
				[
					-19,
					13
				],
				[
					-19,
					13
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-19,
				13
			]
		},
		{
			"id": "WP3lll_vZ3QAr8zQtZDq0",
			"type": "freedraw",
			"x": 339,
			"y": 91.7578125,
			"width": 21,
			"height": 30,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1985295774,
			"version": 16,
			"versionNonce": 318291906,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931723640,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-2,
					0
				],
				[
					-4,
					0
				],
				[
					-7,
					-2
				],
				[
					-11,
					-8
				],
				[
					-15,
					-15
				],
				[
					-17,
					-21
				],
				[
					-19,
					-26
				],
				[
					-21,
					-29
				],
				[
					-21,
					-30
				],
				[
					-21,
					-29
				],
				[
					-21,
					-25
				],
				[
					-19,
					-22
				],
				[
					-19,
					-22
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-19,
				-22
			]
		},
		{
			"id": "rreqsHOSuWtSq4vDMjA0v",
			"type": "freedraw",
			"x": 335,
			"y": 95.7578125,
			"width": 56,
			"height": 21,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 23542174,
			"version": 14,
			"versionNonce": 678339650,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931724134,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4,
					1
				],
				[
					-8,
					2
				],
				[
					-16,
					3
				],
				[
					-25,
					5
				],
				[
					-34,
					9
				],
				[
					-44,
					13
				],
				[
					-50,
					16
				],
				[
					-55,
					18
				],
				[
					-56,
					19
				],
				[
					-56,
					20
				],
				[
					-56,
					21
				],
				[
					-56,
					21
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-56,
				21
			]
		},
		{
			"id": "r52CnULD",
			"type": "text",
			"x": -453,
			"y": -323.2421875,
			"width": 213.43978881835938,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1278176542,
			"version": 55,
			"versionNonce": 934214430,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931744828,
			"link": null,
			"locked": false,
			"text": "Strahlungsintensivität",
			"rawText": "Strahlungsintensivität",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Strahlungsintensivität",
			"lineHeight": 1.25
		},
		{
			"id": "aATHtyLW",
			"type": "text",
			"x": 223,
			"y": 129.7578125,
			"width": 85.01992797851562,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1701137310,
			"version": 11,
			"versionNonce": 1560279746,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931752905,
			"link": null,
			"locked": false,
			"text": "Frequenz",
			"rawText": "Frequenz",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Frequenz",
			"lineHeight": 1.25
		},
		{
			"id": "jVu_lRxhejrmVGUKfBqJ8",
			"type": "freedraw",
			"x": -231,
			"y": 82.7578125,
			"width": 546,
			"height": 165,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 32443102,
			"version": 198,
			"versionNonce": 1366612226,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931785366,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					0
				],
				[
					6,
					0
				],
				[
					12,
					0
				],
				[
					20,
					-1
				],
				[
					30,
					-3
				],
				[
					38,
					-4
				],
				[
					44,
					-5
				],
				[
					46,
					-5
				],
				[
					47,
					-5
				],
				[
					48,
					-6
				],
				[
					49,
					-7
				],
				[
					52,
					-8
				],
				[
					55,
					-8
				],
				[
					58,
					-9
				],
				[
					60,
					-9
				],
				[
					64,
					-10
				],
				[
					69,
					-11
				],
				[
					74,
					-11
				],
				[
					80,
					-12
				],
				[
					85,
					-13
				],
				[
					92,
					-15
				],
				[
					99,
					-17
				],
				[
					105,
					-20
				],
				[
					111,
					-22
				],
				[
					117,
					-24
				],
				[
					121,
					-25
				],
				[
					126,
					-27
				],
				[
					131,
					-29
				],
				[
					135,
					-31
				],
				[
					137,
					-33
				],
				[
					141,
					-35
				],
				[
					144,
					-37
				],
				[
					148,
					-38
				],
				[
					150,
					-39
				],
				[
					152,
					-41
				],
				[
					156,
					-43
				],
				[
					160,
					-45
				],
				[
					164,
					-48
				],
				[
					169,
					-50
				],
				[
					173,
					-52
				],
				[
					177,
					-55
				],
				[
					182,
					-58
				],
				[
					187,
					-61
				],
				[
					192,
					-63
				],
				[
					195,
					-65
				],
				[
					197,
					-67
				],
				[
					200,
					-70
				],
				[
					203,
					-73
				],
				[
					208,
					-78
				],
				[
					213,
					-82
				],
				[
					216,
					-85
				],
				[
					219,
					-87
				],
				[
					221,
					-90
				],
				[
					223,
					-92
				],
				[
					225,
					-94
				],
				[
					228,
					-97
				],
				[
					231,
					-100
				],
				[
					234,
					-103
				],
				[
					236,
					-104
				],
				[
					239,
					-107
				],
				[
					241,
					-108
				],
				[
					243,
					-110
				],
				[
					246,
					-112
				],
				[
					248,
					-113
				],
				[
					252,
					-115
				],
				[
					254,
					-117
				],
				[
					257,
					-119
				],
				[
					260,
					-120
				],
				[
					263,
					-121
				],
				[
					265,
					-123
				],
				[
					269,
					-124
				],
				[
					271,
					-125
				],
				[
					273,
					-127
				],
				[
					276,
					-128
				],
				[
					279,
					-129
				],
				[
					281,
					-130
				],
				[
					282,
					-131
				],
				[
					285,
					-132
				],
				[
					288,
					-134
				],
				[
					291,
					-136
				],
				[
					294,
					-138
				],
				[
					297,
					-140
				],
				[
					300,
					-143
				],
				[
					304,
					-144
				],
				[
					307,
					-147
				],
				[
					310,
					-148
				],
				[
					313,
					-150
				],
				[
					315,
					-151
				],
				[
					318,
					-152
				],
				[
					319,
					-153
				],
				[
					321,
					-153
				],
				[
					323,
					-153
				],
				[
					325,
					-154
				],
				[
					326,
					-154
				],
				[
					329,
					-154
				],
				[
					331,
					-154
				],
				[
					334,
					-154
				],
				[
					335,
					-154
				],
				[
					337,
					-153
				],
				[
					339,
					-152
				],
				[
					342,
					-150
				],
				[
					344,
					-150
				],
				[
					346,
					-149
				],
				[
					348,
					-148
				],
				[
					350,
					-147
				],
				[
					352,
					-146
				],
				[
					354,
					-145
				],
				[
					356,
					-145
				],
				[
					359,
					-144
				],
				[
					363,
					-143
				],
				[
					366,
					-142
				],
				[
					368,
					-141
				],
				[
					370,
					-141
				],
				[
					372,
					-140
				],
				[
					374,
					-138
				],
				[
					378,
					-137
				],
				[
					382,
					-136
				],
				[
					386,
					-135
				],
				[
					389,
					-134
				],
				[
					392,
					-133
				],
				[
					395,
					-132
				],
				[
					398,
					-129
				],
				[
					401,
					-128
				],
				[
					403,
					-126
				],
				[
					406,
					-124
				],
				[
					408,
					-122
				],
				[
					409,
					-121
				],
				[
					410,
					-118
				],
				[
					411,
					-115
				],
				[
					412,
					-111
				],
				[
					413,
					-108
				],
				[
					415,
					-105
				],
				[
					417,
					-99
				],
				[
					418,
					-97
				],
				[
					420,
					-92
				],
				[
					423,
					-88
				],
				[
					425,
					-84
				],
				[
					428,
					-80
				],
				[
					429,
					-78
				],
				[
					431,
					-75
				],
				[
					433,
					-72
				],
				[
					434,
					-69
				],
				[
					436,
					-66
				],
				[
					437,
					-63
				],
				[
					438,
					-60
				],
				[
					439,
					-57
				],
				[
					441,
					-54
				],
				[
					442,
					-50
				],
				[
					443,
					-48
				],
				[
					445,
					-46
				],
				[
					446,
					-43
				],
				[
					446,
					-41
				],
				[
					447,
					-38
				],
				[
					449,
					-35
				],
				[
					451,
					-32
				],
				[
					453,
					-30
				],
				[
					454,
					-28
				],
				[
					454,
					-26
				],
				[
					455,
					-24
				],
				[
					457,
					-20
				],
				[
					459,
					-16
				],
				[
					463,
					-10
				],
				[
					468,
					-6
				],
				[
					472,
					-3
				],
				[
					474,
					-1
				],
				[
					475,
					2
				],
				[
					476,
					3
				],
				[
					478,
					4
				],
				[
					481,
					6
				],
				[
					483,
					7
				],
				[
					487,
					7
				],
				[
					491,
					7
				],
				[
					494,
					8
				],
				[
					499,
					8
				],
				[
					503,
					9
				],
				[
					505,
					10
				],
				[
					508,
					11
				],
				[
					511,
					11
				],
				[
					514,
					11
				],
				[
					517,
					11
				],
				[
					520,
					11
				],
				[
					523,
					11
				],
				[
					525,
					11
				],
				[
					528,
					10
				],
				[
					529,
					10
				],
				[
					531,
					9
				],
				[
					533,
					9
				],
				[
					535,
					8
				],
				[
					537,
					8
				],
				[
					538,
					7
				],
				[
					540,
					7
				],
				[
					541,
					7
				],
				[
					543,
					7
				],
				[
					544,
					7
				],
				[
					546,
					6
				],
				[
					546,
					6
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				546,
				6
			]
		},
		{
			"id": "JPHuWAR4",
			"type": "text",
			"x": 170,
			"y": -347.2421875,
			"width": 71.89991760253906,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 296747138,
			"version": 8,
			"versionNonce": 644496990,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931794586,
			"link": null,
			"locked": false,
			"text": "Theorie",
			"rawText": "Theorie",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Theorie",
			"lineHeight": 1.25
		},
		{
			"id": "AApkucTg",
			"type": "text",
			"x": 222,
			"y": -16.2421875,
			"width": 102.73989868164062,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 497729090,
			"version": 11,
			"versionNonce": 554000734,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708931800274,
			"link": null,
			"locked": false,
			"text": "Experiment",
			"rawText": "Experiment",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Experiment",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 568.5,
		"scrollY": 476.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%