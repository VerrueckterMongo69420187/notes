---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
e- ^QymRRMZi

E=h*f ^llciY85S

Wie bei Billard ^wbiCysLz

p=h/lambda ^TpTj5Ec0

lambda...Wellenlänge ^DfITN6rb

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.25",
	"elements": [
		{
			"id": "8-AKc-FHfZ5X9QFBiVrIq",
			"type": "ellipse",
			"x": -158.75,
			"y": -98.2421875,
			"width": 61,
			"height": 60,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1096643887,
			"version": 27,
			"versionNonce": 1129666671,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769352660,
			"link": null,
			"locked": false
		},
		{
			"id": "I0rXygdLMPplQnvV3R5dv",
			"type": "freedraw",
			"x": -583.75,
			"y": -95.2421875,
			"width": 424,
			"height": 69,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 386822799,
			"version": 117,
			"versionNonce": 987165569,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769360996,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					-1
				],
				[
					3,
					-2
				],
				[
					3,
					-3
				],
				[
					4,
					-3
				],
				[
					5,
					-4
				],
				[
					8,
					-5
				],
				[
					12,
					-6
				],
				[
					17,
					-8
				],
				[
					24,
					-10
				],
				[
					31,
					-10
				],
				[
					40,
					-11
				],
				[
					47,
					-11
				],
				[
					52,
					-11
				],
				[
					54,
					-11
				],
				[
					55,
					-11
				],
				[
					55,
					-9
				],
				[
					56,
					-8
				],
				[
					56,
					-6
				],
				[
					56,
					-3
				],
				[
					57,
					1
				],
				[
					57,
					5
				],
				[
					57,
					10
				],
				[
					57,
					14
				],
				[
					58,
					18
				],
				[
					59,
					20
				],
				[
					61,
					21
				],
				[
					62,
					23
				],
				[
					65,
					25
				],
				[
					70,
					26
				],
				[
					76,
					27
				],
				[
					84,
					27
				],
				[
					93,
					27
				],
				[
					103,
					25
				],
				[
					111,
					22
				],
				[
					119,
					19
				],
				[
					124,
					15
				],
				[
					132,
					10
				],
				[
					139,
					5
				],
				[
					146,
					1
				],
				[
					153,
					-1
				],
				[
					160,
					-2
				],
				[
					169,
					-2
				],
				[
					176,
					1
				],
				[
					183,
					5
				],
				[
					186,
					8
				],
				[
					188,
					12
				],
				[
					191,
					20
				],
				[
					194,
					27
				],
				[
					197,
					33
				],
				[
					202,
					38
				],
				[
					207,
					41
				],
				[
					213,
					42
				],
				[
					220,
					43
				],
				[
					227,
					43
				],
				[
					235,
					42
				],
				[
					242,
					39
				],
				[
					251,
					35
				],
				[
					261,
					31
				],
				[
					270,
					28
				],
				[
					278,
					27
				],
				[
					284,
					27
				],
				[
					289,
					28
				],
				[
					292,
					31
				],
				[
					294,
					34
				],
				[
					295,
					39
				],
				[
					296,
					44
				],
				[
					298,
					47
				],
				[
					299,
					49
				],
				[
					301,
					50
				],
				[
					304,
					50
				],
				[
					311,
					49
				],
				[
					319,
					46
				],
				[
					327,
					41
				],
				[
					336,
					36
				],
				[
					346,
					30
				],
				[
					356,
					25
				],
				[
					364,
					22
				],
				[
					368,
					22
				],
				[
					370,
					22
				],
				[
					370,
					27
				],
				[
					370,
					30
				],
				[
					371,
					32
				],
				[
					375,
					34
				],
				[
					380,
					36
				],
				[
					386,
					38
				],
				[
					392,
					38
				],
				[
					396,
					38
				],
				[
					400,
					38
				],
				[
					402,
					38
				],
				[
					404,
					37
				],
				[
					407,
					36
				],
				[
					410,
					34
				],
				[
					414,
					32
				],
				[
					418,
					31
				],
				[
					419,
					31
				],
				[
					420,
					30
				],
				[
					420,
					29
				],
				[
					420,
					28
				],
				[
					422,
					27
				],
				[
					424,
					26
				],
				[
					424,
					25
				],
				[
					423,
					25
				],
				[
					423,
					26
				],
				[
					422,
					30
				],
				[
					421,
					34
				],
				[
					420,
					38
				],
				[
					419,
					42
				],
				[
					417,
					47
				],
				[
					415,
					51
				],
				[
					414,
					54
				],
				[
					414,
					56
				],
				[
					413,
					56
				],
				[
					412,
					57
				],
				[
					411,
					58
				],
				[
					411,
					58
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				411,
				58
			]
		},
		{
			"id": "Ct-Gn3EYHi2gz55h8j4lK",
			"type": "freedraw",
			"x": -166.75,
			"y": -64.2421875,
			"width": 15,
			"height": 28,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1139263279,
			"version": 16,
			"versionNonce": 1631539585,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769361530,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-2
				],
				[
					-1,
					-3
				],
				[
					-2,
					-6
				],
				[
					-3,
					-9
				],
				[
					-4,
					-11
				],
				[
					-6,
					-13
				],
				[
					-7,
					-15
				],
				[
					-8,
					-17
				],
				[
					-10,
					-20
				],
				[
					-11,
					-24
				],
				[
					-13,
					-26
				],
				[
					-14,
					-27
				],
				[
					-15,
					-28
				],
				[
					-15,
					-28
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-15,
				-28
			]
		},
		{
			"id": "1P9Lk_mLkqya6wtWMcQHM",
			"type": "freedraw",
			"x": -92.75,
			"y": -81.2421875,
			"width": 435,
			"height": 388,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 56363311,
			"version": 77,
			"versionNonce": 1198917601,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769365549,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					0
				],
				[
					4,
					-1
				],
				[
					8,
					-2
				],
				[
					11,
					-4
				],
				[
					16,
					-6
				],
				[
					20,
					-9
				],
				[
					24,
					-12
				],
				[
					29,
					-15
				],
				[
					32,
					-18
				],
				[
					34,
					-21
				],
				[
					34,
					-23
				],
				[
					35,
					-25
				],
				[
					35,
					-29
				],
				[
					36,
					-33
				],
				[
					38,
					-38
				],
				[
					39,
					-42
				],
				[
					40,
					-47
				],
				[
					40,
					-52
				],
				[
					40,
					-56
				],
				[
					42,
					-61
				],
				[
					45,
					-65
				],
				[
					53,
					-68
				],
				[
					63,
					-71
				],
				[
					77,
					-72
				],
				[
					93,
					-72
				],
				[
					107,
					-72
				],
				[
					118,
					-72
				],
				[
					126,
					-74
				],
				[
					133,
					-78
				],
				[
					142,
					-85
				],
				[
					153,
					-94
				],
				[
					167,
					-105
				],
				[
					180,
					-119
				],
				[
					190,
					-133
				],
				[
					196,
					-149
				],
				[
					200,
					-164
				],
				[
					201,
					-177
				],
				[
					201,
					-187
				],
				[
					200,
					-193
				],
				[
					200,
					-196
				],
				[
					200,
					-198
				],
				[
					203,
					-199
				],
				[
					206,
					-201
				],
				[
					213,
					-204
				],
				[
					226,
					-208
				],
				[
					244,
					-213
				],
				[
					269,
					-219
				],
				[
					294,
					-225
				],
				[
					319,
					-232
				],
				[
					339,
					-239
				],
				[
					352,
					-248
				],
				[
					358,
					-254
				],
				[
					359,
					-258
				],
				[
					359,
					-264
				],
				[
					353,
					-270
				],
				[
					346,
					-280
				],
				[
					339,
					-293
				],
				[
					338,
					-307
				],
				[
					341,
					-319
				],
				[
					348,
					-330
				],
				[
					357,
					-338
				],
				[
					367,
					-344
				],
				[
					380,
					-349
				],
				[
					396,
					-354
				],
				[
					411,
					-359
				],
				[
					424,
					-363
				],
				[
					431,
					-367
				],
				[
					434,
					-370
				],
				[
					435,
					-372
				],
				[
					434,
					-376
				],
				[
					433,
					-382
				],
				[
					433,
					-384
				],
				[
					433,
					-387
				],
				[
					433,
					-388
				],
				[
					433,
					-388
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				433,
				-388
			]
		},
		{
			"id": "YIKTzbhindcxWXlHF0dt5",
			"type": "freedraw",
			"x": 349.25,
			"y": -465.2421875,
			"width": 15,
			"height": 36,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 372077775,
			"version": 12,
			"versionNonce": 1941462625,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769366361,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					3
				],
				[
					2,
					7
				],
				[
					5,
					12
				],
				[
					6,
					17
				],
				[
					9,
					23
				],
				[
					12,
					28
				],
				[
					14,
					32
				],
				[
					15,
					34
				],
				[
					15,
					36
				],
				[
					15,
					36
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				15,
				36
			]
		},
		{
			"id": "xYlSjoljKpWH2u3N6FCB3",
			"type": "freedraw",
			"x": 343.25,
			"y": -469.2421875,
			"width": 57,
			"height": 1,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1664849487,
			"version": 13,
			"versionNonce": 850243777,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769366970,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-4,
					0
				],
				[
					-10,
					0
				],
				[
					-17,
					0
				],
				[
					-27,
					0
				],
				[
					-35,
					0
				],
				[
					-44,
					1
				],
				[
					-51,
					1
				],
				[
					-56,
					1
				],
				[
					-57,
					1
				],
				[
					-57,
					1
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-57,
				1
			]
		},
		{
			"id": "QymRRMZi",
			"type": "text",
			"x": -105.75,
			"y": -37.2421875,
			"width": 19.15997314453125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1313973743,
			"version": 32,
			"versionNonce": 781556815,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769378295,
			"link": null,
			"locked": false,
			"text": "e-",
			"rawText": "e-",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "e-",
			"lineHeight": 1.25
		},
		{
			"id": "llciY85S",
			"type": "text",
			"x": 129.25,
			"y": -141.2421875,
			"width": 209.55450439453125,
			"height": 93.99999999999989,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2047349903,
			"version": 50,
			"versionNonce": 303447919,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769503270,
			"link": null,
			"locked": false,
			"text": "E=h*f",
			"rawText": "E=h*f",
			"fontSize": 75.1999999999999,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 66,
			"containerId": null,
			"originalText": "E=h*f",
			"lineHeight": 1.25
		},
		{
			"id": "ZNB-JUkQj0HqRkleVclNs",
			"type": "ellipse",
			"x": -560.75,
			"y": 147.7578125,
			"width": 44,
			"height": 40,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1865108161,
			"version": 62,
			"versionNonce": 1671212335,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "_EHjNvRbqKqy7mGZGOQqX",
					"type": "arrow"
				}
			],
			"updated": 1713769404436,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 90,
			"versionNonce": 1084601647,
			"isDeleted": false,
			"id": "-4uYRgpKQ7qnxA52djuTf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -315.75,
			"y": 153.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 44,
			"height": 40,
			"seed": 868866849,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "_EHjNvRbqKqy7mGZGOQqX",
					"type": "arrow"
				},
				{
					"id": "X36ph_WpI5SW7q71cHCQi",
					"type": "arrow"
				}
			],
			"updated": 1713769418751,
			"link": null,
			"locked": false
		},
		{
			"id": "_EHjNvRbqKqy7mGZGOQqX",
			"type": "arrow",
			"x": -508.75,
			"y": 166.7578125,
			"width": 192,
			"height": 9,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 37272417,
			"version": 80,
			"versionNonce": 121213903,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769409262,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					192,
					9
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "ZNB-JUkQj0HqRkleVclNs",
				"focus": -0.12015288130730335,
				"gap": 8.019089424915833
			},
			"endBinding": {
				"elementId": "-4uYRgpKQ7qnxA52djuTf",
				"focus": -0.15370206245155038,
				"gap": 1.1039377296827055
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "X36ph_WpI5SW7q71cHCQi",
			"type": "arrow",
			"x": -270.75,
			"y": 188.7578125,
			"width": 143,
			"height": 112,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 104618113,
			"version": 26,
			"versionNonce": 38301455,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769418751,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					143,
					112
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "-4uYRgpKQ7qnxA52djuTf",
				"focus": -0.11417109985628816,
				"gap": 6.098521675201631
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "wbiCysLz",
			"type": "text",
			"x": -113.75,
			"y": 172.7578125,
			"width": 288.0381689453125,
			"height": 52,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1807038287,
			"version": 35,
			"versionNonce": 958336833,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769499646,
			"link": null,
			"locked": false,
			"text": "Wie bei Billard",
			"rawText": "Wie bei Billard",
			"fontSize": 41.600000000000016,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 34.99999999999997,
			"containerId": null,
			"originalText": "Wie bei Billard",
			"lineHeight": 1.25
		},
		{
			"id": "TpTj5Ec0",
			"type": "text",
			"x": 134.25,
			"y": -73.2421875,
			"width": 229.42624084472658,
			"height": 53,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 143713985,
			"version": 91,
			"versionNonce": 14622337,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769521930,
			"link": null,
			"locked": false,
			"text": "p=h/lambda",
			"rawText": "p=h/lambda",
			"fontSize": 42.400000000000006,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 36.99999999999999,
			"containerId": null,
			"originalText": "p=h/lambda",
			"lineHeight": 1.25
		},
		{
			"id": "DfITN6rb",
			"type": "text",
			"x": 123.25,
			"y": -13.2421875,
			"width": 346.17734619140634,
			"height": 46,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 553136705,
			"version": 44,
			"versionNonce": 381438991,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713769537497,
			"link": null,
			"locked": false,
			"text": "lambda...Wellenlänge",
			"rawText": "lambda...Wellenlänge",
			"fontSize": 36.80000000000002,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 30.99999999999998,
			"containerId": null,
			"originalText": "lambda...Wellenlänge",
			"lineHeight": 1.25
		},
		{
			"id": "rt8TxSiK",
			"type": "text",
			"x": 73.25,
			"y": 35.7578125,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1351996495,
			"version": 31,
			"versionNonce": 93502561,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1713769605146,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 750.25,
		"scrollY": 476.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%