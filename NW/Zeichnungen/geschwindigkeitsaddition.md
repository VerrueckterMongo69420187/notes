---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
y ^mg8sng5j

x' ^yByql1AO

y' ^jJMfjKSQ

x ^UCccJoK7

I ^I6qPRNJP

v ^7ucmX08W

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.20",
	"elements": [
		{
			"id": "oR1GmniXMkmrJyBAA5TXv",
			"type": "arrow",
			"x": -164,
			"y": 154.7578125,
			"width": 1,
			"height": 448,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 2085172697,
			"version": 62,
			"versionNonce": 156906937,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536181404,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					-448
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "e63GDSO095AWFSetgQkUR",
			"type": "arrow",
			"x": -263,
			"y": 74.7578125,
			"width": 601,
			"height": 1,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1151355831,
			"version": 87,
			"versionNonce": 2027413079,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536181404,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					601,
					-1
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "mg8sng5j",
			"type": "text",
			"x": -205,
			"y": -302.2421875,
			"width": 9.379989624023438,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1586033049,
			"version": 3,
			"versionNonce": 131929753,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536181405,
			"link": null,
			"locked": false,
			"text": "y",
			"rawText": "y",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "y",
			"lineHeight": 1.25
		},
		{
			"id": "yByql1AO",
			"type": "text",
			"x": 353,
			"y": 97.7578125,
			"width": 16.559982299804688,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1023331385,
			"version": 4,
			"versionNonce": 1690864313,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536236808,
			"link": null,
			"locked": false,
			"text": "x'",
			"rawText": "x'",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "x'",
			"lineHeight": 1.25
		},
		{
			"id": "MKHsmeFzsrBJIveIWD5ym",
			"type": "arrow",
			"x": 19,
			"y": 152.7578125,
			"width": 5,
			"height": 460,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1411853017,
			"version": 59,
			"versionNonce": 2031147897,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536181405,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-5,
					-460
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "jJMfjKSQ",
			"type": "text",
			"x": -19,
			"y": -312.2421875,
			"width": 14.699981689453125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 753245015,
			"version": 4,
			"versionNonce": 26702999,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536181405,
			"link": null,
			"locked": false,
			"text": "y'",
			"rawText": "y'",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "y'",
			"lineHeight": 1.25
		},
		{
			"id": "IlJ6NUWylWnNpg0APfRB9",
			"type": "arrow",
			"x": 23,
			"y": 73.7578125,
			"width": 220.8312779320399,
			"height": 1.2364547396579582,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1638311159,
			"version": 177,
			"versionNonce": 990472217,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536186076,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					220.8312779320399,
					-1.2364547396579582
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "UCccJoK7",
				"focus": -1.3974858800235892,
				"gap": 5
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "UCccJoK7",
			"type": "text",
			"x": 228.2016015625,
			"y": 37.7578125,
			"width": 13.038388671875,
			"height": 29,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1142264439,
			"version": 110,
			"versionNonce": 1410512697,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "IlJ6NUWylWnNpg0APfRB9",
					"type": "arrow"
				}
			],
			"updated": 1709536186076,
			"link": null,
			"locked": false,
			"text": "x",
			"rawText": "x",
			"fontSize": 23.2,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 20,
			"containerId": null,
			"originalText": "x",
			"lineHeight": 1.25
		},
		{
			"id": "I6qPRNJP",
			"type": "text",
			"x": -251,
			"y": -164.2421875,
			"width": 10.899993896484375,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 866055641,
			"version": 2,
			"versionNonce": 1252102487,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536194561,
			"link": null,
			"locked": false,
			"text": "I",
			"rawText": "I",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "I",
			"lineHeight": 1.25
		},
		{
			"id": "hR5hWLiELmayFW2gBYix4",
			"type": "arrow",
			"x": 20,
			"y": -102.2421875,
			"width": 86,
			"height": 1,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1699231865,
			"version": 56,
			"versionNonce": 452747351,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536203993,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					86,
					-1
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "7ucmX08W",
			"type": "text",
			"x": 60,
			"y": -85.2421875,
			"width": 10.459991455078125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1868316023,
			"version": 2,
			"versionNonce": 184951385,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709536210984,
			"link": null,
			"locked": false,
			"text": "v",
			"rawText": "v",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "v",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 568.5,
		"scrollY": 476.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%