---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.25",
	"elements": [
		{
			"id": "g2DHEK7KU3Mb88j76FD2q",
			"type": "rectangle",
			"x": -322.5,
			"y": -24.2421875,
			"width": 194,
			"height": 167,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 927203215,
			"version": 57,
			"versionNonce": 538392705,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "wBChQ5KzX988Lt_JLkm0G",
					"type": "arrow"
				}
			],
			"updated": 1712557333083,
			"link": null,
			"locked": false
		},
		{
			"id": "wBChQ5KzX988Lt_JLkm0G",
			"type": "arrow",
			"x": -124.5,
			"y": 59.7578125,
			"width": 279,
			"height": 7,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 81053327,
			"version": 44,
			"versionNonce": 1164735649,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557333083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					279,
					-7
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "g2DHEK7KU3Mb88j76FD2q",
				"focus": 0.0353068757690142,
				"gap": 4
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "wy-ie0gSv_mIXVzAWFbIz",
			"type": "ellipse",
			"x": -26.5,
			"y": -128.2421875,
			"width": 37,
			"height": 43,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 75399265,
			"version": 44,
			"versionNonce": 145595759,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "dMuyGfBg-YtwaxE05Irqe",
					"type": "arrow"
				}
			],
			"updated": 1712557343713,
			"link": null,
			"locked": false
		},
		{
			"id": "dMuyGfBg-YtwaxE05Irqe",
			"type": "arrow",
			"x": -1.5,
			"y": -127.2421875,
			"width": 2,
			"height": 184,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 810572545,
			"version": 48,
			"versionNonce": 356015951,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557343713,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					-184
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "wy-ie0gSv_mIXVzAWFbIz",
				"focus": 0.36336701418146705,
				"gap": 1
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "rxXydlOuM3XGYR_Giz337",
			"type": "line",
			"x": -170.5,
			"y": -326.2421875,
			"width": 410,
			"height": 5,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1930827663,
			"version": 59,
			"versionNonce": 663482817,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557352807,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					410,
					-5
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "whmkfO0LfTT5oKGrC1DKf",
			"type": "freedraw",
			"x": -53.5,
			"y": -331.2421875,
			"width": 107,
			"height": 29,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1200230817,
			"version": 92,
			"versionNonce": 355651631,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557360038,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					1,
					-3
				],
				[
					2,
					-5
				],
				[
					4,
					-7
				],
				[
					5,
					-8
				],
				[
					6,
					-9
				],
				[
					7,
					-10
				],
				[
					8,
					-11
				],
				[
					9,
					-12
				],
				[
					11,
					-12
				],
				[
					12,
					-13
				],
				[
					13,
					-14
				],
				[
					15,
					-15
				],
				[
					16,
					-15
				],
				[
					17,
					-16
				],
				[
					18,
					-16
				],
				[
					19,
					-17
				],
				[
					20,
					-17
				],
				[
					21,
					-18
				],
				[
					22,
					-19
				],
				[
					23,
					-19
				],
				[
					24,
					-20
				],
				[
					26,
					-20
				],
				[
					27,
					-21
				],
				[
					29,
					-21
				],
				[
					30,
					-22
				],
				[
					31,
					-23
				],
				[
					33,
					-23
				],
				[
					34,
					-24
				],
				[
					36,
					-24
				],
				[
					37,
					-24
				],
				[
					37,
					-25
				],
				[
					38,
					-25
				],
				[
					39,
					-25
				],
				[
					42,
					-25
				],
				[
					45,
					-26
				],
				[
					48,
					-26
				],
				[
					50,
					-26
				],
				[
					52,
					-26
				],
				[
					54,
					-26
				],
				[
					55,
					-26
				],
				[
					57,
					-27
				],
				[
					59,
					-28
				],
				[
					60,
					-28
				],
				[
					62,
					-28
				],
				[
					65,
					-28
				],
				[
					67,
					-28
				],
				[
					69,
					-28
				],
				[
					70,
					-28
				],
				[
					72,
					-28
				],
				[
					74,
					-28
				],
				[
					76,
					-29
				],
				[
					77,
					-29
				],
				[
					78,
					-29
				],
				[
					79,
					-29
				],
				[
					81,
					-29
				],
				[
					82,
					-28
				],
				[
					82,
					-27
				],
				[
					83,
					-27
				],
				[
					85,
					-27
				],
				[
					85,
					-26
				],
				[
					87,
					-26
				],
				[
					88,
					-25
				],
				[
					89,
					-24
				],
				[
					90,
					-24
				],
				[
					90,
					-23
				],
				[
					92,
					-22
				],
				[
					93,
					-20
				],
				[
					94,
					-20
				],
				[
					94,
					-19
				],
				[
					95,
					-18
				],
				[
					96,
					-17
				],
				[
					97,
					-16
				],
				[
					98,
					-15
				],
				[
					99,
					-14
				],
				[
					99,
					-13
				],
				[
					100,
					-13
				],
				[
					101,
					-12
				],
				[
					102,
					-11
				],
				[
					103,
					-10
				],
				[
					103,
					-9
				],
				[
					104,
					-9
				],
				[
					104,
					-8
				],
				[
					105,
					-8
				],
				[
					105,
					-7
				],
				[
					106,
					-7
				],
				[
					106,
					-6
				],
				[
					107,
					-6
				],
				[
					107,
					-5
				],
				[
					107,
					-5
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				107,
				-5
			]
		},
		{
			"id": "aOLUF40PGMZ9k2p7iBzYd",
			"type": "freedraw",
			"x": 132.5,
			"y": -137.2421875,
			"width": 37,
			"height": 32,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1710448161,
			"version": 34,
			"versionNonce": 1786860655,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557365633,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-2
				],
				[
					-1,
					-6
				],
				[
					-3,
					-10
				],
				[
					-4,
					-13
				],
				[
					-5,
					-16
				],
				[
					-7,
					-18
				],
				[
					-11,
					-20
				],
				[
					-15,
					-20
				],
				[
					-21,
					-19
				],
				[
					-26,
					-15
				],
				[
					-30,
					-10
				],
				[
					-30,
					-4
				],
				[
					-30,
					2
				],
				[
					-30,
					6
				],
				[
					-27,
					10
				],
				[
					-23,
					12
				],
				[
					-17,
					12
				],
				[
					-10,
					12
				],
				[
					-4,
					12
				],
				[
					2,
					12
				],
				[
					6,
					9
				],
				[
					7,
					5
				],
				[
					7,
					0
				],
				[
					7,
					-5
				],
				[
					5,
					-11
				],
				[
					4,
					-14
				],
				[
					3,
					-16
				],
				[
					2,
					-16
				],
				[
					1,
					-17
				],
				[
					0,
					-17
				],
				[
					-1,
					-17
				],
				[
					-1,
					-17
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-1,
				-17
			]
		},
		{
			"id": "FPOvdTyTtqrpeDbQDmPzM",
			"type": "freedraw",
			"x": 116.5,
			"y": -120.2421875,
			"width": 21,
			"height": 77,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2063353313,
			"version": 21,
			"versionNonce": 789328143,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557366740,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1
				],
				[
					0,
					4
				],
				[
					-1,
					9
				],
				[
					-3,
					17
				],
				[
					-4,
					26
				],
				[
					-6,
					34
				],
				[
					-8,
					42
				],
				[
					-9,
					50
				],
				[
					-9,
					56
				],
				[
					-9,
					60
				],
				[
					-9,
					62
				],
				[
					-9,
					63
				],
				[
					-7,
					65
				],
				[
					-2,
					68
				],
				[
					3,
					71
				],
				[
					8,
					74
				],
				[
					11,
					76
				],
				[
					12,
					77
				],
				[
					12,
					77
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				12,
				77
			]
		},
		{
			"id": "DvjuJM9cK_XDNPHzC5uon",
			"type": "freedraw",
			"x": 105.5,
			"y": -59.2421875,
			"width": 30,
			"height": 20,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 194271041,
			"version": 10,
			"versionNonce": 2130115151,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557367186,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-3,
					3
				],
				[
					-5,
					5
				],
				[
					-13,
					10
				],
				[
					-20,
					15
				],
				[
					-27,
					18
				],
				[
					-30,
					20
				],
				[
					-29,
					19
				],
				[
					-29,
					19
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-29,
				19
			]
		},
		{
			"id": "1l3hyvcJk0HsW81I9JK8r",
			"type": "freedraw",
			"x": 96.5,
			"y": -84.2421875,
			"width": 52,
			"height": 4,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1468688897,
			"version": 12,
			"versionNonce": 320396239,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1712557367672,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4,
					-1
				],
				[
					11,
					-1
				],
				[
					19,
					-1
				],
				[
					28,
					-1
				],
				[
					36,
					-1
				],
				[
					44,
					-2
				],
				[
					49,
					-3
				],
				[
					51,
					-3
				],
				[
					52,
					-4
				],
				[
					52,
					-4
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				52,
				-4
			]
		},
		{
			"id": "T5yzAvNV",
			"type": "text",
			"x": 333.5,
			"y": 75.7578125,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1760465025,
			"version": 32,
			"versionNonce": 2012483055,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1712557413902,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 235,
		"scrollY": 515.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%