---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
c ^IsehRay5

v ^ig3wcwS7

c ^RV58cN8P

c + V
 ^wHQXEsGP

C - v ^lv1nBM0A

E ^TMfvvrDj

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.20",
	"elements": [
		{
			"id": "niERLl5scO85X8A3hSx0E",
			"type": "ellipse",
			"x": -371,
			"y": -180.2421875,
			"width": 105,
			"height": 102,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1950958274,
			"version": 47,
			"versionNonce": 1753028738,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "02DT47UiEUgUkqMhWEUXW",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "TMfvvrDj"
				}
			],
			"updated": 1708933189269,
			"link": null,
			"locked": false
		},
		{
			"id": "TMfvvrDj",
			"type": "text",
			"x": -325.3631038760633,
			"y": -141.80463334051393,
			"width": 13.479995727539062,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1617978462,
			"version": 4,
			"versionNonce": 2064893826,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933191084,
			"link": null,
			"locked": false,
			"text": "E",
			"rawText": "E",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "niERLl5scO85X8A3hSx0E",
			"originalText": "E",
			"lineHeight": 1.25
		},
		{
			"id": "02DT47UiEUgUkqMhWEUXW",
			"type": "arrow",
			"x": -265.00000479935056,
			"y": -124.24218744462289,
			"width": 260.00000479935056,
			"height": 3.0000000553771144,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 964796290,
			"version": 44,
			"versionNonce": 239423234,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933191138,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					260.00000479935056,
					-3.0000000553771144
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "niERLl5scO85X8A3hSx0E",
				"gap": 1.2466798185176629,
				"focus": 0.11013551924478927
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "Du56RubdOKcEr5AjNpsNK",
			"type": "freedraw",
			"x": -280,
			"y": -167.2421875,
			"width": 342,
			"height": 33,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 323131486,
			"version": 88,
			"versionNonce": 1820526338,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933132003,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-2
				],
				[
					4,
					-4
				],
				[
					7,
					-6
				],
				[
					9,
					-8
				],
				[
					12,
					-10
				],
				[
					15,
					-11
				],
				[
					16,
					-11
				],
				[
					16,
					-12
				],
				[
					17,
					-12
				],
				[
					19,
					-12
				],
				[
					20,
					-12
				],
				[
					22,
					-12
				],
				[
					24,
					-12
				],
				[
					27,
					-12
				],
				[
					34,
					-11
				],
				[
					43,
					-9
				],
				[
					52,
					-8
				],
				[
					62,
					-7
				],
				[
					68,
					-6
				],
				[
					72,
					-5
				],
				[
					74,
					-4
				],
				[
					75,
					-3
				],
				[
					75,
					-2
				],
				[
					76,
					-2
				],
				[
					77,
					-2
				],
				[
					80,
					-2
				],
				[
					87,
					-3
				],
				[
					94,
					-4
				],
				[
					100,
					-6
				],
				[
					107,
					-8
				],
				[
					113,
					-11
				],
				[
					118,
					-14
				],
				[
					124,
					-17
				],
				[
					130,
					-21
				],
				[
					137,
					-24
				],
				[
					143,
					-27
				],
				[
					149,
					-27
				],
				[
					154,
					-27
				],
				[
					159,
					-27
				],
				[
					162,
					-27
				],
				[
					163,
					-27
				],
				[
					165,
					-27
				],
				[
					168,
					-25
				],
				[
					171,
					-22
				],
				[
					174,
					-18
				],
				[
					176,
					-15
				],
				[
					178,
					-13
				],
				[
					180,
					-11
				],
				[
					182,
					-9
				],
				[
					184,
					-9
				],
				[
					188,
					-9
				],
				[
					192,
					-9
				],
				[
					197,
					-9
				],
				[
					203,
					-9
				],
				[
					209,
					-10
				],
				[
					215,
					-11
				],
				[
					223,
					-12
				],
				[
					229,
					-13
				],
				[
					234,
					-15
				],
				[
					241,
					-17
				],
				[
					249,
					-20
				],
				[
					256,
					-23
				],
				[
					262,
					-25
				],
				[
					269,
					-28
				],
				[
					277,
					-31
				],
				[
					286,
					-33
				],
				[
					291,
					-33
				],
				[
					295,
					-33
				],
				[
					297,
					-30
				],
				[
					298,
					-27
				],
				[
					300,
					-24
				],
				[
					301,
					-21
				],
				[
					302,
					-19
				],
				[
					303,
					-17
				],
				[
					304,
					-16
				],
				[
					305,
					-14
				],
				[
					307,
					-14
				],
				[
					308,
					-14
				],
				[
					310,
					-14
				],
				[
					312,
					-13
				],
				[
					317,
					-12
				],
				[
					323,
					-12
				],
				[
					331,
					-12
				],
				[
					341,
					-12
				],
				[
					342,
					-12
				],
				[
					342,
					-12
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				342,
				-12
			]
		},
		{
			"id": "ABiS5L9p-luIVl-t1T-Cb",
			"type": "freedraw",
			"x": 43,
			"y": -185.2421875,
			"width": 5,
			"height": 23,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 130051678,
			"version": 14,
			"versionNonce": 690961282,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933135377,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					0,
					-3
				],
				[
					0,
					-4
				],
				[
					0,
					-5
				],
				[
					0,
					-8
				],
				[
					-1,
					-13
				],
				[
					-2,
					-17
				],
				[
					-3,
					-19
				],
				[
					-4,
					-21
				],
				[
					-5,
					-22
				],
				[
					-5,
					-23
				],
				[
					-5,
					-23
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-5,
				-23
			]
		},
		{
			"id": "CqDBloRxYLMIu5MDklwgE",
			"type": "freedraw",
			"x": 63,
			"y": -173.2421875,
			"width": 51,
			"height": 2,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 105468382,
			"version": 13,
			"versionNonce": 1370482754,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933135931,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					-1
				],
				[
					-4,
					-1
				],
				[
					-9,
					0
				],
				[
					-15,
					1
				],
				[
					-24,
					1
				],
				[
					-35,
					1
				],
				[
					-43,
					1
				],
				[
					-48,
					0
				],
				[
					-51,
					0
				],
				[
					-51,
					1
				],
				[
					-51,
					1
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-51,
				1
			]
		},
		{
			"id": "J1E-JwSa6hFHoOkRHCIxU",
			"type": "freedraw",
			"x": 59,
			"y": -181.2421875,
			"width": 27,
			"height": 48,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1200941342,
			"version": 15,
			"versionNonce": 505808002,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933136524,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					0,
					-2
				],
				[
					-1,
					-3
				],
				[
					-2,
					-7
				],
				[
					-6,
					-13
				],
				[
					-10,
					-20
				],
				[
					-15,
					-27
				],
				[
					-20,
					-35
				],
				[
					-24,
					-45
				],
				[
					-27,
					-48
				],
				[
					-27,
					-47
				],
				[
					-27,
					-45
				],
				[
					-27,
					-45
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-27,
				-45
			]
		},
		{
			"id": "K1OZRxeh7Fl5KVyxsxXp3",
			"type": "freedraw",
			"x": 61,
			"y": -179.2421875,
			"width": 54,
			"height": 21,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1063172318,
			"version": 17,
			"versionNonce": 801398850,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933137330,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-4,
					1
				],
				[
					-7,
					2
				],
				[
					-11,
					4
				],
				[
					-14,
					6
				],
				[
					-18,
					9
				],
				[
					-24,
					12
				],
				[
					-31,
					14
				],
				[
					-42,
					17
				],
				[
					-45,
					18
				],
				[
					-50,
					19
				],
				[
					-53,
					20
				],
				[
					-54,
					20
				],
				[
					-54,
					21
				],
				[
					-54,
					21
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-54,
				21
			]
		},
		{
			"id": "IsehRay5",
			"type": "text",
			"x": -132,
			"y": -234.2421875,
			"width": 10.039993286132812,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1711468830,
			"version": 2,
			"versionNonce": 709783426,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933150958,
			"link": null,
			"locked": false,
			"text": "c",
			"rawText": "c",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"id": "ig3wcwS7",
			"type": "text",
			"x": -149,
			"y": -106.2421875,
			"width": 10.459991455078125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 692444638,
			"version": 2,
			"versionNonce": 1860421314,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933153619,
			"link": null,
			"locked": false,
			"text": "v",
			"rawText": "v",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "v",
			"lineHeight": 1.25
		},
		{
			"id": "YpVWQ0FGQmsodprg1PVFg",
			"type": "freedraw",
			"x": -375,
			"y": -130.2421875,
			"width": 181,
			"height": 41,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 990805570,
			"version": 126,
			"versionNonce": 322043358,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933161158,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					-2
				],
				[
					-1,
					-3
				],
				[
					-2,
					-4
				],
				[
					-4,
					-7
				],
				[
					-5,
					-8
				],
				[
					-5,
					-9
				],
				[
					-5,
					-10
				],
				[
					-5,
					-11
				],
				[
					-6,
					-12
				],
				[
					-6,
					-13
				],
				[
					-6,
					-14
				],
				[
					-8,
					-15
				],
				[
					-10,
					-16
				],
				[
					-12,
					-18
				],
				[
					-14,
					-18
				],
				[
					-16,
					-18
				],
				[
					-18,
					-18
				],
				[
					-21,
					-19
				],
				[
					-23,
					-19
				],
				[
					-25,
					-20
				],
				[
					-27,
					-20
				],
				[
					-30,
					-20
				],
				[
					-32,
					-20
				],
				[
					-34,
					-20
				],
				[
					-37,
					-20
				],
				[
					-39,
					-19
				],
				[
					-42,
					-18
				],
				[
					-45,
					-17
				],
				[
					-46,
					-17
				],
				[
					-47,
					-17
				],
				[
					-48,
					-16
				],
				[
					-49,
					-15
				],
				[
					-51,
					-11
				],
				[
					-53,
					-9
				],
				[
					-55,
					-6
				],
				[
					-57,
					-3
				],
				[
					-59,
					-1
				],
				[
					-59,
					0
				],
				[
					-59,
					1
				],
				[
					-61,
					2
				],
				[
					-61,
					3
				],
				[
					-62,
					4
				],
				[
					-62,
					5
				],
				[
					-63,
					5
				],
				[
					-66,
					7
				],
				[
					-68,
					8
				],
				[
					-71,
					9
				],
				[
					-72,
					9
				],
				[
					-73,
					10
				],
				[
					-74,
					10
				],
				[
					-75,
					11
				],
				[
					-79,
					11
				],
				[
					-82,
					11
				],
				[
					-85,
					11
				],
				[
					-88,
					10
				],
				[
					-89,
					10
				],
				[
					-90,
					9
				],
				[
					-91,
					7
				],
				[
					-92,
					5
				],
				[
					-94,
					4
				],
				[
					-95,
					2
				],
				[
					-95,
					1
				],
				[
					-96,
					0
				],
				[
					-96,
					-2
				],
				[
					-97,
					-3
				],
				[
					-97,
					-5
				],
				[
					-97,
					-7
				],
				[
					-98,
					-8
				],
				[
					-99,
					-10
				],
				[
					-100,
					-12
				],
				[
					-101,
					-15
				],
				[
					-103,
					-16
				],
				[
					-104,
					-18
				],
				[
					-105,
					-19
				],
				[
					-105,
					-20
				],
				[
					-107,
					-22
				],
				[
					-108,
					-24
				],
				[
					-109,
					-25
				],
				[
					-111,
					-27
				],
				[
					-113,
					-28
				],
				[
					-115,
					-29
				],
				[
					-116,
					-29
				],
				[
					-117,
					-30
				],
				[
					-118,
					-30
				],
				[
					-120,
					-30
				],
				[
					-122,
					-30
				],
				[
					-124,
					-30
				],
				[
					-127,
					-30
				],
				[
					-130,
					-28
				],
				[
					-136,
					-26
				],
				[
					-140,
					-25
				],
				[
					-144,
					-24
				],
				[
					-145,
					-23
				],
				[
					-145,
					-22
				],
				[
					-146,
					-22
				],
				[
					-146,
					-21
				],
				[
					-147,
					-19
				],
				[
					-148,
					-17
				],
				[
					-148,
					-15
				],
				[
					-148,
					-12
				],
				[
					-148,
					-10
				],
				[
					-149,
					-7
				],
				[
					-150,
					-4
				],
				[
					-152,
					-1
				],
				[
					-153,
					0
				],
				[
					-155,
					1
				],
				[
					-157,
					3
				],
				[
					-159,
					4
				],
				[
					-162,
					5
				],
				[
					-163,
					5
				],
				[
					-164,
					5
				],
				[
					-166,
					7
				],
				[
					-168,
					7
				],
				[
					-171,
					7
				],
				[
					-174,
					7
				],
				[
					-175,
					7
				],
				[
					-177,
					5
				],
				[
					-178,
					2
				],
				[
					-179,
					-2
				],
				[
					-180,
					-4
				],
				[
					-181,
					-6
				],
				[
					-181,
					-7
				],
				[
					-181,
					-8
				],
				[
					-181,
					-8
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-181,
				-8
			]
		},
		{
			"id": "RV58cN8P",
			"type": "text",
			"x": -502,
			"y": -91.2421875,
			"width": 10.039993286132812,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2039326402,
			"version": 2,
			"versionNonce": 456073886,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933164370,
			"link": null,
			"locked": false,
			"text": "c",
			"rawText": "c",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"id": "wHQXEsGP",
			"type": "text",
			"x": -376,
			"y": -19.2421875,
			"width": 52.87998962402344,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 649278722,
			"version": 7,
			"versionNonce": 987620766,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933175274,
			"link": null,
			"locked": false,
			"text": "c + V\n",
			"rawText": "c + V\n",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 43,
			"containerId": null,
			"originalText": "c + V\n",
			"lineHeight": 1.25
		},
		{
			"id": "lv1nBM0A",
			"type": "text",
			"x": -376,
			"y": 24.7578125,
			"width": 51.559967041015625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 709661442,
			"version": 8,
			"versionNonce": 1118127070,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708933184006,
			"link": null,
			"locked": false,
			"text": "C - v",
			"rawText": "C - v",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "C - v",
			"lineHeight": 1.25
		},
		{
			"id": "JISj0Axs",
			"type": "text",
			"x": -409,
			"y": 59.7578125,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1618837342,
			"version": 2,
			"versionNonce": 1940270402,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1708933169519,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 568.5,
		"scrollY": 476.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%