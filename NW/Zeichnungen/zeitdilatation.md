---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Lichtuhr ^pQw5vUHd

v*tB ^szFCyUft

SB ^4vJpRIjs

SA ^i6h3MmuV

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.20",
	"elements": [
		{
			"id": "s8jR5JdNmGJed2uhqBwO4",
			"type": "freedraw",
			"x": -328,
			"y": -257.2421875,
			"width": 334,
			"height": 131,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1687057879,
			"version": 122,
			"versionNonce": 290689049,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537245037,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					0
				],
				[
					4,
					0
				],
				[
					5,
					0
				],
				[
					15,
					0
				],
				[
					26,
					0
				],
				[
					38,
					0
				],
				[
					57,
					0
				],
				[
					83,
					0
				],
				[
					115,
					-2
				],
				[
					169,
					-5
				],
				[
					187,
					-6
				],
				[
					212,
					-7
				],
				[
					229,
					-7
				],
				[
					237,
					-8
				],
				[
					238,
					-8
				],
				[
					239,
					-8
				],
				[
					240,
					-7
				],
				[
					242,
					-4
				],
				[
					245,
					1
				],
				[
					250,
					6
				],
				[
					257,
					14
				],
				[
					266,
					23
				],
				[
					277,
					32
				],
				[
					291,
					41
				],
				[
					302,
					48
				],
				[
					314,
					55
				],
				[
					322,
					60
				],
				[
					327,
					66
				],
				[
					329,
					69
				],
				[
					332,
					72
				],
				[
					333,
					73
				],
				[
					334,
					73
				],
				[
					329,
					76
				],
				[
					322,
					79
				],
				[
					314,
					83
				],
				[
					304,
					90
				],
				[
					293,
					98
				],
				[
					292,
					99
				],
				[
					288,
					103
				],
				[
					286,
					105
				],
				[
					285,
					106
				],
				[
					285,
					108
				],
				[
					284,
					108
				],
				[
					283,
					109
				],
				[
					281,
					110
				],
				[
					280,
					111
				],
				[
					279,
					112
				],
				[
					278,
					112
				],
				[
					277,
					113
				],
				[
					275,
					114
				],
				[
					272,
					116
				],
				[
					268,
					119
				],
				[
					266,
					120
				],
				[
					264,
					122
				],
				[
					262,
					123
				],
				[
					261,
					123
				],
				[
					256,
					123
				],
				[
					247,
					122
				],
				[
					228,
					120
				],
				[
					205,
					118
				],
				[
					181,
					116
				],
				[
					156,
					114
				],
				[
					136,
					112
				],
				[
					117,
					111
				],
				[
					95,
					111
				],
				[
					86,
					112
				],
				[
					76,
					112
				],
				[
					66,
					113
				],
				[
					58,
					113
				],
				[
					51,
					113
				],
				[
					47,
					113
				],
				[
					43,
					112
				],
				[
					40,
					112
				],
				[
					39,
					112
				],
				[
					38,
					112
				],
				[
					34,
					113
				],
				[
					30,
					115
				],
				[
					25,
					117
				],
				[
					19,
					118
				],
				[
					18,
					118
				],
				[
					17,
					118
				],
				[
					16,
					118
				],
				[
					15,
					118
				],
				[
					14,
					118
				],
				[
					14,
					115
				],
				[
					13,
					111
				],
				[
					12,
					106
				],
				[
					11,
					100
				],
				[
					10,
					92
				],
				[
					10,
					85
				],
				[
					9,
					77
				],
				[
					9,
					70
				],
				[
					7,
					61
				],
				[
					6,
					55
				],
				[
					6,
					48
				],
				[
					6,
					42
				],
				[
					6,
					37
				],
				[
					6,
					32
				],
				[
					6,
					30
				],
				[
					6,
					28
				],
				[
					6,
					27
				],
				[
					6,
					26
				],
				[
					6,
					24
				],
				[
					6,
					23
				],
				[
					6,
					21
				],
				[
					6,
					19
				],
				[
					6,
					16
				],
				[
					6,
					15
				],
				[
					6,
					14
				],
				[
					6,
					13
				],
				[
					6,
					12
				],
				[
					5,
					11
				],
				[
					5,
					10
				],
				[
					5,
					8
				],
				[
					4,
					7
				],
				[
					3,
					6
				],
				[
					3,
					5
				],
				[
					3,
					4
				],
				[
					2,
					3
				],
				[
					2,
					3
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				2,
				3
			]
		},
		{
			"id": "fBp6NNmzC923nSgwIvZ6B",
			"type": "freedraw",
			"x": -205,
			"y": -245.2421875,
			"width": 48,
			"height": 23,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 545227543,
			"version": 29,
			"versionNonce": 109839737,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537247129,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					-3,
					-2
				],
				[
					-12,
					-3
				],
				[
					-21,
					-3
				],
				[
					-31,
					-2
				],
				[
					-40,
					1
				],
				[
					-46,
					5
				],
				[
					-48,
					10
				],
				[
					-48,
					15
				],
				[
					-46,
					19
				],
				[
					-40,
					20
				],
				[
					-32,
					20
				],
				[
					-25,
					20
				],
				[
					-15,
					15
				],
				[
					-8,
					11
				],
				[
					-3,
					8
				],
				[
					-1,
					5
				],
				[
					-1,
					3
				],
				[
					-1,
					2
				],
				[
					-2,
					1
				],
				[
					-6,
					0
				],
				[
					-10,
					-1
				],
				[
					-13,
					-1
				],
				[
					-16,
					-1
				],
				[
					-17,
					-1
				],
				[
					-18,
					0
				],
				[
					-18,
					0
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-18,
				0
			]
		},
		{
			"id": "UuklXhmAee3uK0w7f6tbe",
			"type": "freedraw",
			"x": -232,
			"y": -230.2421875,
			"width": 20,
			"height": 61,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 212893623,
			"version": 24,
			"versionNonce": 1103080057,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537248031,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					7
				],
				[
					-2,
					8
				],
				[
					-3,
					11
				],
				[
					-3,
					12
				],
				[
					-3,
					13
				],
				[
					-3,
					16
				],
				[
					-3,
					18
				],
				[
					-2,
					21
				],
				[
					-2,
					24
				],
				[
					-2,
					28
				],
				[
					-2,
					31
				],
				[
					-2,
					32
				],
				[
					1,
					36
				],
				[
					5,
					40
				],
				[
					9,
					46
				],
				[
					13,
					51
				],
				[
					16,
					56
				],
				[
					17,
					59
				],
				[
					17,
					61
				],
				[
					16,
					59
				],
				[
					14,
					58
				],
				[
					14,
					58
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				14,
				58
			]
		},
		{
			"id": "c5rCMIaEeEYLzt_zuY9Zx",
			"type": "freedraw",
			"x": -234,
			"y": -196.2421875,
			"width": 34,
			"height": 24,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1742228151,
			"version": 12,
			"versionNonce": 993910009,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537248466,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-6,
					2
				],
				[
					-11,
					6
				],
				[
					-19,
					11
				],
				[
					-26,
					16
				],
				[
					-32,
					22
				],
				[
					-34,
					24
				],
				[
					-32,
					24
				],
				[
					-28,
					23
				],
				[
					-28,
					23
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-28,
				23
			]
		},
		{
			"id": "81cVynMlrQZaqkcTo3z0-",
			"type": "freedraw",
			"x": -254,
			"y": -215.2421875,
			"width": 47,
			"height": 0,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1296683063,
			"version": 9,
			"versionNonce": 1763945689,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537249003,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					7,
					0
				],
				[
					19,
					0
				],
				[
					29,
					0
				],
				[
					38,
					0
				],
				[
					45,
					0
				],
				[
					47,
					0
				],
				[
					47,
					0
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				47,
				0
			]
		},
		{
			"id": "7TyOXuOWCt1xWTG2R8m_V",
			"type": "freedraw",
			"x": -192,
			"y": -233.2421875,
			"width": 23,
			"height": 21,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 720939607,
			"version": 20,
			"versionNonce": 63284825,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537252168,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					-1
				],
				[
					2,
					-4
				],
				[
					4,
					-7
				],
				[
					5,
					-9
				],
				[
					7,
					-12
				],
				[
					8,
					-15
				],
				[
					9,
					-16
				],
				[
					10,
					-17
				],
				[
					11,
					-17
				],
				[
					13,
					-14
				],
				[
					15,
					-11
				],
				[
					17,
					-8
				],
				[
					18,
					-4
				],
				[
					20,
					-1
				],
				[
					21,
					1
				],
				[
					22,
					3
				],
				[
					23,
					4
				],
				[
					23,
					4
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				23,
				4
			]
		},
		{
			"id": "qblSd7g6ibth_9q-0meyc",
			"type": "freedraw",
			"x": 7,
			"y": -189.2421875,
			"width": 187,
			"height": 26,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1973237975,
			"version": 43,
			"versionNonce": 487607289,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537256382,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4,
					-1
				],
				[
					12,
					-1
				],
				[
					23,
					-1
				],
				[
					36,
					-1
				],
				[
					47,
					-1
				],
				[
					58,
					0
				],
				[
					67,
					0
				],
				[
					75,
					0
				],
				[
					78,
					-1
				],
				[
					80,
					-1
				],
				[
					81,
					-1
				],
				[
					83,
					-1
				],
				[
					89,
					-1
				],
				[
					99,
					-1
				],
				[
					106,
					-2
				],
				[
					112,
					-2
				],
				[
					123,
					-2
				],
				[
					127,
					-2
				],
				[
					132,
					-2
				],
				[
					137,
					-1
				],
				[
					143,
					-1
				],
				[
					149,
					0
				],
				[
					156,
					0
				],
				[
					164,
					0
				],
				[
					172,
					1
				],
				[
					178,
					1
				],
				[
					183,
					1
				],
				[
					186,
					1
				],
				[
					187,
					1
				],
				[
					185,
					0
				],
				[
					179,
					-3
				],
				[
					174,
					-6
				],
				[
					166,
					-10
				],
				[
					158,
					-15
				],
				[
					152,
					-19
				],
				[
					149,
					-21
				],
				[
					148,
					-23
				],
				[
					148,
					-24
				],
				[
					147,
					-24
				],
				[
					147,
					-25
				],
				[
					147,
					-25
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				147,
				-25
			]
		},
		{
			"id": "IxwU1C3zwBXZuIthEjiA0",
			"type": "freedraw",
			"x": 191,
			"y": -183.2421875,
			"width": 39,
			"height": 35,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 146693463,
			"version": 18,
			"versionNonce": 2123576729,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537264089,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					1
				],
				[
					-4,
					3
				],
				[
					-8,
					7
				],
				[
					-13,
					12
				],
				[
					-18,
					17
				],
				[
					-22,
					21
				],
				[
					-26,
					24
				],
				[
					-29,
					26
				],
				[
					-30,
					28
				],
				[
					-31,
					29
				],
				[
					-33,
					31
				],
				[
					-35,
					32
				],
				[
					-37,
					33
				],
				[
					-37,
					34
				],
				[
					-39,
					35
				],
				[
					-39,
					35
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-39,
				35
			]
		},
		{
			"id": "mchA2DlpmnajHC1BvNl_l",
			"type": "freedraw",
			"x": 54,
			"y": -237.2421875,
			"width": 46,
			"height": 34,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 100967831,
			"version": 20,
			"versionNonce": 1812025113,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537265288,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					3
				],
				[
					0,
					9
				],
				[
					0,
					15
				],
				[
					1,
					21
				],
				[
					4,
					27
				],
				[
					7,
					32
				],
				[
					9,
					33
				],
				[
					12,
					34
				],
				[
					13,
					34
				],
				[
					16,
					34
				],
				[
					20,
					31
				],
				[
					27,
					26
				],
				[
					33,
					21
				],
				[
					39,
					14
				],
				[
					44,
					9
				],
				[
					46,
					7
				],
				[
					46,
					6
				],
				[
					46,
					6
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				46,
				6
			]
		},
		{
			"id": "xs0z8oBI-7sL3VdscsGLS",
			"type": "freedraw",
			"x": -93,
			"y": -58.2421875,
			"width": 89,
			"height": 38,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1282610199,
			"version": 32,
			"versionNonce": 868854553,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537270440,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1
				],
				[
					0,
					-2
				],
				[
					-1,
					-4
				],
				[
					-4,
					-6
				],
				[
					-8,
					-7
				],
				[
					-15,
					-7
				],
				[
					-25,
					-5
				],
				[
					-40,
					1
				],
				[
					-53,
					7
				],
				[
					-60,
					13
				],
				[
					-61,
					18
				],
				[
					-61,
					21
				],
				[
					-56,
					26
				],
				[
					-47,
					30
				],
				[
					-36,
					31
				],
				[
					-19,
					31
				],
				[
					-2,
					28
				],
				[
					12,
					22
				],
				[
					22,
					16
				],
				[
					26,
					10
				],
				[
					28,
					3
				],
				[
					28,
					-1
				],
				[
					24,
					-4
				],
				[
					19,
					-5
				],
				[
					12,
					-5
				],
				[
					5,
					-4
				],
				[
					-1,
					-3
				],
				[
					-5,
					-2
				],
				[
					-6,
					-2
				],
				[
					-6,
					-2
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-6,
				-2
			]
		},
		{
			"id": "paXez8PxJm61jZUETcSZk",
			"type": "freedraw",
			"x": -113,
			"y": -24.2421875,
			"width": 40,
			"height": 44,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 736229399,
			"version": 21,
			"versionNonce": 412879225,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537271463,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1
				],
				[
					0,
					4
				],
				[
					1,
					8
				],
				[
					2,
					15
				],
				[
					3,
					22
				],
				[
					4,
					28
				],
				[
					5,
					32
				],
				[
					7,
					34
				],
				[
					7,
					36
				],
				[
					9,
					36
				],
				[
					16,
					38
				],
				[
					24,
					40
				],
				[
					30,
					41
				],
				[
					33,
					42
				],
				[
					36,
					43
				],
				[
					38,
					44
				],
				[
					39,
					44
				],
				[
					40,
					44
				],
				[
					40,
					44
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				40,
				44
			]
		},
		{
			"id": "H3tttbCjYhkrkpZ12F82t",
			"type": "freedraw",
			"x": -104,
			"y": 6.7578125,
			"width": 21,
			"height": 38,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1726692279,
			"version": 14,
			"versionNonce": 1922945465,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537271995,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-2,
					1
				],
				[
					-3,
					2
				],
				[
					-5,
					6
				],
				[
					-8,
					10
				],
				[
					-11,
					15
				],
				[
					-15,
					23
				],
				[
					-20,
					33
				],
				[
					-21,
					37
				],
				[
					-21,
					38
				],
				[
					-21,
					35
				],
				[
					-20,
					29
				],
				[
					-20,
					29
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-20,
				29
			]
		},
		{
			"id": "U_29Yd79W4YoR5kBK4-by",
			"type": "freedraw",
			"x": -128,
			"y": 2.7578125,
			"width": 66,
			"height": 15,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2118623095,
			"version": 12,
			"versionNonce": 1992500281,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537272452,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-2
				],
				[
					11,
					-4
				],
				[
					24,
					-7
				],
				[
					37,
					-9
				],
				[
					46,
					-11
				],
				[
					55,
					-13
				],
				[
					59,
					-13
				],
				[
					64,
					-14
				],
				[
					66,
					-15
				],
				[
					66,
					-15
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				66,
				-15
			]
		},
		{
			"id": "BsDB-mV-Mvnb_TYfcasc5",
			"type": "freedraw",
			"x": -163,
			"y": 45.7578125,
			"width": 152,
			"height": 9,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 876086519,
			"version": 32,
			"versionNonce": 827260985,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537273877,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					2,
					-1
				],
				[
					6,
					-2
				],
				[
					11,
					-2
				],
				[
					16,
					-2
				],
				[
					21,
					-2
				],
				[
					27,
					-2
				],
				[
					33,
					-2
				],
				[
					41,
					-1
				],
				[
					51,
					-1
				],
				[
					58,
					-1
				],
				[
					64,
					-3
				],
				[
					69,
					-3
				],
				[
					76,
					-3
				],
				[
					82,
					-3
				],
				[
					89,
					-4
				],
				[
					96,
					-4
				],
				[
					102,
					-5
				],
				[
					109,
					-7
				],
				[
					115,
					-7
				],
				[
					121,
					-7
				],
				[
					125,
					-7
				],
				[
					129,
					-7
				],
				[
					134,
					-7
				],
				[
					138,
					-8
				],
				[
					142,
					-8
				],
				[
					146,
					-8
				],
				[
					148,
					-8
				],
				[
					151,
					-9
				],
				[
					152,
					-9
				],
				[
					152,
					-9
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				152,
				-9
			]
		},
		{
			"id": "ZjTvdxQOIyskA8n7oKKWY",
			"type": "freedraw",
			"x": -28,
			"y": -74.2421875,
			"width": 36,
			"height": 56,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 642476279,
			"version": 68,
			"versionNonce": 877679545,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537278390,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3,
					-1
				],
				[
					7,
					-2
				],
				[
					11,
					-3
				],
				[
					13,
					-4
				],
				[
					16,
					-4
				],
				[
					21,
					-3
				],
				[
					25,
					-1
				],
				[
					29,
					2
				],
				[
					32,
					4
				],
				[
					34,
					6
				],
				[
					36,
					9
				],
				[
					36,
					11
				],
				[
					36,
					14
				],
				[
					35,
					19
				],
				[
					33,
					23
				],
				[
					31,
					25
				],
				[
					28,
					26
				],
				[
					23,
					26
				],
				[
					18,
					26
				],
				[
					13,
					26
				],
				[
					12,
					26
				],
				[
					14,
					26
				],
				[
					18,
					26
				],
				[
					22,
					26
				],
				[
					25,
					27
				],
				[
					27,
					29
				],
				[
					29,
					31
				],
				[
					30,
					34
				],
				[
					32,
					37
				],
				[
					32,
					39
				],
				[
					32,
					40
				],
				[
					32,
					43
				],
				[
					30,
					46
				],
				[
					28,
					47
				],
				[
					26,
					49
				],
				[
					23,
					50
				],
				[
					21,
					51
				],
				[
					18,
					52
				],
				[
					15,
					52
				],
				[
					10,
					52
				],
				[
					6,
					52
				],
				[
					4,
					52
				],
				[
					2,
					51
				],
				[
					1,
					51
				],
				[
					1,
					50
				],
				[
					1,
					49
				],
				[
					0,
					49
				],
				[
					0,
					48
				],
				[
					0,
					47
				],
				[
					0,
					44
				],
				[
					0,
					40
				],
				[
					1,
					35
				],
				[
					2,
					31
				],
				[
					3,
					27
				],
				[
					3,
					23
				],
				[
					5,
					18
				],
				[
					8,
					14
				],
				[
					9,
					11
				],
				[
					9,
					8
				],
				[
					10,
					7
				],
				[
					11,
					5
				],
				[
					11,
					3
				],
				[
					11,
					2
				],
				[
					11,
					1
				],
				[
					11,
					0
				],
				[
					11,
					0
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				11,
				0
			]
		},
		{
			"id": "ab-JLdViS20l2p5vBP0fE",
			"type": "rectangle",
			"x": 207,
			"y": -62.2421875,
			"width": 111,
			"height": 322,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 83000695,
			"version": 67,
			"versionNonce": 858624951,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537293348,
			"link": null,
			"locked": false
		},
		{
			"id": "WSpAW2uYpw6jyC25Mry2x",
			"type": "line",
			"x": 250,
			"y": -62.2421875,
			"width": 0,
			"height": 324,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 97518807,
			"version": 82,
			"versionNonce": 686390809,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537306423,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					324
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "8FJ1uIYQn8BNOG4agi-Gv",
			"type": "line",
			"x": 260,
			"y": -63.2421875,
			"width": 1,
			"height": 326,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 455365369,
			"version": 88,
			"versionNonce": 482603479,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537313623,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					326
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "VJGUygjw0VwdPjBQGMxnH",
			"type": "freedraw",
			"x": 259,
			"y": -65.2421875,
			"width": 10,
			"height": 11,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1161142007,
			"version": 36,
			"versionNonce": 2101617081,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537319317,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					2
				],
				[
					0,
					3
				],
				[
					-2,
					5
				],
				[
					-5,
					7
				],
				[
					-6,
					10
				],
				[
					-6,
					11
				],
				[
					-5,
					11
				],
				[
					-1,
					11
				],
				[
					1,
					9
				],
				[
					1,
					7
				],
				[
					1,
					5
				],
				[
					1,
					3
				],
				[
					1,
					2
				],
				[
					0,
					2
				],
				[
					-2,
					4
				],
				[
					-2,
					5
				],
				[
					-4,
					5
				],
				[
					-5,
					5
				],
				[
					-7,
					5
				],
				[
					-8,
					6
				],
				[
					-9,
					6
				],
				[
					-9,
					7
				],
				[
					-9,
					8
				],
				[
					-8,
					9
				],
				[
					-6,
					9
				],
				[
					-5,
					8
				],
				[
					-5,
					6
				],
				[
					-4,
					3
				],
				[
					-4,
					1
				],
				[
					-4,
					0
				],
				[
					-5,
					5
				],
				[
					-6,
					8
				],
				[
					-6,
					9
				],
				[
					-6,
					9
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-6,
				9
			]
		},
		{
			"id": "RD7V5TTyG5N1-J3PY-O9Q",
			"type": "freedraw",
			"x": 257,
			"y": 259.7578125,
			"width": 13,
			"height": 16,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 194531191,
			"version": 33,
			"versionNonce": 876863129,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537321198,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					-1
				],
				[
					-1,
					-2
				],
				[
					-3,
					3
				],
				[
					-4,
					6
				],
				[
					-5,
					10
				],
				[
					-5,
					12
				],
				[
					-4,
					13
				],
				[
					-2,
					12
				],
				[
					2,
					8
				],
				[
					4,
					4
				],
				[
					4,
					1
				],
				[
					4,
					0
				],
				[
					4,
					-1
				],
				[
					3,
					-2
				],
				[
					2,
					-2
				],
				[
					2,
					-3
				],
				[
					2,
					0
				],
				[
					2,
					3
				],
				[
					2,
					6
				],
				[
					2,
					7
				],
				[
					2,
					6
				],
				[
					2,
					3
				],
				[
					1,
					1
				],
				[
					-1,
					0
				],
				[
					-2,
					0
				],
				[
					-5,
					0
				],
				[
					-7,
					4
				],
				[
					-9,
					6
				],
				[
					-9,
					7
				],
				[
					-8,
					7
				],
				[
					-8,
					7
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-8,
				7
			]
		},
		{
			"id": "pQw5vUHd",
			"type": "text",
			"x": 229,
			"y": -109.2421875,
			"width": 77.419921875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 132787351,
			"version": 9,
			"versionNonce": 69804889,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537327996,
			"link": null,
			"locked": false,
			"text": "Lichtuhr",
			"rawText": "Lichtuhr",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "Lichtuhr",
			"lineHeight": 1.25
		},
		{
			"id": "UJDH5_5TZMQ6t_uGZ8Bk7",
			"type": "freedraw",
			"x": -286,
			"y": 251.7578125,
			"width": 298,
			"height": 180,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1932107831,
			"version": 150,
			"versionNonce": 262708281,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537551273,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1,
					0
				],
				[
					6,
					0
				],
				[
					13,
					0
				],
				[
					21,
					1
				],
				[
					31,
					2
				],
				[
					47,
					4
				],
				[
					65,
					6
				],
				[
					83,
					8
				],
				[
					99,
					9
				],
				[
					110,
					9
				],
				[
					117,
					9
				],
				[
					121,
					9
				],
				[
					124,
					9
				],
				[
					130,
					8
				],
				[
					137,
					6
				],
				[
					144,
					6
				],
				[
					151,
					6
				],
				[
					160,
					7
				],
				[
					168,
					7
				],
				[
					177,
					7
				],
				[
					185,
					7
				],
				[
					191,
					7
				],
				[
					200,
					7
				],
				[
					209,
					7
				],
				[
					218,
					6
				],
				[
					225,
					5
				],
				[
					230,
					5
				],
				[
					233,
					5
				],
				[
					235,
					5
				],
				[
					237,
					5
				],
				[
					239,
					5
				],
				[
					242,
					4
				],
				[
					246,
					4
				],
				[
					251,
					3
				],
				[
					257,
					3
				],
				[
					260,
					3
				],
				[
					261,
					3
				],
				[
					261,
					1
				],
				[
					262,
					-4
				],
				[
					262,
					-10
				],
				[
					263,
					-17
				],
				[
					263,
					-24
				],
				[
					263,
					-33
				],
				[
					261,
					-41
				],
				[
					258,
					-50
				],
				[
					256,
					-58
				],
				[
					255,
					-67
				],
				[
					254,
					-76
				],
				[
					252,
					-83
				],
				[
					251,
					-91
				],
				[
					249,
					-95
				],
				[
					249,
					-98
				],
				[
					247,
					-102
				],
				[
					246,
					-107
				],
				[
					246,
					-114
				],
				[
					245,
					-120
				],
				[
					245,
					-126
				],
				[
					245,
					-132
				],
				[
					245,
					-138
				],
				[
					245,
					-143
				],
				[
					245,
					-150
				],
				[
					246,
					-155
				],
				[
					248,
					-158
				],
				[
					248,
					-160
				],
				[
					249,
					-163
				],
				[
					251,
					-168
				],
				[
					251,
					-169
				],
				[
					251,
					-171
				],
				[
					252,
					-171
				],
				[
					251,
					-171
				],
				[
					248,
					-171
				],
				[
					244,
					-169
				],
				[
					239,
					-167
				],
				[
					234,
					-164
				],
				[
					226,
					-161
				],
				[
					218,
					-157
				],
				[
					210,
					-154
				],
				[
					204,
					-151
				],
				[
					195,
					-148
				],
				[
					186,
					-144
				],
				[
					177,
					-139
				],
				[
					167,
					-133
				],
				[
					157,
					-127
				],
				[
					146,
					-121
				],
				[
					135,
					-117
				],
				[
					124,
					-113
				],
				[
					116,
					-111
				],
				[
					108,
					-107
				],
				[
					100,
					-104
				],
				[
					91,
					-101
				],
				[
					84,
					-98
				],
				[
					75,
					-91
				],
				[
					72,
					-88
				],
				[
					69,
					-86
				],
				[
					64,
					-83
				],
				[
					61,
					-80
				],
				[
					57,
					-77
				],
				[
					54,
					-75
				],
				[
					52,
					-72
				],
				[
					48,
					-69
				],
				[
					43,
					-66
				],
				[
					38,
					-62
				],
				[
					35,
					-59
				],
				[
					32,
					-58
				],
				[
					29,
					-55
				],
				[
					26,
					-53
				],
				[
					23,
					-50
				],
				[
					20,
					-47
				],
				[
					17,
					-46
				],
				[
					16,
					-44
				],
				[
					13,
					-41
				],
				[
					11,
					-39
				],
				[
					8,
					-37
				],
				[
					5,
					-34
				],
				[
					-1,
					-30
				],
				[
					-2,
					-29
				],
				[
					-6,
					-27
				],
				[
					-10,
					-25
				],
				[
					-13,
					-24
				],
				[
					-16,
					-22
				],
				[
					-19,
					-20
				],
				[
					-22,
					-18
				],
				[
					-25,
					-16
				],
				[
					-28,
					-14
				],
				[
					-31,
					-13
				],
				[
					-33,
					-13
				],
				[
					-34,
					-12
				],
				[
					-35,
					-12
				],
				[
					-35,
					-9
				],
				[
					-35,
					-8
				],
				[
					-32,
					-8
				],
				[
					-29,
					-8
				],
				[
					-25,
					-7
				],
				[
					-22,
					-7
				],
				[
					-18,
					-6
				],
				[
					-15,
					-6
				],
				[
					-10,
					-5
				],
				[
					-7,
					-5
				],
				[
					-4,
					-5
				],
				[
					1,
					-5
				],
				[
					8,
					-5
				],
				[
					14,
					-4
				],
				[
					18,
					-4
				],
				[
					20,
					-4
				],
				[
					22,
					-3
				],
				[
					23,
					-3
				],
				[
					24,
					-2
				],
				[
					24,
					-2
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				24,
				-2
			]
		},
		{
			"id": "5H2E88PNxQ4EpFXq3xQ2a",
			"type": "freedraw",
			"x": -32,
			"y": 199.7578125,
			"width": 39,
			"height": 56,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2038762743,
			"version": 23,
			"versionNonce": 59896921,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537556403,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-5,
					1
				],
				[
					-8,
					2
				],
				[
					-12,
					4
				],
				[
					-16,
					5
				],
				[
					-20,
					7
				],
				[
					-24,
					8
				],
				[
					-26,
					11
				],
				[
					-29,
					16
				],
				[
					-32,
					22
				],
				[
					-34,
					28
				],
				[
					-36,
					34
				],
				[
					-38,
					40
				],
				[
					-38,
					45
				],
				[
					-39,
					49
				],
				[
					-39,
					52
				],
				[
					-39,
					53
				],
				[
					-39,
					54
				],
				[
					-39,
					55
				],
				[
					-39,
					56
				],
				[
					-37,
					55
				],
				[
					-37,
					55
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-37,
				55
			]
		},
		{
			"id": "cPG4BsafZz_q63oceZsDz",
			"type": "freedraw",
			"x": -44,
			"y": 225.7578125,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1396869847,
			"version": 3,
			"versionNonce": 255706873,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537556744,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		},
		{
			"id": "RYG16MJMPQZooPHfcOt0b",
			"type": "freedraw",
			"x": -34,
			"y": 74.7578125,
			"width": 14,
			"height": 32,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1647232567,
			"version": 11,
			"versionNonce": 2063938713,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537558164,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					2
				],
				[
					1,
					6
				],
				[
					3,
					12
				],
				[
					6,
					19
				],
				[
					9,
					25
				],
				[
					11,
					29
				],
				[
					13,
					31
				],
				[
					14,
					32
				],
				[
					14,
					32
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				14,
				32
			]
		},
		{
			"id": "Ix76XKII4g0suAppeNCBB",
			"type": "freedraw",
			"x": -41,
			"y": 87.7578125,
			"width": 26,
			"height": 36,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 597685911,
			"version": 12,
			"versionNonce": 908670745,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537558832,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-4,
					6
				],
				[
					-8,
					13
				],
				[
					-13,
					20
				],
				[
					-17,
					26
				],
				[
					-21,
					30
				],
				[
					-24,
					33
				],
				[
					-26,
					36
				],
				[
					-25,
					36
				],
				[
					-25,
					36
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-25,
				36
			]
		},
		{
			"id": "3DmnZ8x7-W8I-YCiFpXMK",
			"type": "freedraw",
			"x": -39,
			"y": 79.7578125,
			"width": 60,
			"height": 20,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 101362711,
			"version": 17,
			"versionNonce": 623991289,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537559849,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-2,
					-2
				],
				[
					-4,
					-3
				],
				[
					-7,
					-4
				],
				[
					-13,
					-7
				],
				[
					-21,
					-10
				],
				[
					-28,
					-11
				],
				[
					-36,
					-13
				],
				[
					-43,
					-16
				],
				[
					-54,
					-18
				],
				[
					-57,
					-19
				],
				[
					-59,
					-20
				],
				[
					-60,
					-20
				],
				[
					-59,
					-20
				],
				[
					-59,
					-20
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-59,
				-20
			]
		},
		{
			"id": "cM7WcyMl722sliJKz3VAH",
			"type": "freedraw",
			"x": -48,
			"y": 84.7578125,
			"width": 53,
			"height": 56,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1355170615,
			"version": 18,
			"versionNonce": 1927863737,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537560672,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4,
					3
				],
				[
					-5,
					4
				],
				[
					-8,
					9
				],
				[
					-11,
					14
				],
				[
					-13,
					19
				],
				[
					-16,
					26
				],
				[
					-19,
					32
				],
				[
					-24,
					36
				],
				[
					-30,
					39
				],
				[
					-39,
					44
				],
				[
					-48,
					48
				],
				[
					-51,
					50
				],
				[
					-52,
					53
				],
				[
					-53,
					55
				],
				[
					-53,
					56
				],
				[
					-53,
					56
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-53,
				56
			]
		},
		{
			"id": "szFCyUft",
			"type": "text",
			"x": -170,
			"y": 286.7578125,
			"width": 46.619964599609375,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1533858679,
			"version": 6,
			"versionNonce": 916448729,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537682624,
			"link": null,
			"locked": false,
			"text": "v*tB",
			"rawText": "v*tB",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "v*tB",
			"lineHeight": 1.25
		},
		{
			"id": "4vJpRIjs",
			"type": "text",
			"x": -226,
			"y": 124.7578125,
			"width": 26.699981689453125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1342621975,
			"version": 3,
			"versionNonce": 1194055065,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537574806,
			"link": null,
			"locked": false,
			"text": "SB",
			"rawText": "SB",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "SB",
			"lineHeight": 1.25
		},
		{
			"id": "i6h3MmuV",
			"type": "text",
			"x": -13,
			"y": 152.7578125,
			"width": 25.279983520507812,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 161088919,
			"version": 7,
			"versionNonce": 221899929,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1709537582675,
			"link": null,
			"locked": false,
			"text": "SA",
			"rawText": "SA",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "SA",
			"lineHeight": 1.25
		},
		{
			"id": "EI9BcR3upgUlOgm0mYkjY",
			"type": "freedraw",
			"x": 197,
			"y": -187.2421875,
			"width": 179,
			"height": 45,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 464630071,
			"version": 25,
			"versionNonce": 656210999,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1709537260311,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1,
					0
				],
				[
					-7,
					1
				],
				[
					-18,
					4
				],
				[
					-37,
					9
				],
				[
					-63,
					17
				],
				[
					-96,
					25
				],
				[
					-126,
					33
				],
				[
					-152,
					39
				],
				[
					-172,
					44
				],
				[
					-179,
					45
				],
				[
					-178,
					43
				],
				[
					-175,
					41
				],
				[
					-173,
					40
				],
				[
					-170,
					39
				],
				[
					-167,
					39
				],
				[
					-161,
					38
				],
				[
					-155,
					38
				],
				[
					-149,
					38
				],
				[
					-145,
					38
				],
				[
					-144,
					38
				],
				[
					-143,
					38
				],
				[
					-143,
					38
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-143,
				38
			]
		},
		{
			"id": "1bIXH9AE",
			"type": "text",
			"x": -62,
			"y": 131.7578125,
			"width": 55.27995300292969,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 799648727,
			"version": 8,
			"versionNonce": 1586433273,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1709537349943,
			"link": null,
			"locked": false,
			"text": "s=v*t\n",
			"rawText": "s=v*t\n",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 43,
			"containerId": null,
			"originalText": "s=v*t\n",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 568.5,
		"scrollY": 476.7578125,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%