---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.20",
	"elements": [
		{
			"type": "line",
			"version": 73,
			"versionNonce": 2123760338,
			"isDeleted": false,
			"id": "BRZxj4Ej4kJzVeHw7UWYX",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -327.5,
			"y": -133.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 627,
			"height": 4,
			"seed": 2030299470,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327717520,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					627,
					4
				]
			]
		},
		{
			"type": "arrow",
			"version": 32,
			"versionNonce": 1359668494,
			"isDeleted": false,
			"id": "JSiU2jDxyOnsQq3q2aVON",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -227.5,
			"y": -263.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 2,
			"height": 183,
			"seed": 1848760466,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327723485,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					2,
					183
				]
			]
		},
		{
			"type": "arrow",
			"version": 22,
			"versionNonce": 1708422226,
			"isDeleted": false,
			"id": "P-uuW7PDH1fXkST1EHGJN",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -143.5,
			"y": -282.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 1,
			"height": 221,
			"seed": 1695377038,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327727853,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-1,
					221
				]
			]
		},
		{
			"type": "arrow",
			"version": 38,
			"versionNonce": 2071763214,
			"isDeleted": false,
			"id": "0OHP_vxPxaZbaEnLJw1lu",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -43.5,
			"y": -264.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 8,
			"height": 196,
			"seed": 396761618,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327734201,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					8,
					196
				]
			]
		},
		{
			"type": "arrow",
			"version": 28,
			"versionNonce": 425232914,
			"isDeleted": false,
			"id": "eJuTg6x5xIxcfPp2LcMPY",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 91.5,
			"y": -252.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 5,
			"height": 188,
			"seed": 182601550,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327735435,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-5,
					188
				]
			]
		},
		{
			"type": "arrow",
			"version": 35,
			"versionNonce": 2013688974,
			"isDeleted": false,
			"id": "KbV3jVHTstcLVtHke2rCz",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 208.5,
			"y": -245.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 4,
			"height": 189,
			"seed": 1973516242,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327738136,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					4,
					189
				]
			]
		},
		{
			"type": "freedraw",
			"version": 64,
			"versionNonce": 1916294546,
			"isDeleted": false,
			"id": "gp57J-rykoEFDeS1_vHio",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 14.5,
			"y": -130.2421875,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 13,
			"height": 20,
			"seed": 547664590,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1708327742396,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1
				],
				[
					-1,
					6
				],
				[
					-1,
					10
				],
				[
					-1,
					12
				],
				[
					-1,
					13
				],
				[
					0,
					13
				],
				[
					1,
					12
				],
				[
					2,
					11
				],
				[
					5,
					7
				],
				[
					5,
					6
				],
				[
					6,
					3
				],
				[
					6,
					0
				],
				[
					5,
					-2
				],
				[
					3,
					-4
				],
				[
					2,
					-4
				],
				[
					-1,
					-2
				],
				[
					-2,
					1
				],
				[
					-3,
					5
				],
				[
					-3,
					7
				],
				[
					1,
					6
				],
				[
					4,
					3
				],
				[
					5,
					0
				],
				[
					5,
					-1
				],
				[
					5,
					-2
				],
				[
					5,
					-3
				],
				[
					5,
					-4
				],
				[
					2,
					-4
				],
				[
					-1,
					-4
				],
				[
					-3,
					-2
				],
				[
					-4,
					1
				],
				[
					-4,
					5
				],
				[
					-4,
					7
				],
				[
					-3,
					7
				],
				[
					-1,
					7
				],
				[
					1,
					6
				],
				[
					4,
					3
				],
				[
					5,
					-1
				],
				[
					5,
					-4
				],
				[
					5,
					-6
				],
				[
					4,
					-7
				],
				[
					2,
					-7
				],
				[
					-1,
					-5
				],
				[
					-3,
					-1
				],
				[
					-3,
					2
				],
				[
					-3,
					6
				],
				[
					-3,
					7
				],
				[
					0,
					7
				],
				[
					1,
					7
				],
				[
					2,
					4
				],
				[
					4,
					2
				],
				[
					4,
					-2
				],
				[
					3,
					-4
				],
				[
					1,
					-5
				],
				[
					-3,
					-5
				],
				[
					-6,
					-3
				],
				[
					-7,
					1
				],
				[
					-7,
					5
				],
				[
					-6,
					7
				],
				[
					-2,
					8
				],
				[
					2,
					8
				],
				[
					5,
					6
				],
				[
					5,
					6
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 38,
			"versionNonce": 46598738,
			"isDeleted": false,
			"id": "_8Cg7z-eX9D-ZbeBjLjKK",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -367.5,
			"y": 174.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 240,
			"height": 2,
			"seed": 1975733390,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1708327758486,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					240,
					-2
				]
			]
		},
		{
			"type": "rectangle",
			"version": 66,
			"versionNonce": 455045198,
			"isDeleted": false,
			"id": "q-xxXoobvu47e7p4zG-wy",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -124.5,
			"y": 88.7578125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 223,
			"height": 168,
			"seed": 1708519442,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [],
			"updated": 1708327763436,
			"link": null,
			"locked": false
		},
		{
			"id": "5JH63KAQyFA3tKRIgwdkr",
			"type": "freedraw",
			"x": -14.252133182602279,
			"y": -180.60422993513447,
			"width": 67.33333333333331,
			"height": 83.33333333333331,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 567662926,
			"version": 38,
			"versionNonce": 1443498386,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1708328004933,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.3333333333333144,
					-2
				],
				[
					-2,
					-4.666666666666657
				],
				[
					-3.3333333333333144,
					-8.666666666666657
				],
				[
					-4,
					-17.333333333333343
				],
				[
					-4,
					-30.666666666666686
				],
				[
					-4,
					-46.666666666666686
				],
				[
					-4,
					-62
				],
				[
					-4,
					-72.66666666666669
				],
				[
					-4,
					-79.33333333333331
				],
				[
					-4,
					-82.66666666666669
				],
				[
					-4,
					-83.33333333333331
				],
				[
					-2.6666666666666856,
					-82
				],
				[
					-1.3333333333333144,
					-78.66666666666669
				],
				[
					1.3333333333333144,
					-73.33333333333331
				],
				[
					4.666666666666686,
					-66
				],
				[
					10,
					-56
				],
				[
					16.666666666666686,
					-43.333333333333314
				],
				[
					24.666666666666686,
					-32.666666666666686
				],
				[
					30,
					-24
				],
				[
					37.333333333333314,
					-16.666666666666657
				],
				[
					42.666666666666686,
					-10
				],
				[
					45.333333333333314,
					-6.666666666666657
				],
				[
					46,
					-6
				],
				[
					46,
					-6.666666666666657
				],
				[
					46,
					-7.333333333333343
				],
				[
					46,
					-10
				],
				[
					46,
					-14
				],
				[
					47.333333333333314,
					-20
				],
				[
					50,
					-30
				],
				[
					52,
					-40
				],
				[
					54.666666666666686,
					-47.333333333333314
				],
				[
					57.333333333333314,
					-50.666666666666686
				],
				[
					60,
					-53.333333333333314
				],
				[
					62.666666666666686,
					-56
				],
				[
					63.333333333333314,
					-57.333333333333314
				],
				[
					63.333333333333314,
					-57.333333333333314
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				63.333333333333314,
				-57.333333333333314
			]
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 393.2521331826023,
		"scrollY": 330.94797993513447,
		"zoom": {
			"value": 1.5
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%