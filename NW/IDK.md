---
Fach: NW
Datum: Monday, 13.05.2024
Zeit: 08:11
Themen: 
Seiten: 339 -
---
---
Bsp:
n=1
l=0
m=0
s=$\pm\frac{1}{2}$
-> 2 Elektronen in Schale 1

n=2
l=0; 1
m=-1; 0; 1
s=$\pm\frac{1}{2}$
-> l=0
	m=0
	$s=\pm\frac{1}{2}$ -> s-Orbital
-> $s=\pm\frac{1}{2}$
	6 Elektronen in p-Orbital
-> 8 $e^-$ in Schale 2


n=3
l=0
	m=0
	$s=\pm\frac{1}{2}$
	-> 2 $e^-$
l=1
	m=-1; 0; 1
	mit je $s=\pm\frac{1}{2}$
	-> 6 $e^-$
l=2
	m=-2; -1; 0; 1; 2
	mit je $s=\pm\frac{1}{2}$
	-> 10 $e^-$


$\beta^-$: $n$ -> $p^{+}+ e^{-} + antiv$
$\beta^{+}$: $p^{+}$: $n+e^{+} + v$
