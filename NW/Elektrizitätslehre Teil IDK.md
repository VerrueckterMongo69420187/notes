---
Fach: NW
Datum: Monday, 19.02.2024
Zeit: 08:12
Themen: 
Seiten: 94, 95, 99, 100, 101, 102, 104
---
---
$$\phi...magnetischer Fluss$$
$$[\phi]=1Wb (Weber)$$
$$B=\frac{\phi}{A}...magnetische Flussdichte$$
$$[B]=1T(Tesla)$$
$$F=B*I*l$$
$$F=B*Q*v$$
$$I=\frac{dQ}{dt}$$![[Spule]]
$$U_{ind}=-N*\frac{d\phi}{dt}$$
$$\phi=B*A=\mu_0*\mu_r*\frac{I*N}{l}*A=\mu_0*\mu_r*\frac{N*A}{l}*I$$
$$U_{ind}=-N*\frac{d\phi}{dt}=-N*\frac{d*(\mu_0*\mu_r*\frac{NA}{l}*I)}{dt}=-\frac{\mu_0*\mu_r*N^2*A}{l}*\frac{dI}{dt}$$

$$U_{ind}=-L*\frac{dI}{dT}$$
$$L...Induktivität$$
$$\phi=B*A*cos(\varphi)=B*A*cos(\omega*t)$$
$$\varphi=\omega*t$$

$$U_{ind}=-N*\frac{d\phi}{dt}=-N*\frac{d*(B*A*cos(\omega t))}{dt}=-N*B*A*\frac{d*(cos(\omega t))}{dt}=N*B*A*\omega*sin(\omega t)$$
![[elektrizitaetslehre1.jpg]]
![[elektrizitaetslehre2.jpg]]