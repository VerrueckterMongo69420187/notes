---
Fach: NW
Datum: Monday, 04.03.2024
Zeit: 08:06
Themen: 
Seiten: "317"
---
---
# Galileitransformation
- ![[geschwindigkeitsaddition]] 
$$x=x'+vt$$
$$y=y'$$
$$z=z'$$
$$t=t'$$
$$x'=x-v*t$$
$$y'=y$$
$$z'=z$$
$$t'=t$$
- => Problem mit Lichtgeschwindigkeit (Äther)
- Bezugssystem 1 ruht
- Bezugssystem 2 bewegt ich relativ dazu in x-Richtung mit Geschwindigkeit v
- modern
# Lorentztransformation
$$x=y*x'+v*\gamma*t'$$
$$y=y'$$
$$fz=z'$$
$$t=\gamma*\frac{v}{{c_0}^2}*x'+\gamma*t'$$
mit $$\gamma=\frac{1}{\sqrt{1-\frac{v^2}{{c_0}^2}}}$$
# Zeitdilatation (Zeitdehnung)
![[zeitdilatation]]
$$s=v*t$$
$$0,3m=3*10^8s*t$$
$$t=\frac{B*10^{-1}}{B*10^8}=10^{-9}=1_{ns}$$
$$S_B=c_0*t$$
$$S_A=c_0*t_A$$
$$(c_0*t_B)^2=(v*t_B)^2+(c_0*t_A)^2$$
$${c_0}^2*{t_B}^2=v^2*{t_B}^2+{c_0}^2*{t_A}^2$$
$${c_0}^2*{t_B}^2-v^2*{t_B}^2={c_0}^2*{t_A}^2$$
$${t_b}^2*({c_0}^2-v^2)={c_0}^2*{t_A}^2$$
$${t_b}^2=\frac{{c_0}^2}{{c_0}^2-v^2}*{t_A}^2$$
$$t_B=t_A*\frac{1}{\sqrt{1-\frac{v^2}{{c_0}^2}}}$$
$${t_A}^2={t_B}^2*\frac{{c_0}^2-v^2}{{c_0}^2}={t_B}^2*(1-\frac{v^2}{{c_0}^2})$$
$$t_A=t_B*\sqrt{1-\frac{v^2}{{c_0}^2}}$$
- Eine bewegte Uhr geht langsamer als eine Ruhende
- Bei geringe Geschwindigkeit ist die Zeitdehnung vernachlässigbar n
- z.B. $v=100km/h=27,\dot{7}\frac{m}{s}$
- $t_A=t_B*\sqrt{1-\frac{27,\dot{7}^2}{299792458^2}}=t_B$
- wird für v die Lichtgeschwindigkeit c eingesetztz, wird die vom Ruhesystem aus gesehene bewegte Uhr...?
- Der Effekt gilt nicht nur für Lichtuhren, sondern generell die Zeit vergeht langsamer

# Längenkontraktion
$$l_B=l_A*\sqrt{1-\frac{v^2}{{c_0}^2}}$$
