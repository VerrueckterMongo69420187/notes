### Fakten

* Licht ist eine elektromagnetische Welle, die für das **menschliche Auge erkennbar** ist und Wellenlängen zwischen ca. 400 nm und 800 nm hat.
* In der Regel breitet sich Licht vom Ort seiner Entstehung als Kugelwelle geradlinig nach allen Seiten aus.
* Einzelne Sektoren der Kugelwelle heißen **Strahlenbündel**. Die Strahlenbündel beginnen im Kugelmittelpunkt und haben einen endlichen Öffnungswinkel.
* **Lichtstrahlen sind Strahlenbündel mit verschwindendem Öffnungswinkel**, also unendlich enge Strahlenbündel.
* Treffen Lichtstrahlen auf ein undurchsichtiges Objekt, ist die Projektion des Objektes sein Schatten.
* Die geometrische Optik oder Strahlenoptik beschreibt den Verlauf von Lichtstrahlen hinreichend genau, wenn die innerhalb des Strahlenbündels dargestellten Objekte groß im Vergleich zur Wellenlänge sind, d. h. sehr viel größer als 1 um.


Licht breitet sich in einem homogenen isotropen Medium geradlinig aus. Ein Medium ist homogen und isotrop, wenn die betrachteten physikalischen Parameter
gleich und richtungsunabhängig sind. Trifft Licht auf eine Grenzfläche zwischen zwei Medien, z. B. von homogener Luft auf eine völlig ebene Glasplatte der Dicke *d*,
dann wird:
* ein Teil **reflektiert (zurückgeworfen)**,
* ein Teil in der Platte **absorbiert (aufgenommen)** und
* ein Teil geht **durch** die Platte **hindurch** — er wird **transmittiert**.

### Strahlungsarten
![[Strahlungsarten.png]]

#### Gammastrahlung
z.B. Früher, wenn man als Schauattraktion sein Skelett mit einem Röntgengerät herzeigt, den Menschen nicht bewusst war, wie schädlich diese Strahlung war. (in 2 Jahren Rücken aufgelöst) 

## Vakuumlichtgeschwindigkeit
Licht breitet sich im Vakuum definitionsgemäß mit der Vakuumlichtgeschwindigkeit $299 792 458 m/s$ ~ $1 079 252 848,8 km/h$ aus.

## Kern- und Halbschatten
Der dunkelste Bereich eines schattigen Gebietes ist der Kernschatten. Der Halbschatten ist für eine reale Lichtquelle jener Bereich, in dem Teile der Lichtquelle sichtbar sind.

## Reflexion

### Reflexionsgesetz
Trifft Licht unter dem Winkel a zum Lot auf eine (sehr glatte) Grenzfläche, wird ein Teil unter dem Winkel $\beta = \alpha$ zum Lot reflektiert.
**Kurz: Einfallswinkel $\alpha$ = Reflexionswinkel $\beta$.**
Im Raum gilt die Zusatzbedingung, dass der einfallende, der reflektierte Strahl
und das Lot in einer Ebene liegen.

### diffuse Reflexion
![[diffuse_reflexion.png]]
Bei der **diffusen Reflexion** wird das auftreffende gebündelte Licht in alle Richtungen zerstreut. Die Bündelung geht verloren.

## Brechung
### Totalreflexion
Beim Übergang von einem optisch dichteren Medium 1 in ein optisch dünneres
Medium 2 ($n_1 < n_2$).
* nennt man den Einfallswinkel $a_{Grenz}$, für den der Brechungswinkel $\beta$ 900 ist, Grenzwinkel der Totalreflexion. $a_{Grenz} = arcsin(n_2/n_1) = arcsin(c_1/c_2) = arcsin(\lambda_1/\lambda_2)$.
* Für größere Einfallswinkel als den Grenzwinkel $a_{Grenz}$ tritt Totalreflexion auf. Der Lichtstrahl verbleibt im Medium 1; es findet keine Brechung mehr statt.

## Dispersion
Die Abhängigkeit der Brechzahl n von der Frequenz heißt Dispersion: $n=n(f)$
Rotes Licht ist in durchsichtiger Materie schneller als violettes, weshalb bei normaler Dispersion $n_{rot}<n_{violett}$ ist.

![[dispersion.png]]