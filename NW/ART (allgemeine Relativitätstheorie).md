---
Fach: NW
Datum: Monday, 15.04.2024
Zeit: 08:27
Themen: 
Seiten:
---
---
- gibt den Zusammenhang zwischen Raum-Zeit, den Massen/Energien und der Gravitation an
- Die Gravitation kann durch eine Krümmung der Raum-Zeit beschrieben werden.
- Diese Krümmung der Raum-Zeit wird durch in dem Gebiet vorhandenen Massen/Energien verursacht.
- 