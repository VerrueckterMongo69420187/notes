# Ursachen von Krisen
Krisen können verschiedene Ursachen haben:
* **interne Ursachen**: kommen aus dem Unternehmen selbst,
* **externe Ursachen**: kommen aus dem Umfeld des Unternehmens.

![[Ursachen_Krisen.png]]

## Interne Ursachen
* fast immer Managementfehler
* an erster Stelle fehlendes Controlling

# Ablauf von Krisen
Am Ende von Krisen steht oft die **Insolvenz** eines Unternehmens. Das Unternehmen kann seine Schulden nicht mehr bezahlen **(keine Liquidität)** oder es ist **überschuldet** (das Eigenkapital ist durch Verlust aufgebraucht und daher negativ).

![[Ablauf_Krisen.png]]

# Controlling zum Erkennen von Krisen
![[Krisenarten.png]]
