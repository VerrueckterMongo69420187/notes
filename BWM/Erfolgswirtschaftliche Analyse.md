---
Fach: BWM
Datum: Thursday, 19.09.2024
Zeit: 08:31
Themen: 
Seiten: 208ff
---
---
![[Aufwandskennzahlen.png]]

![[Umsatzkennzahlen.png]]![[Gewinn (Verlust).png]]
![[Gewinnbegriffe.png]]
![[Bsp Ergebnisanalyse.png]]
# BSP
## Ü6.7
### Angabe
![[Ü6.7.png]]
### 1.
Material-/Wareneinsatz: 66,07%
### 2.
Personalintensität: 19,02%
### 3.
Umsatz je Beschäftigung: 217000€
### 4.
Bruttogewinn: 1100000
### 5.
Handelsspanne: 33,8%
### 6.
Rohaufschlag: 51,16%



![[Rentabilitätsanalyse.png]]
![[Rentabilität des Gesamtkapitals.png]]
![[Umsatzrentabilität.png]]
![[Return on Investment (ROI).png]]
![[Pasted image 20240926080826.png]]
![[Pasted image 20240926080916.png]]
![[Pasted image 20240926080941.png]]
# Ü6.8
## Angabe
![[Pasted image 20240926081009.png]]
## Rentabilität des Eigenkapitals
62,73%
## Rentabilität des Gesamtkapitals
39,29%
## Umsatzrentabilität
21,38%
## Return on Investment (ROI)
39,31%
# Ü6.9
## Angabe
![[Pasted image 20240926083326.png]]
![[Pasted image 20240926083340.png]]
![[Pasted image 20240926083358.png]]
## Lösungen
### a)
#### Aufwandskennzahlen
- Material-/Warenintensität
	- 2022: 62,19%
	- 2023: 65,27%
- Personalintensität
	- 2022: 20,06%
	- 2023: 18,9%
#### Ertragskennzahlen
- Umsatz je Beschäftigung (auf 100€ gerundet)
	- 2022: 199 000€
	- 2023: 224 000€
#### Ergebniskennzahlen
- Bruttogewinn
	- 2022: 750 000€
	- 2023: 776 000€
- Handelsspanne
	- 2022: 37,69%
	- 2023: 34,64%
- Rohaufschlag
	- 2022: 60,48%
	- 2023: 53%
#### Kennzahlen der Rentabilität
- Rentabilität des Eigenkapitals
	- Durchschn. Eig. Kap.
		- 2022: 274 000€
		- 2023: 284 000€
	- Rentabilität 
		- 2022: 70,8%
		- 2023: 65,85%
- Rentabilität des Gesamtkapitals
	- Durchschn. Ges. Kap.
		- 2022: 570 000€
		- 2023: 581 000€
	- Rentabilität
		- 2022: 35,61%
		- 2023: 33,56%
- Umsatzrentabilität
	- 2022: 10,2%
	- 2023: 8,71%
- Return on Investment
	- 2022: 35,61%
	- 2023: 33,58%
### b)
**Beurteilung der Kennzahlen:**

#### 1. **Rohaufschlag**
- **Rohaufschlag 2020: 62,4 %, 2021: 61,3 %, Branchendurchschnitt 2023: 51,0 %**
  - **Beurteilung**: Der Rohaufschlag des Unternehmens liegt deutlich über dem Branchendurchschnitt, was auf eine starke Preissetzungsmacht oder niedrige Beschaffungskosten hinweist. Der leichte Rückgang von 62,4 % (2020) auf 61,3 % (2021) deutet jedoch auf eine Reduzierung dieser Vorteile hin.
  - **Mögliche Ursachen für den Rückgang**:
    - **Erhöhte Beschaffungskosten**: Eventuell sind die Einkaufspreise gestiegen, was den Rohaufschlag schmälert.
    - **Stärkere Wettbewerbsdynamik**: Um konkurrenzfähig zu bleiben, könnte das Unternehmen gezwungen gewesen sein, Preise zu senken.
    - **Inflation**: Allgemeine Preisanstiege oder wirtschaftliche Unsicherheiten könnten die Margen belastet haben.
    - **Wechsel der Produktpalette**: Ein Wandel zu Produkten mit geringerer Marge könnte auch den Rückgang des Rohaufschlags erklären.

#### 2. **Rentabilität des Eigenkapitals**
- **Beurteilung**: Die Rentabilität des Eigenkapitals misst, wie effizient ein Unternehmen das von den Eigentümern zur Verfügung gestellte Kapital einsetzt, um Gewinne zu generieren. Ein hoher Wert deutet auf eine gute Nutzung des Eigenkapitals hin.
  - **Zu berücksichtigender Gewinn**: Für eine realistische Beurteilung der Rentabilität des Eigenkapitals sollte der **Jahresüberschuss (Netto-Gewinn)** herangezogen werden. Dieser berücksichtigt sämtliche Aufwendungen und Erträge und spiegelt das tatsächliche Ergebnis nach Steuern wider.

#### 3. **Umsatzrentabilität**
- **Umsatzrentabilität 2020: 10,2 %, 2021: 10,1 %, Branchendurchschnitt 2023: 8,0 %**
  - **Beurteilung**: Die Umsatzrentabilität des Unternehmens liegt konstant über dem Branchendurchschnitt, was auf eine solide Profitabilität hinweist. Der geringe Rückgang von 10,2 % auf 10,1 % zwischen 2020 und 2021 ist unbedeutend und zeigt eine stabile Geschäftsentwicklung. Im Vergleich zur Branche ist das Unternehmen profitabler.

# Ü6.10
## Angabe
![[Pasted image 20241001114820.png]]
## Lösungen
### a)
#### Kennzahlen der Aktivseite der Bilanz
- Anteil des Anlagevermögens