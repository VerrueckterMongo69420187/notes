Im Exekutionsverfahren werden die Parteien als
+ betreibende Partei und
+ verpflichtete Partei
bezeichnet.

![[Exekutionsverfahren.png]]

Zur **Einbringung von Geldforderungen** kann Exekution geführt werden,
und zwar auf
* Fahrnisse (bewegliche Sachen),
* Forderungen (z. B. Lohnpfändung),
* Liegenschaften oder
* Vermögensrechte.

# Grundsätze
![[Prinzipien_Exekutionsverfahren.png]]

# Ablauf
## Schritte im Exekutionsverfahren: 
Liegt ein Exekutionstitel vor, werden folgende Schritte eingeleitet:
![[Schritte_exekutionsverfahren.png]]
### Exekution auf bewegliche Sachen **(Fahrnisexekution)**

* Der Gerichtsvollzieher erstellt ein Pfändungsprotokoll.
* Gleichzeitig wird der voraussichtlich erzielbare Erlös aus einer Verwertung der Gegenstände geschätzt. Diese Schätzung ist der sogenannte **Bleistiftwert**.
* Die Pfändung wird auf den Gegenständen mit **Pfändungsmarken** - im Volksmund auch „Kuckuck" genannt - ersichtlich gemacht. 

### Exekution auf unbewegliche Sachen

* **zwangsweise Pfandrechtsbegründung:** Der betreibende Gläubiger kann die zwangsweise Pfandrechtsbegründung verlangen.
* **Zwangsverwaltung**: Das Gericht bestellt eine Person, die die Liegenschaft bewirtschaftet und die Miete bzw. Pacht einhebt. 
* **Zwangsversteigerung**: Der Wert der Liegenschaft wird von einem oder einer vom Gericht bestellten Sachverständigen geschätzt.


### Exekution auf Forderungen
![[Lohnpfändung.png]]
Der pfändbare Betrag ist vom Arbeitgeber bzw. von der Arbeitgeberin anhand von **Lohnpfändungstabellen** selbst zu errechnen und regelmäßig an den Gläubiger zu überweisen, bis dessen Forderung befriedigt ist.
### Exekution auf Vermögensrechte

Vermögensrechte sind beispielsweise Anteile an einer Gesellschaft, der Teilungsanspruch des Miteigentümers an einer Liegenschaft und Berechtigungen aus virtuellen Währungen („Krypto-Assets"). Der Vollzug und die Verwertung solcher Rechte erfolgt durch einen vom Exekutionsgericht bestellten Verwalter.

### Beendigung des Exekutionsverfahrens

Das Exekutionsverfahren ist beendet, wenn die Forderungen des betreibenden Gläubigers befriedigt wurden.

---

### Übungen
#### Ü6.9

![[Ü6.9.png]]

#### K6.2
##### a)
nein

##### b)
1. **Pfändung von Vermögenswerten:** Die Bank kann versuchen, die Wertgegenstände von Herrn Winter zu pfänden, um die Schulden einzutreiben. Dies könnte die Sammlung von Briefmarken und den PKW umfassen, abhängig von den Pfändungsgrenzen und den örtlichen Gesetzen.
    
2. **Gehaltspfändung:** Da Herr Winter ein monatliches Einkommen von €2.500,- hat, könnte die Bank versuchen, einen Teil dieses Einkommens durch eine Gehaltspfändung einzuziehen. Es gibt jedoch bestimmte Freigrenzen, die in vielen Ländern gelten und die Höhe des pfändbaren Einkommens begrenzen.
    
3. **Kontopfändung:** Sofern Herr Winter Bankkonten besitzt, könnten diese im Rahmen einer Kontopfändung genutzt werden, um die ausstehenden Schulden zu begleichen.

#### K6.3
Frau Sieberer sucht ein Wochenendhaus im Grünen und interessiert sich für Immobilienversteigerungen. Sie kann sich über Zeitungsanzeigen, Online-Plattformen, Behördenwebsites und Immobilienmakler nach Versteigerungsterminen informieren. Wichtig ist, die spezifischen Abläufe solcher Versteigerungen zu verstehen.