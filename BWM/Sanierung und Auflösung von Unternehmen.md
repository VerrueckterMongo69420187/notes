# Insolvenz
![[Insolvenz.png]]
**Einzelunternehmen und Personengesellschaften sind bei Zahlungsunfähigkeit insolvent.** Das ist dann der Fall, wenn fällige Schulden nicht mehr in angemessener Frist bezahlt werden können.

Für **Kapitalgesellschaften** sind die Regelungen strenger: Nicht nur Zahlungsunfähigkeit löst eine Insolvenz aus. Liegt Überschuldung vor und gibt es für das Unternehmen eine negative Fortbestandsprognose, ist die Gesellschaft bereits insolvent.

# Insolvenzverfahren
![[Insolvenzverfahren.png]]

Außerdem muss den Gläubigern beim **Sanierungsverfahren in Eigenverwaltung** eine Rückzahlungsquote der Schulden von mindestens 30% angeboten werden, die innerhalb von zwei Jahren erfüllt sein muss. bei der **Fremdverwaltung** 20%.

# Konkurs
Im Konkurs ernennt das Gericht eine/n Insolvenzverwalter/in (Masseverwalter/in), mit dem Ziel, das Vermögen des Unternehmens bestmöglich zu. Der/die Insolvenzverwalter/in kann
* das Unternehmen als Ganzes verkaufen Oder
* das Anlage- und Umlaufvermögen des Unternehmens einzeln verwerten und dann schließen (das Unternehmen wird „zerschlagen").
![[Konkursverfahren.png]]

# Gläubiger/innen im Insolvenzverfahren
![[Rangordnung_Forderung.png]]

![[Bsp_Konkurs.png]]

# Übungen
## Ü7.7

![[ü7.7a.png]]
![[ü7.7b.png]]
### Lösung

|Nummer| Krisenabschnitt | Controllinginstrument |
|:---:|:----------:|:---------------------:|
|a)| Erfolgskrise | Gewinn-/Verlustrechnung |
|b)|Liquiditätskrise|kurzfristige Finanzpläne|
|c)|Strategiekrise|Stärken-Schwächen-Analyse|
## Ü7.8
![[ü7.8.png]]

### Lösung

|Nr.|Interne bzw. externe Ursache|extern/intern?|
|:------:|:--------------------:|:---------:|
|a)|Insolvenz von Kunden/Lieferanten|extern|
|b)|mangelnde Qualität der Produkte|intern|
|c)|Fehler im Marketing|intern|
|d)|steigende Rohstoffpreise|extern|
|e)|starker Konkurrenzdruck|extern|
|f)|Fehler in der Marktanalyse|intern|

## Ü7.11
![[ü7.11.png]]

### Lösung

|Nr|Strategiekrise|Erfolgskrise|Liquiditätskrise|
|:-:|:-:|:-:|:-:|
|a)|Sie haben durch kein neues Design Marktanteile verloren|||
|b)||Da er einen anhaltenden Umsatzrückgang hat||

## Ü7.18
![[ü7.18.png]]

### Lösung
#### a)
Sanierungsverfahren mit Fremdverwaltung

#### b)
Umsatzeinbruch durch Corona-Krise

#### c)
$940000-(0,2*940000)=752000$

#### d)
zwei Jahre

## Ü7.20
![[ü7.20.png]]

### Lösung
#### a)
Insolvenzverwalter

#### b)
20% der Schulden

#### c)
2 Jahre

#### d)
die Mehrheit der Gläubiger

#### e)
es kommt zum Konkurs

## Ü7.23
![[ü7.23.png]]

### Lösungen

#### a)
$(79000-69000)/340000=2,94$

#### b)
Die Konkursquote ist ein Prozentsatz, der angibt, wie viel Prozent der Insolvenzforderungen mit den verfügbaren Vermögenswerten im Insolvenzverfahren bedient werden können

## Ü7.28
![[ü7.28.png]]

### Lösungen

|Nr|Insolvenzantrag möglich, weil|Insolvenzantrag nicht möglich, weil|
|:-:|:-:|:-:|
|a)|Ein Insolvenzantrag ist möglich, aber dazu ist im vorliegenden Fall nur die Geschäftsführung der Gerstner KG berechtigt.||
|b)|Ein Insolvenzantrag ist möglich wenn Tanja Sabic von einem Konkurs der Gerstner KG ausgeht.||
|c)||Der Insolvenzantrag ist nicht möglich- Er kann nur durch die Gerstner KG gestellt werden und im Anschluss daran wird im gerichtlichen Insolvenzverfahren entschieden, welche Art von Sanierungsverfahren durchgeführt wird.|
|d)|Der Insolvenzantrag ist möglich. Außerdem muss die Gerstner KG einen Zahlungsplan mit Details zur Begleichung der offenen Verbindlichkeiten vorlegen und die Mehrheit der Gläubiger/innen muss diesem zustimmen||
|e)||Der Insolvenzantrag ist nicht möglich. Das Gericht ist ohne vorliegenden Insolvenzantrag der Gerstner KG, der Sabic GmbH oder einesanderen Gläubigers/einer anderen Gläubigerin nicht berechtigt, ein Insolvenzverfahren zu eröffnen|




