---
Fach: BWM
Datum: Monday, 08.04.2024
Zeit: 10:09
Themen: 
Seiten:
---
---
# Geschäftsidee
## Welches Produkt/welche Dienstleistung bieten Sie an?
Wir bieten motorisierte Haxltaxis in Touristenstädten an, für Privatpersonen werden auch getunte zur Verfügung gestellt auf Anfrage
## Worin unterscheiden Sie sich von der Konkurrenz?
Dass wir Motorisierte haben.
## Erarbeiten Sie Ihr USP!
Ein motorisiertes Haxltaxi mit verschieden Apekten, die es so noch nicht gibt in Österreich.
## Definieren Sie Ihre Unternehmensziele!
Genug Profit erzielen, damit man auch das Unternehmen mehr ins Ausland auslagern kann.

# Marktorientierung
## Für welche Menschen ist das Produkt bestimmt? Wie viele gibt es?
Touristen/Privatpersonen, 10000
## Beschreiben Sie die Konkurrenz
Es gibt ein Unternehmen in Hofstetten, es gibt auch ein Unternehmen in Italien.
## Definieren Sie Ihre Zielgruppe!#
Touristen/Fahrradenthusiasten
## Beschreiben Sie Ihr Produkt genau!
Ein Haxltaxi, das mit vielen verschiedenen Features wie kleinen ausklappbaren Tischen in den hinteren Reihen oder Panoramaroof ausgestattet ist. In der Stadt hat es einen leistungsstarken Elektromotor eingebaut, bei Privatanfragen kann man sich auch für einen Verbrennermotor entscheiden. Das getunte Haxltaxi ist generell mit der üblichen Haxltaxi-Ausstattung ausgestattet und noch mehr innovativen Features, um das Fahrerlebnis mit seinen Kollegen zu verschönern.
## Legen Sie Ihre Preise fest! Erläutern Sie die Strategie dahinter.
Mieten: 15€/Stunde, jede Stunde 2€ billiger, 10€ Cap
Privatanfrage: kommt drauf an
Getränkestandl: 5€/Soda

Strategie: Die Preise sollen die Kunden dazu verlocken, dass sie länger fahren. Der Stundenrabatt wird die Kunden noch eher anziehen, da es billiger oder falsch interpretiert wird.
## Wie setzen sie ihr Produkt ab?
Wir haben eine Art Mini-Halle, in der die Haxltaxis zum Vermieten stehen. Vor diesem Gebäude steht ein kleiner Stand, wo man sich ein Haltaxi mieten kann.
## Wie können Sie die Zielgruppe über ihre Produkte informieren?
- TikTok
- Insta
- Plakate


# Mitarbeiter
## Wie viele Mitarbeiter werden Sie brauchen? Welche Kompetenzen erfüllen Sie selbst?
Pro Stand:
- 2 Instandhalter für die Haxltaxis
- 1 Vermieter für Haxltaxis
## Erstellen Sie ein Organigramm Ihres Unternehmens. Tragen Sie zu besetzende Stellen inklusive kurzer Stellenbeschreibung ein.

## Welche Leistungen werden Sie auf dem Markt zukaufen?
- Instandhaltung
- Salesmen
## Welche Partner (Informationen von Banken/Steuerberatern/Lieferanten) werden Sie brauchen?
- Partner, der Produktion für den Anfang übernimmt
- Bank
- Partner, der uns Tipps zu Unternehmensstandorten gibt
- Getränkestandl Partner (Türkisch)
# Finanzplan
## Erstellen sie einen Finanzplan