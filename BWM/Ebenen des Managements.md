---
Fach: BWM
Datum: Tuesday, 15.10.2024
Zeit: 11:47
Themen: 
Seiten: 110ff
---
---
# Ü 3.11
![[Pasted image 20241015114753.png]]
# Ü 3.22
![[Pasted image 20241015114823.png]]
## a)
- Umsatzsteigerung
- Kostensteigerung
- Risikosteigerung
## b)
- Investitionskosten
- Langfristige Einsparungen
- Personalpolitik
# Ü 3.23
![[Pasted image 20241015115247.png]]
Aufnahme neuer Mitarbeiter
# Ü 3.24
![[Pasted image 20241015115258.png]]
- **Logistik**: Umstellung auf kontaktlose Lieferungen.
- **Einzelhandel**: Einführung von Online-Shopping und Abholservices.
- **Produktion**: Anpassung von Produktionslinien für Hygieneprodukte.
- **Personalmanagement**: Einführung von Homeoffice und Schichtplänen.
- **Gastronomie**: Umstellung auf Lieferservices und Abholungen.

# Ü 3.25
![[Pasted image 20241015115351.png]]
![[Pasted image 20241015115457.png]]
![[Pasted image 20241015115515.png]]
![[Pasted image 20241015115528.png]]
# Ü 3.26
![[Pasted image 20241015115559.png]]
- **Normative Ebene**: Festlegung von Nachhaltigkeitswerten und ethischen Grundsätzen, wie der langfristige Fokus auf Klimaneutralität.
    
- **Strategische Ebene**: Ausbau des Produktsortiments mit neuen, nachhaltigen Getränken, um den Marktanteil zu erhöhen.
    
- **Operative Ebene**: Implementierung effizienter Produktionsprozesse, die den Energieverbrauch und die Emissionen weiter reduzieren.
# Ü 3.27
![[Pasted image 20241015115657.png]]
![[Pasted image 20241015115759.png]]
# Ü 3.28
![[Pasted image 20241015115826.png]]
- Das Leitbild gibt die strategische Ausrichtung vor und beeinflusst direkt die Unternehmenskultur.
- Eine starke, vom Leitbild geprägte Kultur führt zu engagierten Mitarbeitern, die sich mit den Unternehmenszielen identifizieren.
- Dies resultiert in höherer Produktivität, besserer Teamarbeit und Innovationskraft.
- Kunden nehmen eine konsistente Unternehmensidentität wahr, was Vertrauen und Loyalität fördert.
- All diese Faktoren tragen letztendlich zum Unternehmenserfolg bei, sowohl in finanzieller Hinsicht als auch in Bezug auf langfristiges Wachstum und Nachhaltigkeit.
# Ü 3.29
![[Pasted image 20241015120140.png]]
![[Pasted image 20241015120154.png]]
a) Beurteilung der Klarheit der Werte:

H&M: Die Werte sind relativ klar definiert. Sie beschreiben konkrete Verhaltensweisen und Einstellungen.

Airbnb: Die Werte sind etwas weniger klar definiert als bei H&M, aber immer noch verständlich. Sie geben allgemeine Richtlinien für das Verhalten der Mitarbeiter.

IKEA: Die Werte sind klar definiert. Sie beschreiben spezifische Verhaltensweisen und Einstellungen, die von Mitarbeitern erwartet werden.

b) Würde ich in dem Unternehmen arbeiten wollen? Begründung: Diese Frage ist sehr subjektiv und hängt von persönlichen Präferenzen ab. Ich als KI-Assistent kann keine persönlichen Präferenzen ausdrücken, aber ich kann erklären, dass die Entscheidung davon abhängen würde, wie gut die beschriebenen Werte zu den eigenen Werten und Arbeitsvorstellungen passen.

c) Beurteilung, ob Mitarbeiter die Werte nach außen transportieren können: Ohne persönliche Erfahrungen mit Mitarbeitern dieser Unternehmen kann ich diese Frage nicht beantworten. Die Fähigkeit, Unternehmenswerte nach außen zu transportieren, hängt von vielen Faktoren ab, einschließlich der internen Kommunikation, der Schulung der Mitarbeiter und der tatsächlichen Umsetzung der Werte im Arbeitsalltag.

# Ü 3.30
![[Pasted image 20241015120116.png]]
- **Mission**: Konkreter als Vision, warum das Unternehmen existiert und welcher positive Beitrag für Kunden und Gesellschaft geleistet wird.
- **Vision**: Welche Vorstellungen ein Unternehmen von der eigenen Zukunft hat und wo es sich hin entwickeln möchte.
# Ü 3.31

| Sachverhalt                       | strategisches Management                                                                                 | operatives Management             |
| --------------------------------- | -------------------------------------------------------------------------------------------------------- | --------------------------------- |
| Zeithorizont                      | 1-5 Jahre                                                                                                | 1 Jahr                            |
| Wer trifft welche Entscheidungen? | Top und Middle-Management                                                                                | mittlere und untere Führungsebene |
| Managementinstrumente             | Branchen- und Umweltanalysen, SWOT-Analyse, Gestaltung der Aufbau- und Ablauforganisation, Führungsstile | PDCA-Kreislauf                    |
# K 3.1)

## 1. Aufgaben, die auf Viktor als Geschäftsführer zukommen

- **Strategische Planung:** Entwicklung und Umsetzung der Unternehmensstrategie.
- **Operatives Management:** Überwachung der täglichen Geschäftsabläufe.
- **Finanzmanagement:** Budgeterstellung und Kontrolle der Finanzen.
- **Personalmanagement:** Rekrutierung, Schulung und Mitarbeiterführung.
- **Marketing und Vertrieb:** Entwicklung von Marketingstrategien und Verkaufsförderung.
- **Nachhaltigkeit:** Implementierung umweltfreundlicher und sozial verantwortlicher Maßnahmen.
- **Compliance:** Sicherstellung der Einhaltung gesetzlicher Vorschriften.
- **Risikomanagement:** Identifikation und Management von Unternehmensrisiken.

## 2. Ebene des Managements und zu treffende Entscheidungen

- **Managementebene:** **Top-Management (Strategische Ebene)**
- **Entscheidungen auf dieser Ebene:**
    - Festlegung der Unternehmensvision und -mission.
    - Langfristige Zielsetzung und strategische Ausrichtung.
    - Investitionsentscheidungen und Ressourcenallokation.
    - Aufbau und Anpassung der Organisationsstruktur.
    - Entwicklung von Nachhaltigkeits- und Wachstumsstrategien.

## 3. Managementfunktionen und deren Zusammenhänge

- **Planung:** Festlegung von Zielen und Strategien.
- **Organisation:** Strukturierung von Ressourcen und Aufgaben.
- **Führung:** Motivation und Anleitung der Mitarbeiter.
- **Kontrolle:** Überwachung der Zielerreichung und Anpassung der Strategien.

**Zusammenhänge:** Planung bildet die Grundlage für Organisation und Führung, während Kontrolle sicherstellt, dass die geplanten Ziele erreicht werden und notwendige Anpassungen vorgenommen werden.

## 4. Konzept für ein Leitbild

**Leitbild von fAIR'n fRESH Fashion**

_fAIR'n fRESH Fashion bietet modebewussten Menschen aller Altersgruppen hochwertige, trendige und nachhaltige Bekleidung zu fairen Preisen. Wir verpflichten uns zu umweltfreundlichen Materialien, ethischen Produktionsprozessen und sozialer Verantwortung. Durch Innovation, exzellenten Kundenservice und eine positive Unternehmenskultur streben wir danach, führend in der nachhaltigen Modebranche zu sein._

**Managementebenen:**

- **Normativ:** Werte wie Nachhaltigkeit, Qualität und Integrität.
- **Strategisch:** Langfristige Ziele und Marktpositionierung.
- **Operativ:** Tägliche Umsetzung der Nachhaltigkeitsmaßnahmen.

## 5. Handlungsempfehlungen zur Vermeidung einer schlechten Unternehmenskultur

1. **Förderung eines positiven Arbeitsumfelds:** Regelmäßige Team-Events und Anerkennungen.
2. **Offene Kommunikation:** Etablierung von Feedback-Runden und transparenten Kommunikationskanälen.
3. **Gesundheitsfördernde Maßnahmen:** Ergonomische Arbeitsplätze und Zugang zu Gesundheitsprogrammen.
4. **Mitarbeiterentwicklung:** Angebote für Schulungen und Weiterbildungsmöglichkeiten.
5. **Faire Vergütung und Benefits:** Wettbewerbsfähige Gehälter und flexible Arbeitsmodelle.

## 6. Verantwortung gegenüber Gesellschaft und Umwelt verankern

### a) Konkrete Maßnahme

**Einführung eines Nachhaltigkeitsprogramms:** Nutzung umweltfreundlicher Materialien, Reduzierung des CO₂-Fußabdrucks und Förderung fairer Arbeitsbedingungen.

### b) Gesamtkonzept für die Umsetzung

- **Normative Ebene:**
    
    - **Strategie:** Definition der Unternehmenswerte bezüglich Nachhaltigkeit.
    - **Entscheidungen:** Erstellung eines Leitbildes und Verhaltenskodex.
    - **Entscheidungsträger:** Geschäftsführer und Ethikkomitee.
    - **Managementinstrumente:** Leitbild, Schulungen.
- **Strategische Ebene:**
    
    - **Strategie:** Integration nachhaltiger Ziele in die Unternehmensstrategie.
    - **Entscheidungen:** Auswahl nachhaltiger Lieferanten, Investition in grüne Technologien.
    - **Entscheidungsträger:** Geschäftsführung, Nachhaltigkeitsbeauftragter.
    - **Managementinstrumente:** SWOT-Analyse, Balanced Scorecard.
- **Operative Ebene:**
    
    - **Strategie:** Umsetzung der nachhaltigen Maßnahmen im Tagesgeschäft.
    - **Entscheidungen:** Anpassung der Produktionsprozesse, Einführung von Recycling.
    - **Entscheidungsträger:** Abteilungsleiter, Projektmanager.
    - **Managementinstrumente:** Prozessoptimierung, Monitoring-Tools.

**Konkrete Strategie:** _fAIR'n fRESH Fashion integriert nachhaltige Praktiken in alle Geschäftsbereiche bis 2026 durch umweltfreundliche Materialien, CO₂-Reduktion und faire Arbeitsbedingungen._