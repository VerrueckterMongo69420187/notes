---
Fach: BWM
Datum: Wednesday, 20.11.2024
Zeit: 09:44
Themen: 
Seiten: 101-
---
---
# Unternehmen führen
**Aufgaben Führungskräfte:**
- Existenz von Unternehmen sichern
- Unternehmen gestalten, lenken, entwickeln

**Aufgabenschritte**
![[Pasted image 20241120094949.png]]

**Stufen des Managements**:
![[Pasted image 20241120095055.png]]

## Top-Management
Unternehmensleitung trifft Entscheidungen:
- wo Unternehmenshauptsitz
- in welchen Ländern tätig
- welche Produkte herstellen

## Middle-Management
Bereichs-/Unternehmensleiter treffen Entscheidungen einzelner Bereiche bzw. Abteilung und setzen Entscheidungen von Top-Management um.

**Beispiel:**
Marketing Manager ist für Werbe- und PR-Maßnahmen zuständig.

## Lower-Management
Gruppenleiter treffen Entscheidungen, die ihr Team betreffen. Abteilungen können in Gruppen/Teams aufgeteilt werden, z.B. nach Produkten oder Regionen. 

**Beispiele:**
- Teamleiterin genehmigt Urlaubsplanung von Mitarbeiter
- Maurer-Vorarbeiter kontolliert/versperrt Geräte auf Baustelle

## Wie werden Unternehmen geführt
![[Pasted image 20241120100040.png]]

- Planung ist kurz- und langfristig 
- Top-Management legt Unternehmensziele fest, kümmert sich um Unternehmensstrategie
- Unternehmensgerüst wird von Top-Management festgelegt
- **Aufbauorganisation**: Struktur/Gliederung von Unternehmen, wer macht welche Aufgaben
- **Ablauforganisation:** Wie werden Aufgaben erfüllt, welche Prozesse werden durchgeführt

# Ebenen des Managements
![[Pasted image 20241120101133.png]]

## Normatives Management
### Aufgaben
![[Pasted image 20241120101213.png]]
### Leitbild
![[Pasted image 20241120101255.png]]
### Vision
- kurz und klar
- beantwortet Frage: "Wohin gehen wir?"

### Werte und Normen (nd so wichtig)
- Werte und Normen = Kern jedes Unternehmens  
- Bilden Leitbild mit Vision und Mission  
- Prägen Unternehmenskultur und Identität
- Sichtbar und authentisch vertreten  
- Stärken Mitarbeiterbindung und Motivation  
- Fördern Teamgeist und Produktivität  
- Einfluss auf Image und Erfolg  
- Beispiele: Qualität, Zuverlässigkeit, Umweltbewusstsein

### Unternehmenskultur
Entsteht aus Leitbild, wie formulierte Werte gelebt werden.

## Strategisches Management
### Aufgaben
![[Pasted image 20241120102547.png]]
## Operatives Management
### Aufgaben
![[Pasted image 20241120102622.png]]

# Führen und Führungstheorien
- Führen ist ein **Prozess**: fortlaufend
- **Führung bedeutet Über- und Unterordnung**: Führungskraft übergeordnet, Geführten untergeordnet
- **Beeinflussung** von Personen/Gruppen: Führender beeinflusst durch Handlungen andere
- **Kommunikation**: Führung gelingt durch gute Kommunikation
- **Macht**: Führungskräfte üben Macht über Mitarbeiter aus durch Beeinflussung des Verhaltens von Mitarbeitern
- **Entscheidung**: Führungskraft trifft Entscheidungen
- **Zielorientierung**: Zweck der Führung, Mitarbeiterinnen zum Arbeiten zu bringen
- **Verantwortung**: tragen Verantwortung für Zielerfüllung und Mitarbeiter, sollen als Vorbild dienen.

## Personenzentrierte Führungstheorien
### Great-Man-Theory
"Geborene Führer" sollen Führungskräfte sein, Geführten hatten keinen Einfluss auf Geschehen

### Eigenschaftstheorie
Verfeinerung von Great-Man-Theory, Untersuchung erfolgreicher Charaktereigenschaften/Fähigkeiten für erfolgreiche Führungskräfte

## Verhaltenstheorien
### Kontinuum-Theorie
![[Pasted image 20241120104303.png]]

### Verhaltensgitter-Modell
![[Pasted image 20241120104344.png]]

## Situationstheorien
### Situationstheorie-Reifegrad-Ansatz
![[Pasted image 20241120231419.png]]

## Neue Führungstheorien-der systematische Ansatz
- Verstehen der Welt vereinfachen
- behauptet, dass es verschiedene Systeme gibt, die sich gegenseitig abgrenzen
- ein Unternehmen hat ein System, um das es sich kümmern soll
![[Pasted image 20241120105119.png]]

# Führungsinstrumente
## Direkte Beeinflussung
![[Pasted image 20241120105322.png]]
![[Pasted image 20241120105429.png]]
## Indirekte Beeinflussung
![[Pasted image 20241120105336.png]]
![[Pasted image 20241120105456.png]]
## Management by Objectives (MbO), Führen mit Zielvereinbarung
Jahresbeginn -> Untergebene und Führungskraft vereinbaren **konkrete Ziele** -> 1 Jahr später... -> Besprechung, ob/in welchem Ausmaß Ziele erreicht wurden -> Repeat

![[Pasted image 20241120105851.png]]
![[Pasted image 20241120110033.png]]

## Führungsgrad der systemischen Führungstheorie (nach Fredmund Malik, St. Gallener Schule)
![[Pasted image 20241120110146.png]]
### 5 Aufgaben
1) **für Ziele sorgen**: Mitarbeitenden Ziele (Unternehmen, Abteilung, persönlich) kommunizieren
2) **Organisieren**: Aufbau-/Ablauforganisation gestalten, sodass Mitarbeiter Aufgaben effizient erfüllen
3) **Entscheiden**: Meinungen in Entscheidung einbeziehen, erst reiflich überlegen
4) **Kontrollieren, messen, beurteilen**: erkennen, ob Ziele erreicht wurden oder ob Hilfe gebraucht wird
5) **Menschen fördern und entwickeln**: selbsterklärend
### 7 Werkzeuge
1) **Sitzungen**: sollen strukturiert ablaufen
	- Besprechungsleiter
	- Tagesordnung mit zu besprechenden Punkten
	- Zeitplan
	- Protokoll
2) **Berichte, schriftliche Kommunikation**: Schriftstücke verständlich verfassen
3) **Stellengestaltung**: sollen noch immer Herausforderung darstellen, nicht überlasten
4) **Persönliche Arbeitsmethodik:** Führungskräfte für sich selbst Methode entwickeln, wann sie welche Arbeiten erledigen
5) **Budget und Budgetierung**: Alle Ausgaben/Einnahmen geordnet geplant. Welche Ausgaben notwendig/verzichtbar sind, planen.
6) **Leistungsbeurteilung**: regelmäßig Feedback geben (Weiterbildungsmöglichkeiten,...)
7) **Systematische Müllabfuhr**: Routinen, die es schon länger gibt und aus Prinzip weitergemacht werden, soll Führungskraft stoppen, wenn sie nicht notwendig sind.