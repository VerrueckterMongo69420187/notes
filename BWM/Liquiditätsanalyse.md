---
Fach: BWM
Datum: Thursday, 12.09.2024
Zeit: 08:02
Themen: 
Seiten: 201-
---
---
# Theorie
![[Liquiditätsanalyse.png]]
![[Liquiditätsgrade-1.png]]
![[Liquiditätsgrade-2.png]]
![[Working Capital.png]]
![[Gliederung-Vermögen-Fremdkapital.png]]
![[Cashflow.png]]
![[Operativer Cashflow.png]]![[Cashflow-Praktiker.png]]
![[Cashflow-Prozent.png]]
![[Fiktive Schuldentilgungsdauer.png]]
# Übungsbeispiel
![[Übungsbeispiel Liquiditätsanalyse-1.png]]
![[Übungsbeispiel Liquiditätsanalyse-2.png]]
# Übungen
## Ü6.6
### Angabe
![[Ü6.6-1.png]]![[Ü6.6-2.png]]
### Lösung
#### a)
- Kennzahlen der statischen Liquiditätsbeurteilung
	- Liquidität 2. Grades
		- 2022: 93,27%
		- 2023: 89,83%
	- Working Capital
		- 2022: 226000
		- 2023: 236000
- Kennzahlen der dynamischen Liquiditätsbeurteilung
	- Operativer Cashflow
		- 2022: 212000€
		- 2023: 213000€
	- Cashflow (Praktiker-Formel)
		- 2022: 232000€
		- 2023: 219000€
	- Cashflow in Prozent der Betriebsleistung (auf Basis der Praktiker-Formel)
		- 2022: 11,6%
		- 2023: 9,76%