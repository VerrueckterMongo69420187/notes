---
Fach: BWM
Datum: Wednesday, 11.09.2024
Zeit: 09:43
Themen: 
Seiten: 27-28
---
---
# K1.2
![[K1.2-1.png]]
![[K1.2-2.png]]
## 1
- He is a Second Hand shop owner, good for the environment
- He has an online shop
## 2
both, he probably sells new stuff in his shop in Graz and he sells second hand so retail
## 3
he trades in cameras and equipment for cameras
## 4
I think self service is more preferred by customers, because if he hasn't got a product in his shop in Graz. People probably know what they are looking for, if they want to buy such professional equipment. If he doesn't have it in his shop, people can just preorder it online and get the product in his shop. 
## 5
- 247 cameras
- 848 Lenses
- 357 camera bags
- 55 camera stands
He has a very in-depth catalogue of products. Especially cameras.
## 6
A speciality store. 
- in-depth catalogue of products
- mostly expensive
- small shop in town
## 7
The people working there are probably very familiar with all their products and can tell customers all the information they need. The shop probably has a good location in the city of Graz. The online shop is very well made with a nice looking website and pre-ordering products is also an option online which is very nice.
## 8
Camera shops aren't that common in cities in my opinion, means everyone can just find his product catalogue online which is very important. Mostly bigger stores sell cameras in the city anymore. For example, Hartlauer sells cameras, but they don't have such a depth in cameras and accessories unlike Foto Köberl. People also become lazier from time-to-time so they'll probably go to the online shop first to look for their needs.

<div style="page-break-after: always;"></div>


# K2.2
![[K2.2.png]]
## 1
An investment loan would be most appropriate, because the warehouse could be seen as an investment for the business.
## 2
It's part of the lending business, because the bank expects the business to pay them back.
## 3
- Financial statements
- Business plan
- Tax returns
- Cash flow projections
- Collateral details
## 4
The bank uses the investment object as a security measure. So if Bakes and More GmbH can't repay the loan, the bank will sell the investment object.
## 5
Because the bank doesn't want to loose money. They have to profit in some kind of way, otherwise this would be bad business for the bank.
## 6
### a)
No, there will be no compensation, because the person that caused the crash was the employee and was driving the vehicle. This means the cause of damage was because of a known person. If it was an unknown person or an animal, there would be compensation.
### b)
The bonus-malus system rewards drivers with lower bonuses if they don't crash for a specific period of time. Drivers who report damage have to pay higher bonuses.
<div style="page-break-after: always;"></div>



# K1.2 (BO4)
## 1.
Recruitment has to plan the Demand, recruit and select Personell. Personell deployment has to remunerate, judge, develope, and motivate Personell. Personell release has to dismantle Personell and manage staff departuers.
## 2.
The accountant should have expertise, personal expertise, social expertise and method expertise.

Examples:

| Expertise          | Example                                      |
| ------------------ | -------------------------------------------- |
| Expertise          | has knowledge with accounting software       |
| personal Expertise | is very stressresistent                      |
| social Expertise   | is good in teamwork                          |
| method Expertise   | is good in problem solving and data analysis |
## 3.
Magic Cards' requirements probably are full-time versus part-time, maybe specific skills, because of nice handwriting.
## 4.
Magic Cards can take measurements such as training the current employees to fill the gaps they need to fill. They can also outsource, means they temporarily hire personell from external sources. They can asjust the job roles or just hire new personell.
## 5.
Seasonal demands, means that businesses need more employees in, e.g. holidays. Businesses can hire or downsize due to economic changes. There can also be random changes due to exmployees resignations or retirements.
## 6.
First, the business promotes work offers and creates requirement profiles. Next, the accountant analyses work offers and creates application documents. Next, the business analyses the application and makes a pre-choice. Next, the business and the applicants go through a selection process. Then, the business makes a decision on the applicants. At last, the business and the applicant/s make an employment contract.
## 7.
Internal recruitment can make adaption easier, but external recruitment may bring in new ideas for the business, but theoretically, the internal recruitment can do this too.
## 8.
First, the business has to check his qualifications and experience. Then, the business does an interview with the applicant and evaluates, if he can fit in the role they need. Then, the business will contact previous employers of the applicant to make sure he is right in what he says. Then, the applicant gets the job offer, if he meets all the criterias.
## 9.
Bank transfer, Online payment platforms (Paypal, ...), Payroll services (Gusto, Paychex, ...)
## 10.
They can choose their worktime themselves and can make their work environment like they want, so they'll feel motivated.
## 11.
There may be a bias. Managers may make unfair ratings because of their personal bias. Unclear criterias can lead to appraisal errors, also inconsistent evaluations. Standards of the appraisers may vary, which make the comparison unfair.
## 12.
Staff may get the option to retire early, so the staff member count gets lower. Eliminating positions, which are unneccessary is also an option. Laid-off employees may get compensation for being laid off, which makes things easier for them.
<div style="page-break-after: always;"></div>

# K2.2 (BO4, S. 99)
![[Pasted image 20241002171918.png]]
## 1.
### a)
An entrepreneur has to:
- believe in the idea: If the entrepreneur wouldn't believe in his idea, it would be pretty useless.
- stress resistent: An entrepreneur is put under a lot of stress most of the time, so he/she has to be ready for that.
- ready for taking risks: An entrepreneur has to take alot of risk upon himself, when he creates a business.
- self-motivation: An entrepreneur has to be self-motivated enough to care for his employees, business, his work, etc...
- creativity: An entrepreneur has to be creative, so he gets nice and creative ideas for the business.

### b)
**The commercial law**.
Herzlein is probably going under free-trade, because she makes her products herself. That means, she is a craftsman, means Herzlein is a regulated business.

### c)
1) You have to be 18 years or older
2) You cannot have a criminal record
3) You must have citizenship in austria or a country that is a contracting state and you must have a residence authorisation
4) If you have a regulated business, you must have a certificate of competence
5) You may need a reliability check

## 2
### a)
The founder sets all planning steps for the realisation of the business idea. It really helps for founding the business, so no steps are going to be missed and the economic efficiency can be analyzed. Banks may need a business plan from a business to give them a loan.

### b)
1) The business idea
2) The business goals
3) The market orientation
	1) The market and the possible customer circle
	2) The competition
	3) The target group
	4) The product(s)
	5) The price(s)
	6) The distribution
	7) The communication and Ads
4) The employees, business organisation and partners
5) The financing

## 3
The USP of Herzlein is probably the own and unique designing of baby clothing. From the website it seems like the clothing is made out of high quality materials and also looks high quilty.
<div style="page-break-after: always;"></div>

# K5.2 (BO 4/5, S.185)
## 1.
It is a replacement investment, because the new loom is replacing an older or broken loom, means, that it's probably used for production.
## 2.
First, you have to plan the investment. You have to assert the need for the investment, define the criterias and take alternatives in consideration. Second is the investment decision. You have to evaluate the best offers and then you have to select the best alternative based on the criterias defined before. Third, you buy the product for the investment. Fourth, you have to check the investment if it meets the objectives after a period of time.
## 3.
### Cost comparison
You can make a decision on the loom with the lowest price.
### Profitability comparison
You can select the loom which gets you the highest return. Higher return = good.
### Amortisation periodo
You can choose the investment with the lowest amortiasation period. The amortisation period deteremines the time needed for the investment to make the money back that was invested in it.
## 4.
### a)

| Date           | Weighting | Glimakra Victoria | Score | Öxäbäck Lilla | Score |
| -------------- | --------- | ----------------- | ----- | ------------- | ----- |
| Price          | 40%       | 10                | 4     | 5             | 2     |
| Cash discount  | 5%        | 0                 | 0     | 2             | 0.1   |
| Delivery costs | 10%       | 6                 | 0.6   | 10            | 1     |
| Working width  | 20%       | 7                 | 1.4   | 6             | 1.2   |
| Bench included | 10%       | 0                 | 0     | 10            | 1     |
| Guarantee      | 15%       | 5                 | 0.75  | 3             | 0.45  |
| **Total**      | 100%      | 28                | 6,75  | 36            | 5,75  |
### b)
Flying Dreams should choose Glimakra Victoria, because it has a better score, but if Flying Dreams would look at it from the persective of getting more quality and extras and bonuses for the price, Flying Dreams would choose Öxäbäck Lillia.

## 5.
I think that Flying Dreams should choose Glimarka Victoria, because most of the costs like Maintenance costs per year are the same, but the price on Glimakra Victoria is the same, besides the delivery costs and the Cash discount. But those get overshadowed by the lower price in my opinion.

## 6.
The difference is, that static investment appraisals are used for short term investments as estimated calculations. Dynamic investment appraisals are used for short term and capital intensive investments to increase the significance.

## 7.

<div style="page-break-after: always;"></div>

# K4.2, 1-3 (BO 4/5)
## 1)
- Revenue: Planned sales: €1,600,000
- Expenditure:
	- Salaries: € 520,000
	- Purchase of goods: € 980,000
	- Rent: €20,000
	- Insurance: €5,000
	- Energy: €10,000
	- Loan repayments and interest: €\ŧext 142,000
- Total expenditure: € 1,677,000
- Result: $\text{Financial deficit} = \text{income} - \text{expenditure} =  \text{1,600,000€} - \text{1,677,000} = \text{-77,000€}$

Result: The company expects a deficit of 77,000€.
## 2)

The company is considering building a new warehouse for about €200,000, but does not want to take out a new loan. Based on the results from the previous financial year and the financial plan for the coming year, it would be difficult to finance this investment solely from internal funds as there is a deficit. Alternatives could include freeing up cash or optimising inventories.

## 3)

The company could use the following external financing instruments without taking out a new loan:
- Leasing: possibility of financing the warehouse through leasing, thus conserving cash.
- Factoring: Sale of receivables to obtain short-term liquidity.
- Equity capital: Raising equity capital from new investors, although this could require co-determination rights.
<div style="page-break-after: always;"></div>

## 4)
#### a) **Golden rule of financing**
$$\text{Long-term funds} \geq \text{Fixed assets} $$

- Fixed assets (Land, buildings, machines, IT system, vehicles): €2,040,000
- Long-term funds (Equity + Long-term loans): €670,000 + €750,000 = €1,420,000

**Calculation:**
Golden rule is **not satisfied** since €1,420,000 < €2,040,000.

---

#### b) **Equity ratio**
$$ \text{Equity ratio} = \frac{\text{Equity}}{\text{Total assets}} \times 100 $$

**Calculation:**
$$
\text{Equity ratio} = \frac{670,000}{2,800,000} \times 100 = 23.93\%
$$

---

#### c) **Debt-to-equity ratio**
$$\text{Debt-to-equity ratio} = \frac{\text{Total debt}}{\text{Equity}} $$

Total debt = Long-term loans + Short-term loans = €750,000 + €1,050,000 = €1,800,000

**Calculation:**
$$
\text{Debt-to-equity ratio} = \frac{1,800,000}{670,000} = 2.69
$$

---

#### d) **Cash flow in € and as % of operating period**
$$ 
\text{Cash flow} = \text{Profit} + \text{Depreciation} + \text{Interest paid} $$

Depreciation = €85,000

**Calculation:**
$$
\text{Cash flow} = 36,000 + 85,000 + 31,000 = 152,000 \, \text{€}
$$

$$
\text{Cash flow as \% of sales} = \frac{152,000}{1,490,000} \times 100 = 10.20\%
$$
---

#### e) **Debt repayment period (years)**
$$\text{Debt repayment period} = \frac{\text{Total debt}}{\text{Cash flow}} $$

**Calculation:**
$$
\text{Debt repayment period} = \frac{1,800,000}{152,000} = 11.84 \, \text{years}
$$
---

#### f) **Return on equity (ROE)**
$$$\text{ROE} = \frac{\text{Profit}}{\text{Equity}} \times 100$$
**Calculation:**
$$
\text{ROE} = \frac{36,000}{670,000} \times 100 = 5.37\%
$$
---

#### g) **Return on assets (ROA)**
$$\text{ROA} = \frac{\text{Profit} + \text{Interest paid}}{\text{Total assets}} \times 100$$

**Calculation:**
$$
\text{ROA} = \frac{36,000 + 31,000}{2,800,000} \times 100 = 2.39\%
$$

---

### Summary of Results:
- **Golden rule of financing:** Not satisfied.
- **Equity ratio:** 23.93%
- **Debt-to-equity ratio:** 2.69
- **Cash flow in €:** €152,000
- **Cash flow as %:** 10.20%
- **Debt repayment period:** 11.84 years
- **Return on equity (ROE):** 5.37%
- **Return on assets (ROA):** 2.39%