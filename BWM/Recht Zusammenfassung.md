# Rechtsordnung
## Wichtige Begriffe

![[Pasted image 20231129200518.png]]
  
**Moral:**
- Werte, die durch eigene Wertvorstellung vorgegeben werden
- bei Nichtbeachtung nicht bestraft

**Sitte:**
- in Gruppe/Gesellschaft anerkannte Verhaltensweisen
- Befolgung nicht erzwungen aber z.B. Nichtbeachtung Ausschluss aus Gruppe

**Gesetz:**
- von Staat festgelegt
- Strafe

**Rechtsnormen:**
- Vorschrift für Verhalten

**Rechtsordnung:**
- Gesamtheit der geltenden Rechtsnormen

**Rechtsquellen:**
- geschriebenen/ungeschriebenen Grundlagen der Rechtsordnung
- z.B. Verfassung, Gesetz

**Person/Rechtssubjekte:**
- Rechtsordnung zuerkennt
- Pflichten auferlegt

![[Pasted image 20231129201757.png]]

**Natürliche Personen:**
- Alle Menschen

**Juristische Personen:**
- künstliche Personen, können Verträge abschließen
- Besitz haben und in Rechtsstreitigkeiten auftreten
- benötigen Organe (Menschen) für rechtliche Handlungen
- z.B. Bund (Republik Österreich), Gemeinden

**Behörden:**
- Staatliche Organe für Verwaltungsaufgaben
- Befugt zur Anwendung von Zwang ("behördliche Befehls- und Zwangsgewalt" genannt)
- Legalitätsprinzip der österreichischen Bundesverfassung
- Handeln nur auf Grundlage von Gesetzen erlaubt
- z.B. Polizei kann betrunkenem Autofahrer den Schlüssel wegnehmen

**Parteien:**
- Direkt von Gerichts- oder Verwaltungsentscheidungen betroffene Personen
- Nicht verwechseln mit politischen Parteien

**Rechtsmittel:**
- Partei kann bei Uneinigkeit Rechtsmittel einlegen
- Rechtsmittel ermöglicht Anfechtung von Entscheidungen

**Instanzenzug:**
- Aufeinanderfolge der Entscheidungsebenen
- Rechtsmittel gegen Entscheidung der ersten Instanz möglich
- Nächste Instanz prüft und bestätigt/ändert Entscheidung in der Regel

![[Pasted image 20231129203334.png]]

**Rechtskräftig:**
- Entscheidungen, die nicht mehr angefochten werden können

**Judikatur:**
- Häufig auftretende Rechtsprobleme führen zur Entwicklung ständigen Rechtsprechung, der Judikatur


## Arten des Rechts

![[Pasted image 20231129203932.png]]

![[Pasted image 20231129203955.png]]

![[Pasted image 20231129204007.png]]

### Abgrenzung Zivil- und Strafrecht

**strafrechtliche Folgen:**
- Staat hat Recht, Kriminelle zu bestrafen

**zivilrechtliche Folgen:**
- Opfer hat Recht, vom Täter Ersatz für Schaden fordern

![[Pasted image 20231129204536.png]]
![[Pasted image 20231129204558.png]]

## Rechtsanwendung

![[Pasted image 20231129205234.png]]

![[Pasted image 20231129205305.png]]

**Sachverhalt:**
- Fall real ereignet, rechtliche Folgen bestimmen

**Tatbestand:**
- Gesetzesteil, der Fall einschließt

## Stufenbau der Rechtsordnung

![[Pasted image 20231129205944.png]]

![[Pasted image 20231129210011.png]]

**Generelle Rechtsnormen:**
- müssen kundgemacht, verlautbart, veröffentlicht werden → treten dann in Kraft

**Bescheide, Urteile und Beschlüsse:**
- werden zumindest  per Post an Parteien geschickt

# Aufgaben und Grundlagen des Verfassungsrechtes

## Verfassung
- wichtigsten Grundregeln

In den meisten Verfassungen zu finden
- die **Staatsform** (Republik oder Monarchie)
- wie der **Staat aufgebaut** ist (als Bundesstaat oder Zentralstaat)
- die **Staatsfunktionen** (Gesetzgebung, Verwaltung und Gerichtsbarkeit)
- die **Grundrechte** des einzelnen Menschen im Staat

## Staatszielbestimmung

![[Pasted image 20231129211125.png]]

## Grundprinzipien der Verfassung

![[Pasted image 20231129211308.png]]

## Demokratische Prinzip
- Volk kann mitbestimmen → Wahlen

![[Pasted image 20231129211617.png]]

## Republikanisches Prinzip
- Regierung für kurze zeit von Volk gewählt

![[Pasted image 20231129211738.png]]

## Bundesstaatliches Prinzip
- Staat entweder zentralistisch oder bundesstaatlich organisiert

![[Pasted image 20231129211832.png]]

## Rechtsstaatliches Prinzip
- jede Person soll Recht durchsetzen können

**Rechtsstaat im formellen Sinn:**
- Rechtsordnung wirksam durchgesetzt

**Rechtsstaat im materiellen Sinn:**
- Rechtsordnung soll Wertvorstellung entsprechen

![[Pasted image 20231129212526.png]]

**Legalitätsprinzip:**
- dann handeln, wenn sie ein Gesetz dazu ermächtigt, und
- so weit handeln, wie das Gesetz es ihnen vorgibt.
→ Gesetzesgeben soll Gesetze präziser formulieren

![[Pasted image 20231129212830.png]]

## Liberales Prinzip
- gewährleistet Freiraum → staatsfreien Raum

**Grund- und Freiheitsrechte:**
- das Recht auf Leben
- der Gleichheitsgrundsatz
- das Recht auf freie Meinungsäußerung
- das Recht auf Religionsfreiheit

## Gewaltentrennendes Prinzip

![[Pasted image 20231129213124.png]]



# Bürgerliches Recht

## Bedeutungen

![[Pasted image 20231129213313.png]]

![[Pasted image 20231129213324.png]]

## Rechtsquellen und Einteilung des Privatrechts

![[Pasted image 20231129213405.png]]

![[Pasted image 20231129213439.png]]

# Personenrecht

## Rechtsfähigkeit

![[Pasted image 20231129213542.png]]

**Rechtssubjekte:**
- natürliche, juristische Personen
- haben Rechte und Pflichen

**Rechtsobjekte:**
- Sachen
- haben keine Rechte und Pflichten


**Tod einer natürlichen Person:**
![[Pasted image 20231129213827.png]]

**Verschollenheit:**
![[Pasted image 20231129213930.png]]

Ende → **gerichtlicher Todeserklärungsbeschluss**
- Erbfolge
- Höchstpersönliche Rechte gelten als erloschen
- Ehe gilt als aufgelöst



### Verein Beispiel für juristische Person

**Juristische Personen:**
- juristische Personen des Privatrechts
- juristische Personen des öffentlichen Rechts

![[Pasted image 20231129214322.png]]

freiwilligen Zusammenschluss von mindestens zwei Personen
![[Pasted image 20231129214335.png]]

![[Pasted image 20231129214344.png]]

 **Verein:**
 - nicht auf Gewinn ausgerichtet
 - freiwillig auflösen 
 - durch Vereinsbehörde aufgelöst


## Handlungsfähigkeit

### Handlungsfähigkeit natürlicher Personen

![[Pasted image 20231129214833.png]]

#### Deliksfähigkeit

Personen ab vollendeten 14. Lebensjahr für handeln selbst verantwortlich

**Zivilrechtliche Deliktsfähigkeit:**
- Person muss Schaden ersetzen

**Strafrechtliche Deliktsfähigkeit:**
- ab 14 strafmündig
- Geld- und Freiheitsstrafen


#### Geschäftsfähigkeit

![[Pasted image 20231129215134.png]]

### Handlungsfähigkeit juristischer Personen
- durch ihre Organe handeln

- Verein haftet mit **Vereinsvermögen**
- von seinen Funktionären Schadenersatz verlangen
- Vereine strafrechtlich zur Verantwortung gezogen


### Vertretung für Minderjährige
- jeder Elternteil
- Pflegschaftsgericht
- Kinderbeistand

### Vertretung für Volljährige
- nur wenn Entscheidungsfähigkeit eingeschränkt

![[Pasted image 20231129215921.png]]

#### Vorsorgevollmacht
schon im Vorhinein eine Vertrauensperson

#### Gewählte Erwachsenenvertretung
Tragweite dieser Bevollmächtigung zumindest in Grundzügen versteht

#### Gesetzliche Erwachsenenvertretung
Vertretungsbefugnis nächster Angehöriger

#### Gerichtliche Erwachsenenvertretung
Bestellung erfolgt durch das Gericht




# Schadenserstzrecht

## Grundlangen
### Gründe für Schadensersatz
- Ausgleich für erlittene Schäden
- Anreiz zur Schadensvermeidung

### Entstehung von Schadensersatzansprüchen

![[Pasted image 20231129221628.png]]

### Beweislast
für Verschulden trifft Geschädigten

### Zivilrechtliche Deliktsfähigkeit
- Personen ab 14 selbst verantwortlich
- unter 14 Haftung der Erziehungsberechtigten

### Verhältnis Schadensersatz und Straftrecht
Sachverhalt:
- Schadensersatz
- gerichtliche Strafe


## Voraussetzungen für einen Schadenersatzanspruch

![[Pasted image 20231129222146.png]]

### Schaden

![[Pasted image 20231129222252.png]]
**ideeller Schaden:**
- nur in Ausnahmefällen erstattet

### Verursachung (Kausalität)
Person, die zurechenbar ist, hat Schaden verursacht durch:
- Handlung
- Unterlassung

### Verschulden

![[Pasted image 20231129222537.png]]

### Rechtswirdrigkeit
- Verstoß gegen ein Gesetz
- Verstoß gegen einen Vertrag

Rechtsfertigungsgründe:
- Notwehr
- Notstand


## Art und Umfang des Schadnesersatzes
- ursprünglicher Zustand des Schädigers wiederherzustellen
- wenn nicht möglich → Geldersatz

**Vermögensschäden:**
- leichter Fahrlässigkeit
- grober Fahrlässigkeit
- Vorsatz

**Personenschäden:**
- Heilungskosten
- Verdienstentgang
- Schmerzensgeld
- Verunstaltungsentschädigung
- Rente bei bleibenden Schäden
- Rente für Unterhaltungsberechtigte bei Tod des Unterhaltspflichtigen


### Schadensminderungspflicht
- Schadensverhinderung oder Schadensbegrenzung
- z.B. ärztliche Behandlung nach Verletzung

### Mitverschulden des Geschädigten
Schaden durch mehrere Personen vorsätzlich herbeigeführt, haften diese solidarisch

### Verjährung
innerhalb von 3 Jahren

![[Pasted image 20231129223755.png]]

## Haftung für fremdes Verschulden

![[Pasted image 20231129232806.png]]

### Haftung für Erfüllungsgehilfe
- Unternehmer haftet für Helfer bei Vertragsarbeit
- Kunden nicht benachteiligt bei Mitarbeitern/Subunternehmern
- Unternehmen verantwortlich für Fehler der Helfer
- Geschädigte Kunden können sich direkt ans Unternehmen wenden
- Beweislastumkehr wegen vertraglicher Haftung

### Haftung für Besorgungsgehilfen
- Unternehmer haftet für Schäden nur bei untüchtigen oder gefährlichen Gehilfen.
- Beweislast liegt bei der geschädigten Person.

### Haftung von Aufsichtspersonen
- Aufsichtspflichtige haften bei Verletzung ihrer Pflicht.
- Eltern, Lehrer, Pflegekräfte usw. haften, wenn die zumutbare Aufsicht vernachlässigt wird.
- Die Einschätzung der Zumutbarkeit hängt von Alter, Verhalten und Aktivitäten der Kinder ab.

### Haftung des Wohnungsinhabers
- Wohnungsinhaber haften für Schäden durch Herauswerfen/Herausgießen aus Wohnung

### Amtshaftung
- in Vollziehung der Gesetze


## Gefährdungshaften

![[Pasted image 20231129233720.png]]

### Kfz-Halterhaftung
- Strengere Kfz-Haftung aufgrund von Fahrzeuggefahren
- Hohe Schadenersatzzahlungen bei schweren Unfällen
- Pflicht zur Kfz-Haftpflichtversicherung schützt Halter und Opfer
- Fahrzeuglenker haften bei Verschulden zusätzlich zum Halter und seiner Versicherung

### Haftung der Wohnungs- und Gebäudeinhaber
- Besitzer/innen eines Gebäudes haften für Schäden durch Einsturz oder Ablösung

### Wegehalterhaftung
- Weghalter haften für Schäden durch mangelhaften Zustand nur bei grober Fahrlässigkeit
- Winter: Wege müssen ordnungsgemäß geräumt und gestreut sein

### Gastwirtehaftung
- Beherbergungsbetriebe, Badeanstalten und Garagen haften als Verwahrer für Gästesachen, die diesen übergeben oder anvertraut werden
- Haftungsobergrenze von € 1.100,-

### Produkthaftung
- Mängel von Produkten durch gesetzliche Gewährleistung und vertragliche Garantie geregelt
- Fehlerhafte Produkte können Schäden an Personen und Sachen verursachen
- Bei ausländischen Produkten oft schwierig, die Verantwortlichen zur Rechenschaft zu ziehen

- Hersteller umfasst den Produzenten, Markenanbringer und EWR-Importeur

- Haftung bei Sachschäden mit Selbstbehalt von 500 € für Konsumenten
- Personenschäden haben keine Haftungsgrenze
- Solidarische Haftung mehrerer Parteien mit möglichen Regressansprüchen

## Tierhaftung
Haftung bei Schäden durch **Tiere**:
- Verursacher: Person, die das Tier angetrieben, gereizt oder nachlässig verwahrt hat
- Tierhalter haftet, wenn keine angemessene Verwahrung oder Beaufsichtigung erfolgt ist

## Verkehrssicherungspflicht
- Verkehrssicherungspflicht: Allgemeine Pflicht, auf andere Rücksicht zu nehmen und Gefahrenquellen zu sichern
- Hauseigentümer können haften, z.B. bei gestürzten Zeitungszustellern
- Unternehmen müssen Gefahren für Kunden beseitigen oder warnen