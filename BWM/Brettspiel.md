---
Fach: BWM
Datum: Monday, 11.11.2024
Zeit: 08:47
Themen: Recht
Seiten: 262-266
---
---
# Frage
Wie funktioniert das trademark register? Was ist der "Österreichische Markenanzeiger"? Welche Möglichkeiten des Schutzes einer Marke gibt es EU-Weit bzw. weltweit?

# Antwort
Durch das Eintragen einer Marke in das trademark register ist das Aussehen und der Inhaber einer Marke bekannt. 

Es ist ein öffentliches Verzeichnis von angemeldeten und eingetragenen Marken. Es informiert über neue Markenanmeldungen, Änderungen und Schutzfristen.

Es wurden dafür internationale Abkommen abgeschlossen:
- **National**: Schutz im eigenen Land laut Vorschriften, blablabla
- **Innerhalb der EU:** "Unionsmarke" durch Anmeldung beim Amt der Europäischen Union für geistiges Eigentum (EUIPO)
- **International: **Aufgrund des "Madrider Markenabkommens" kann durch Hinterlegen der Marke bei der **Weltorganisation für geistiges Eigentum** (WIPO) in Genf die Marke geschützt werden. Gilt für 20 Jahre.