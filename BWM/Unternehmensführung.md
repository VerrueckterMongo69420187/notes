---
Fach: BWM
Datum: Monday, 04.11.2024
Zeit: 09:00
Themen: 
Seiten: "-207"
---
---
Hier sind die verkürzten Antworten:

### Ü 6.11
Führen heißt, Menschen zielgerichtet leiten und motivieren.

### Ü 6.12
Formelle Führung: offizielle Position.  
Informelle Führung: Einfluss ohne formale Rolle.

### Ü 6.13
a) Gruppe: Eigenschaftstheorien.  
b) Bestimmte Menschen sind geborene Führer.

### Ü 6.14
a) Eigenschaften einer guten Führungskraft.  
b) Nutzen ist nicht bewiesen.  
c) Vernachlässigt Erfahrung und Umfeld.

### Ü 6.15
a) Kontinuum-Modell: von autoritär bis laissez-faire.  
b) Autoritär: streng.  
c) Informierend: klare Kommunikation.  
d) Delegativ: Verantwortung übertragen.  
e) Kooperativ wird oft bevorzugt.

### Ü 6.16
a) Verhaltensgitter: Aufgaben- vs. Mitarbeiterorientierung.  
b) 5.9: Balance zwischen Teamarbeit und Effizienz.  
c) In 5.9: klare Kommunikation, wenig Kontrolle.

### Ü 6.17
Systemische Theorie: Führung als Gesamtsystem.
### Ü 6.21

|**Direkte Führungsinstrumente**|**Element des Führungsgrads**|
|---|---|
|Mitarbeitergespräche, Förderung/Ausbildung|Menschen fördern und entwickeln|
|Zielvereinbarungsgespräche|Ziele setzen und kontrollieren|
|Feedbackgespräche|Verhalten beeinflussen|
|Leistungsbeurteilung|Leistung bewerten|
|Coaching|Entwicklung unterstützen|
|Konfliktmanagement|Zusammenarbeit sichern|

| **Indirekte Führungsinstrumente** | **Element des Führungsgrads**     |
| --------------------------------- | --------------------------------- |
| Unternehmensrichtlinien           | Orientierung geben                |
| Arbeitszeitregelungen             | Strukturen und Prozesse gestalten |
| Betriebliches Vorschlagswesen     | Innovationskultur fördern         |
| Weiterbildungsprogramme           | Fachwissen und Kompetenz fördern  |
| Betriebsklima                     | Mitarbeiterzufriedenheit steigern |
| Kommunikationsplattformen         | Information und Austausch fördern |


### Ü 6.22 Allgemeine Führungsinstrumente
1. **Hauptzweck:** Führung von Mitarbeiter:innen, Sicherung der Unternehmensziele.
2. **Maßnahmen zur direkten Beeinflussung:** Zielvereinbarungen, Feedback-Gespräche, Mitarbeiterschulungen, Leistungsbeurteilungen, Prämien oder Boni.
3. **Maßnahmen zur indirekten Beeinflussung:** Arbeitsumfeldgestaltung, Unternehmensleitbild, Betriebsklima, Karrieremöglichkeiten, Teambuilding-Aktivitäten.

### Ü 6.23 Allgemeine Führungsinstrumente – Teamsport
Liste der allgemeinen Führungsinstrumente, z. B. Teambesprechungen, Feedback-Runden, Zielvereinbarungen, Rollenverteilung, Leistungsmessung. Trainerinnen werden aufgrund ihrer Leistung (z. B. gewonnenen Spiele, Spielerentwicklung) bewertet. Erfolgreiche Trainerinnen haben oft hohe Kommunikationsfähigkeiten, strategische Planung und Motivationsfähigkeiten.

### Ü 6.24 Management by Objectives
Der Ablauf des Mbo-Systems im Zeitablauf:
1. Zielsetzungsgespräch
2. Zielvereinbarung
3. Umsetzung und Unterstützung
4. Überwachung und Zwischenfeedback
5. Zielerreichungskontrolle und Bewertung
6. Folgegespräch zur Leistungsbeurteilung

### Ü 6.25 Management by Objectives – Vor-/Nachteile
1. **Vorteile für Mitarbeiterinnen:** Klarheit über Ziele, höhere Motivation, Eigenverantwortung, Förderung der Weiterentwicklung, Transparenz.
2. **Vorteile für das Unternehmen:** Verbesserung der Zielerreichung, bessere Leistungsmessung, strategische Planung, höhere Effizienz, Mitarbeiterbindung.
3. **Nachteile für Mitarbeiterinnen:** Leistungsdruck, geringe Flexibilität, mögliche Überforderung, Gefahr von Zielkonflikten, fehlende Kreativität.
4. **Nachteile für das Unternehmen:** Hoher Planungsaufwand, Abhängigkeit von Zieldefinitionen, Zeitaufwand für Überwachung, mögliche Konflikte bei Zielabweichungen, Bedarf an qualifizierten Führungskräften.

### Ü 6.26 Führungsgrad
1. **Grundsätze für wirksame Führung:** Zielorientierung, Klarheit, Konsequenz, Fairness, Unterstützung.
2. **Hauptaufgaben für wirksame Führung:** Planung, Organisation, Motivation, Kontrolle, Kommunikation.
3. **Werkzeuge für wirksame Führung:** Zielvereinbarungen, Feedback-Gespräche, Mitarbeiterschulungen, Teamentwicklung, Konfliktmanagement, Mitarbeiterbeurteilungen, Entwicklungspläne.

### K6.1

**1. Interpretation des Führungsstils des Firmengründers**
- Der Kollege beschreibt den Gründer als autoritären, zielorientierten Führer mit starker Hand. Dies deutet auf einen autokratischen Führungsstil hin, bei dem Entscheidungen alleine und schnell getroffen werden, ohne viel Mitspracherecht der Mitarbeitenden.

**2. Meinung zur patriarchalischen Führung**
- Der patriarchalische Führungsstil basiert auf Autorität und Fürsorge, wobei der Chef als "Vaterfigur" fungiert. Mitarbeitende werden in Entscheidungen wenig eingebunden, was zu Abhängigkeit und passivem Verhalten führen kann.

**3. Verhaltensgitter-Modell des Controlling-Chefs**
- Der Controlling-Chef zeigt eine starke Orientierung an Aufgaben, jedoch geringe Mitarbeiterorientierung, was auf ein eher "1,9" oder "9,1" Führungsstil im Managerial Grid hinweist.

**4. Vertriebsleiter und Führungsinstrumente**
- Die Führungsinstrumente des Vertriebsleiters umfassen Zielsetzung, Leistungsbeurteilung, Kategorisierung nach Fähigkeiten und Motivation sowie das Delegieren von Verantwortung.

**5. Systemische Führungstheorie**
- Daniela benötigt Informationen über das Unternehmen, die Rolle der Führungskräfte, interne Prozesse und den Einfluss der Systemtheorie auf die Unternehmenskultur, um die systemische Führungstheorie verstehen und interpretieren zu können.

**6. Probleme mit dem MbO-System**
- Wahrscheinliche Ursachen: fehlende Zieltransparenz, unrealistische Ziele oder mangelnde Partizipation. Vorschlag: Einführung klar definierter Ziele mit regelmäßigem Feedback zur Förderung der Mitarbeitereinbindung und zur Vermeidung von Missverständnissen.

**7. Meinung zur systemischen Führung von Malik**
- Maliks systemische Führung setzt auf das Verstehen komplexer Wechselwirkungen im Unternehmen. Daniela könnte antworten, dass sie dieses Modell als sinnvoll erachtet, da es flexibel ist und den Einfluss von Systemen auf die Führung berücksichtigt.