---
Fach: BWM
Datum: Tuesday, 08.10.2024
Zeit: 11:52
Themen: 
Seiten: 149ff
---
---
![[Pasted image 20241008115332.png]]
![[Pasted image 20241008115349.png]]
![[Pasted image 20241008115413.png]]
![[Pasted image 20241008115427.png]]
![[Pasted image 20241008115600.png]]
![[Pasted image 20241008115623.png]]
![[Pasted image 20241008115716.png]]
![[Pasted image 20241008115731.png]]
# K4.1
## Angabe

![[Pasted image 20241010080217.png]]
![[Pasted image 20241010080230.png]]
![[Pasted image 20241010080244.png]]
## 1)
- Business Angels
- Venture Capital
## 2)
Ausfallbürgschaft, da sie nur eine eingeht, wenn es gefordert wird von der Bank.
## 3)
### a)
Eine Bonitätsprüfung bewertet die Kreditwürdigkeit einer Person oder eines Unternehmens, um das Risiko von Zahlungsausfällen zu bestimmen.
### b)
![[Pasted image 20241010081957.png]]
## 4)
Nein, zu wenig Gewinn
## 5)
### a)
- Verkauf von Forderungen eines Unternehmens an einen Dritten (Factor) gegen sofortige Liquidität 
- Unternehmen nutzen Factoring um:
	- Liquidität zu verbessern
	- Ausfallrisiken zu minimieren
	- Verwaltungskosten zu senken.
### b)
![[Pasted image 20241010082635.png]] (ka)
### c)
Ja
## 6)
### Verschuldungsgrad
### Cashflow
61500
### Gesamtkapitalrentabilität
