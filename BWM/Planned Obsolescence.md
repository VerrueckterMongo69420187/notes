# Overview
* What is planned Obsolescence
* Historical Examples of Planned Obsolescence
* Types of Planned Obsolescence
* The Impact of Planned Obsolescence on the Environment
* The Ethical Considerations of Planned Obsolescence
* Possible Solutions to Planned Obsolescence
* Conclusion and Call to Action

# What is Planned Obsolescence
* Manufacturers design products to break down after certain amount of time 
* compels customers to replace products more frequently
* became common practice, maximize profits

# Historical Examples of Planned Obsolescence
## Light Bulb Conspiracy
* between 1925-1939, the Phoebus Cartel worked together to limit light bulb lifespan
* made sure to be frequently replaced
* one of the first examples

## Printer Ink
* Printers break down after certain amount of pages printed
* customers need new printers, not new Ink

## Software Updates
* Companies use Software updates to break down their products
* Examples: Making device slower, making important things not compatible anymore
* users have to buy new models
* Example: Batterygate (forces processor slowdowns or older battery to turn off under high load)

# Types of Planned Obsolescence
## Functional Obsolescence
* intentional design to wear, become outdated in specific timeframe
* Examples: Printer

## Style Obsolescence
* Manufacturers make frequent aesthetic changes
* product seems outdated
* need for replacement
* Example: Clothes and Accessoires

## Systemic Obsolescence
* forced upgrades because incompability issues
* Example: Software

# The Impact of Planned Obsolescence on the Environment
## Electronic Waste
* obsolescence => e-waste added => harm environment

## Natural Resource Depletion
* fast exploit of resources => harm ecosystem

## Carbon Footprint
* Manufacturing
* Transportation

# The Ethical Considerations of Planned Obsolescence
## Manipulating Customers
* it exploits customers psychology
* unneccessary purchases

## Environmental Impact
* profits > sustainability
* contributes to environmental degradation (points named before)

## Social Equality
* disadvantaged communities -> struggling financially (replacing products)

# Possible Solutions to Planned Obsolescence
## Extended Lifespan Design
* Encouraging the production of durable and repairable products

## Education and Awareness
- Informing consumers about the effects of planned obsolescence and promoting conscious purchasing practices.

## Policy and Legislation
- Companies must be held accountable for their actions
- sustainable manufacturing processes should be promoted

# Conclusion and Call to Action
Planned obsolescence presents significant challenges to the environment and consumers. It requires collective action, advocacy, and regulatory changes to promote sustainable and ethical practices in product design and manufacturing.