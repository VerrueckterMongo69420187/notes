---
Fach: BWM
Datum: Monday, 02.12.2024
Zeit: 08:43
Themen: alles glaub ich
Seiten: PDFs
---
# Bsp 1
## Aufgabe a) Quicktest und Gesamtbeurteilung

#### Kennzahlenberechnung:

1. **Eigenkapitalquote**:

$$Eigenkapitalquote= \frac{\text{Eigenkapital}}{\text{Gesamtkapital}} \times 100$$
Für 2024:
$$\text{Eigenkapitalquote} = \frac{50.700}{139.300} \times 100 \approx 36,4\%$$
2. **Anlagendeckungsgrad**:
$$Anlagendeckungsgrad=\frac{\text{Eigenkapital + langfristiges Fremdkapital}}{\text{Anlagevermögen}} \times 100$$Langfristige Verbindlichkeiten: $$Darlehen - kurzfristiger Anteil = 40.000−8.000=32.000$$$$40.000 - 8.000 = 32.000$$$$\text{Anlagendeckungsgrad} = \frac{50.700 + 32.000}{31.500} \times 100 \approx 264,4\%$$
3. **Liquidität 1. Grades**:
$$\text{Liquidität 1. Grades} = \frac{\text{Kassa + Bank}}{\text{kurzfristige Verbindlichkeiten}} \times 100$$
Kurzfristige Verbindlichkeiten: Lieferverbindlichkeiten + Darlehen (kurzfristig)
$$\text{Liquidität 1. Grades} = \frac{13.100}{44.900 + 8.000} \times 100 \approx 24,5$$
4. **Eigenkapitalrentabilität**:
$$\text{Eigenkapitalrentabilität} = \frac{\text{Jahresüberschuss}}{\text{Eigenkapital}} \times 100 $$$$\text{Eigenkapitalrentabilität} = \frac{19.500}{50.700} \times 100 \approx 38,5\%$$

#### Gesamtbeurteilung:

- Die **Eigenkapitalquote** ist akzeptabel (>30 %), zeigt eine solide Finanzierung.
- Der **Anlagendeckungsgrad** ist sehr gut (>100 %), das langfristige Kapital deckt das Anlagevermögen ausreichend.
- Die **Liquidität 1. Grades** ist kritisch (<50 %), deutet auf eine mögliche Unterdeckung hin.
- Die **Eigenkapitalrentabilität** ist ausgezeichnet (>10 %), was auf hohe Effizienz hinweist.

---

## Aufgabe b) Multiple Diskriminanzanalyse

Die Diskriminanzfunktion lautet:

$$Z=a1⋅K1+a2⋅K2+…+an⋅KnZ = a_1 \cdot K_1 + a_2 \cdot K_2 + \ldots + a_n \cdot K_n$$

- **Variablen (Kennzahlen):**
    - Eigenkapitalquote
    - Anlagendeckungsgrad
    - Liquidität 1. Grades
    - Eigenkapitalrentabilität
- Gewichtungsfaktoren $$a1,a2,…a_1, a_2, \ldots$$ gemäß Formelsammlung anwenden.

---

## Aufgabe c) Entscheidung zur Übernahme

#### Entscheidung:

Ja, die Übernahme erscheint sinnvoll aufgrund:

1. **Starke Eigenkapitalrentabilität (38,5 %).**
2. **Ausreichende Eigenkapitalquote (36,4 %).**
3. **Anlagendeckung (264,4 %) übertrifft deutlich die Norm.**

#### Einschränkung:

Die niedrige Liquidität 1. Grades (24,5 %) könnte kurzfristig problematisch sein. Verbesserungsmaßnahmen (z. B. Reduktion der kurzfristigen Verbindlichkeiten) sollten eingeplant werden.
# Bsp 2
## a) Dynamische Amortisationsrechnung

#### Berechnung der Barwerte:

Die Formel für den Barwert ist:

$$\text{Barwert} = \sum_{t=1}^{n} \frac{\text{Einsparung}}{(1 + i)^t} + \frac{\text{Restwert}}{(1 + i)^n} - \text{Anschaffungskosten}$$

**System A:**

- Anschaffungskosten: 60.000 €
- Restwert: 7.000 €
- Einsparungen: 10.000 € pro Jahr
- Nutzungsdauer: 8 Jahre
- Zinsfuß: 8 %

$$\text{Barwert Einsparungen} = \sum_{t=1}^{8} \frac{10.000}{(1 + 0,08)^t} =10.000⋅(1−(1+0,08)−80,08)≈62.355\text{€}= 10.000 \cdot \left(\frac{1 - (1 +0,08)^{-8}}{0,08}\right) \approx 62.355 \text{€}$$ $$\text{Barwert Restwert} = \frac{7.000}{(1 + 0,08)^8} \approx 3.747\text{€}$$$$\text{Barwert Gesamt} = 62.355 + 3.747 - 60.000 = 6.102 \text{€}$$

**System B:**

- Anschaffungskosten: 70.000 €
- Restwert: 3.000 €
- Einsparungen: 8.000 € pro Jahr
- Nutzungsdauer: 10 Jahre
- Zinsfuß: 8 %

$$\text{Barwert Einsparungen} = \sum_{t=1}^{10} \frac{8.000}{(1 + 0,08)^t} =8.000⋅(1−(1+0,08)−100,08)≈53.387\text{€}= 8.000 \cdot \left(\frac{1 - (1 + 0,08)^{-10}}{0,08}\right) \approx 53.387 \text{€} $$

$$\text{Barwert Restwert} = \frac{3.000}{(1 + 0,08)^{10}} \approx 1.389 \text{€}$$
$$\text{Barwert Gesamt} = 53.387 + 1.389 - 70.000 = -15.224 \text{€}$$

#### Ergebnis:

- **System A** hat einen positiven Barwert (6.102 €) und ist wirtschaftlicher als System B, das einen negativen Barwert (-15.224 €) hat.

## b) Methode mit variablem Zinsfuß

Die _Interne Zinsfuß-Methode_ (IRR) berechnet den Zinssatz, bei dem der Kapitalwert (Barwert) der Investition 0 ist:

$$0 = \sum_{t=1}^{n} \frac{\text{Einsparung}}{(1 + \text{IRR})^t} + \frac{\text{Restwert}}{(1 + \text{IRR})^n} - \text{Anschaffungskosten}$$

Dieser Zinssatz zeigt, wie rentabel eine Investition unabhängig vom vorgegebenen Zinsfuß ist.

---

## c) Kein Kosten-/Gewinnvergleich

Ein einfacher Vergleich der Kosten oder Gewinne ignoriert:

1. **Zeitwert des Geldes:** Künftige Einsparungen oder Restwerte haben bei heutiger Betrachtung weniger Wert.
2. **Langfristige Betrachtung:** Dynamische Methoden wie die Barwertmethode berücksichtigen die gesamte Laufzeit.
# Bsp 3
## a) Bedeutung der Umsatzsteuer im Kfz-Bereich

- **Bedeutung:** Unternehmen können die Umsatzsteuer (USt) für geschäftlich genutzte Fahrzeuge als Vorsteuer abziehen, falls der Bus ausschließlich betrieblich genutzt wird.
- **Unterschiede in der Verbuchung:**
    - **Anschaffung:** Vorsteuer wird aktiviert und später gegen Umsatzsteuer verrechnet.
    - **Folgerechnungen (z. B. Treibstoff):** Umsatzsteuer aus Betriebskosten kann ebenfalls als Vorsteuer geltend gemacht werden, solange sie betrieblich anfallen.

## b) Übersicht über USt-Sätze und Anwendung

- Aktuelle USt-Sätze in Österreich:
    - **20 %** (Standard, z. B. Fahrzeuge, Treibstoff)
    - **10 %** (z. B. Lebensmittel, Bücher)
    - **13 %** (z. B. Beherbergung, Kulturveranstaltungen)
- Anwendung:
    - Für den Kauf des Busses: 20 % USt.

## c) Grafische Darstellung von steuerbaren/nicht steuerbaren und steuerpflichtigen/steuerfreien Umsätzen: Eine Skizze könnte enthalten:

1. **Steuerbare Umsätze:** Alle Umsätze innerhalb des Steuerrechts (z. B. Verkauf von Waren/Dienstleistungen).
2. **Nicht steuerbare Umsätze:** Umsätze außerhalb des Steuerrechts (z. B. Schenkungen).
3. **Steuerpflichtige Umsätze:** Umsätze, bei denen USt anfällt.
4. **Steuerfreie Umsätze:** Umsätze ohne USt (z. B. Exporte, Heilbehandlungen).

## d) Abschreibungsmöglichkeiten in Österreich
![[Pasted image 20250115170445.png]]

## e) Abschreibungsberechnung und Wahl:**

- **Buskosten:** 33.000 € netto.
- **Skontoabzug:** 3 % von 33.000 € = 990 €, neuer Wert = 32.010 €.
- **Lineare Abschreibung:** $$\text{Jahresabschreibung} = \frac{32.010}{8} = 4.001,25\text{€}$$

## f) Verbuchung des Buskaufs mit Skonto:

1. **Kauf:**
    - Kfz (Anlagekonto): 33.000 €
    - Vorsteuer: 6.600 €
    - Verbindlichkeiten: 39.600 €
2. **Skontoabzug:**
    - Verbindlichkeiten: 990 €
    - Vorsteuerkorrektur: 198 €
    - Bank: 38.412 €
3. **Abschreibung:**
    - Abschreibungen: 4.001,25 €
    - Kfz (Anlagekonto): 4.001,25 €

**Auswirkungen des Skontoabzugs:**
- Der Anschaffungswert des Busses reduziert sich auf 32.010 €, was die Abschreibungsbasis senkt.

# Bsp 4
## a)
Erläutern der Rechte:
Kündigt der **Arbeitnehmer**, ist die Kündigungsfrist bis zum letzten Tag des Monats. Durch Arbeits-/Kollektivverträge kann die Frist verlängert werden.

**Auflösung der Dienstverhältnisse**:
![[Pasted image 20241212082506.png]]

**Kündigungsfristen**
![[Pasted image 20241205080318.png]]

Unterschied zwischen Kündigung und Entlassung:
- **Kündigung**: Beendigung durch Arbeitgeber oder Arbeitnehmer mit Frist.
- **Entlassung**: Fristlose Beendigung durch Arbeitgeber aus wichtigem Grund.

22.08. frühester Kündigungstermin ist der 30.10

## b)
Der Betriebsrat muss Betriebsvereinbarungen machen. Er ist so gesehen die Vertretung gegenüber den Mitarbeitern. Der Betriebsrat kann ab einer Mitgliederanzahl von 151 Mitarbeitern kann er ein Mitglied zur Gänze von der Arbeit freistellen.

## c)
![[Pasted image 20241209085907.png]]
**Gehaltsabrechnung**
$$
Gehalt: 2972.94
$$
$$
\text{ÜG: }2972.94:167 = 23.79 * 24=570.96
$$
$$
\text{ÜZ steuerfrei: } 11.90 * 18 = 214.20 \text{, steuerfrei höchstens 200€} 
$$
$$
\text{ÜZ steuerpflichtig: } 11.90 * 6 = 71.40 + (214.20 - 200) = 57.20
$$
$$
\text{SV lfd.: }2972.94*18.07\% = 537.21
$$
$$
\text{LSt lfd.: } 2972.94-537.21-17.40-16.58 = 2401.75
$$

$$
\text{Gesamtbruttogehalt: } 2972.94+570.96+200+57.20 = 3801.11
$$
$$
\text{SV lfd.: } 3801.11 * 18.07\% = 686.86
$$
$$
\text{LSt lfd.: } 3801.11-200-686.86-17.40-16.58 = 2880.27 \times 30\% = 864.08 - 428.96 = 435.12
$$$$
\text{Gewerkschaftsbeitrag: } 16.58
$$
$$
\text{Auszahlung: } 3801.11-686.86-435.12-16.58=2662.55
$$

## d1)
Für Angestellte und Arbeiter gelten verschiedene Gesetze. 
![[Pasted image 20241212075734.png]]

**Arbeiter**:
- entrichten handwerkliche Arbeiten, hochqualifizierte oder Hilfsdienste
- für sie gelten noch die Gewerbeordnung aus 1859 und das Arbeitgeber
- moderne gesetzliche Grundlagen fehlen im Kollektivvertrag
- mit vertraglicher Vereinbarung kann auch das Angestelltengesetz für sie gelten (Vertragsangestellten)

**Angestellter**:
- für sie gilt das Angestelltengesetz aus 1921
- Angestellte sind z.B. welche die:
	- kaufmännische Tätigkeiten verrichten
	- höhere, nicht kaufmännische Tätigkeiten verrichten
	- Kanzlerarbeiten verrichten
- Angestellte müssen eine bestimmte Entscheidungsbefugnis haben

## d2) (PV/4)
Das Unternehmen muss für den Angestellten zusätzlich zum Bruttogehalt folgende **Dienstgeberabgaben** leisten:

### Sozialversicherungsbeiträge (DG-Anteil)

- **Prozentsatz:** ~21,23 % des Bruttogehalts.
- Enthalten sind Beiträge zur Krankenversicherung (3,78 %), Pensionsversicherung (12,55 %), Arbeitslosenversicherung (3,00 %), und Unfallversicherung (1,38 %).

#### **2. Kommunalsteuer:**

- **Prozentsatz:** 3 % der Bruttolohnsumme.

#### **3. Dienstgeberbeitrag (DB):**

- **Prozentsatz:** 3,9 % der Bruttolohnsumme.

#### **4. Zuschlag zum Dienstgeberbeitrag (DZ):**

- **Prozentsatz:** ~0,4 bis 0,5 % (je nach Bundesland).

### Verbuchung der Dienstgeberabgaben
**Sozialversicherungsbeiträge**:
6560 Sozialversicherungsaufwand an 3600 Verbindlichkeiten Sozialversicherung

**Kommunalsteuer**:
6660 Kommunalsteuer an 3540 Verbindlichkeiten Kommunalsteuer

**Dienstgeberbeitrag**:
6661 Dienstgeberbeitrag an 3541 Verbindlichkeiten DB

**Zuschlag Dienstgeberbeitrag**:
6662 Zuschlag DB an 3542 Verbindlichkeiten DZ





# Bsp 5 (BWM3/120)


# Bsp 6
## a) (BO2/28)
![[Pasted image 20250114113841.png]]

## b)

## c)

# Bsp 7
## a)
### Events
- Jubiläumsevents mit z.b. Musik
- Weinverkostung
- Spezielle Angebote wie Rabatte
### Social Media
- gezielte Werbung
- teilen von historischen Meilensteinen des Hotels
- z.b. Hashtags erstellen dafür
### Direktmarketing
- Personalisierte Einladungen an Stammkunden versenden
- Newsletter
### Kooperationen
- Zusammenarbeit mit regionalen Produzenten
- Hervorhebung von lokalen Produkten
- Partnerschaften mit Reiseveranstalter
### Kundeneinbindung
- Wettbewerb, bei dem Kunden ihre schönsten Momente im Hotel teilen können
- Gewinnspiele

## b) (BO3/163)


![[Pasted image 20250114120424.png]]
### Erhebungsmix
Ist eine Kombination aus objektiven und nicht-objektiven Techniken. Die Techniken müssen vor der Ist-Aufnahme festgelegt werden, da Nacherhebung Zeit kostet.

**ABC-Analyse** kann das wesentliche gut vom unwesentlichen bei Daten trennen und die Daten klassifizieren.sdaf

## c)
![[Pasted image 20250115163036.png]]
