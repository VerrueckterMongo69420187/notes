
## K2.10)
![[K2.10.png]]
### a)

Strafrecht: wird von Staat verhängt

Zivilrecht: Schadensersatz wird zwischen Privatpersonen geltend gemacht.

7200€ gehen an den Staat

### b)

können erfüllt sein

### c)

Der verletzte Disco-Besucher kann Schadenersatz für medizinische Kosten, Verdienstausfall und Schmerzensgeld verlangen.

### d)

3 Jahre


---

## K2.11)

![[K2.11.png]]

### a)

Die vier Voraussetzungen für einen Schadenersatzanspruch könnten vorliegen: Rechtswidrigkeit (mögliche Pflichtverletzung des Pistenbetreibers), schuldhaftes Verhalten, kausaler Zusammenhang und entstandener Schaden.

### b)

Die verletzte Snowboarderin kann Schadenersatz für medizinische Kosten (Operationen, Krankenhausaufenthalt, Physiotherapie), Schmerzensgeld, Einkommensverlust und mögliche weitere Unfall-bedingte Ausgaben verlangen.

---

## K2.12)

![[K2.12.png]]


### Antwort

Deine Freundin, als Halterin des Fahrzeugs, müsste für beide Schäden haften – sowohl für das beschädigte fremde Auto als auch für den Schaden an ihrem eigenen PKW.

---

## K2.13)

![[K2.13.png]]

### Antwort

Der Radfahrer kann Ansprüche auf Produkthaftung gegen den Hersteller des Mountainbikes geltend machen, da die Gabel des Fahrrads gebrochen ist und zu einer schweren Verletzung führte. Es besteht auch die Möglichkeit einer Haftung des Sportgeschäfts, in dem das Fahrrad gekauft wurde, wenn ein Vertriebsfehler vorliegt.

---

## K6.1)

![[K6.1.png]]

### a)

Strafrecht, weil Gericht

### b)

Bezirksgericht, Schaden < 15000€

### c)

Anwaltspflicht, da Schaden > 5000€

### d)

Anspruch auf Verfahrenshilfe


## Ü5.2
| ja  | nein |
| :-: | :--: |
|     |  x   |
|     |  x   |
|     |  x   |
|     |  x   |
|  x  |      |
|     |  x   |
|     |  x   |
|  x  |      |
|     |  x   |
|  x  |      |

## Ü5.4
### a)
| freie Gewerbe                                                                                                                | reglementierte Gewerbe                                                                                                                                                                                                         |
| ---------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Personen, die ein derartiges Gewerbe<br>ausüben möchten, müssen keine Befähigung zur Ausübung dieser Tätigkeiten nachweisen. | Personen, die ein derartiges Gewerbe<br>ausüben möchten, müssen ihre Befähigung<br>nachweisen. D.h., sie müssen Kenntnisse,<br>Fähigkeiten und Erfahrungen besitzen,<br>die für die Ausübung des Gewerbes<br>benötigt twerden. |

### b)
Alle Gewerbe, die nicht in § 94 der Gewerbeordnung stehen, sind freie Gewerbe.


## Ü5.4
|        Tätigkeit         | freies Gewerbe | reglementiertes Gewerbe |
| :----------------------: | :------------: | :---------------------: |
|     Rauchfangkehrer      |                |      x (Handwerk)       |
|    Vermögensberatung     |                |            x            |
| Reparatur von Fahrrädern |       x        |                         |
| Herstellung von Schuhen  |                |      x (Handwerk)       |
|         Bäckerin         |                |      x (Handwerk)       |
|   Sicherheitsfachkraft   |                |            x            |

## Ü5.5

| Ja  | Nein |             Begründung             |
| :-: | :--: | :--------------------------------: |
|     |  x   |              nicht 18              |
|     |  x   |          Unbescholtenheit          |
|  x  |      | kein österreichischer Staatsbürger |
|  x  |      |              Konkurs               |
|     |  x   |     keine Aufenthaltserlaubnis     |
|  x  |      |                                    |

## Ü5.6

| Vorraussetzungen               | Begründung                                                                 |
| ------------------------------ | -------------------------------------------------------------------------- |
| Befähigungsnachweis benötigt   | solange er nur mit Patent produziert                                       |
| kein Befähigungsnachweis nötig | kein Patent, reglementiertes Gewerbe **aber fällt unter Industriebetrieb** |
| Befähigungsnachweis nötig      | kein Patent, reglementiertes Gewerbe, kein Industriebetrieb                |

## Ü5.13

| richtig | falsch | Richtigstellung |
| ------- | ------ | --------------- |
|         | x      |                 |
|         | x      |                 |
| x       |        |                 |
|         | x      |                 |
|         | x      |                 |
|         | x      |                 |
| x       |        |                 |
|         | x      |                 |

## Ü5.14
![[5.14.png]]
![[Pasted image 20240405081734.png]]

## Ü5.15
![[5.15.png]]
Er darf es ausüben, da er theoretisch Schlosser oder so ist

## Ü5.16
![[5.16.png]]
### a)
![[Pasted image 20240405081309.png]]

### b)
Tischlergewerbe

### c)
nicht direkt, man braucht Nebenrechte ab Tischler, Installateur usw

### d)
Ja, gehört zum Nebenrecht


## K5.1
![[K5.1_1.png]]
![[K5.1_2.png]]
### a)
![[Pasted image 20240405082351.png]]

### b)
- Ich muss 18 sein
- keine Straftaten begangen haben
- Staatsbürger sein

### c)
Mit der Anmeldung

### d)
nein

### e)
Magistrat

### f)
Ausbilderprüfung muss man machen

### g)
?


## K5.2
### a)
Industriebetrieb

### b)
Nein, braucht er nicht

### c)
Er muss es melden

### d)
wenn es keine Hauptaktivität ist

### e)
wenn seine Anlage niemanden belästigt


## K5.2
### a)
- Staatsbürgerschaft
- Meisterprüfung
- 18

### b)
Ja, Bestellung eines gewerblichen Geschäftsführers, der über den Befähigungsnachweis verfügt
### c)
braucht man

### d)
Ab der Anmeldung

### e)
Rechtsmittel ergreifen

### f)
ja