---
Fach: BWM
Datum: Tuesday, 03.09.2024
Zeit: 11:45
Themen: 
Seiten: 187-
---
---
![[aktiven Rechnungsabgrenzungsposten.png]]
![[Ü6.1.png]]
![[Beziehung-Vermögen-Kapital.png]]
![[Anlagendeckung.png]]
![[Lagerumschlagskennzahl.png]]
![[Durchschnittslager.png]]
![[Debitorenumschlagskennzahl.png]]
![[Durchschnittliche Lieferforderungen.png]]
![[Kreditorenumschlagskennzahl.png]]
![[Durchschnittliche Lieververbindlichkeiten.png]]
![[Kapitalumschlagshäufigkeit.png]]
![[Durchschnittliches Gesamtkapital.png]]
![[Ü6.4.png]]
a) 
- Lagerumschlagshäufigkeit: 4,57
- Lagerumschlagsdauer: 80
b)
- Debitorenumschlagshäufigkeit: 6,03
- Debitorenumschlagsdauer: 60,57
c)
- Kreditorenumschlagshäufigkeit: 12,56
- Kreditorenumschlagsdauer: 29,06
d)
- Kapitalumschlagshäufigkeit: 1,95

![[Ü6.5.png]]![[Ü6.5 Angabe 1.png]]![[Ü6.5 Angabe 2.png]]
# a)
## Kennzahlen der Aktivseite der Bilanz
- Anteil des Anlagevermögens
	- 2022: 21,48%
	- 2023: 17,85%
- Anteil des Umlaufvermögens
	- 2022: 78,52%
	- 2023: 82,15%
## Kennzahlen der Passivseite der Bilanz
- Anteil des Eigenkapitals
	- 2022: 49,3%
	- 2023: 48,48%
- Anteil des Fremdkapitals
	- 2022: 50,7%
	- 2023: 51,5%
- Fiktive Schuldentilgungsdauer
	- 2022: 1,094 Jahre
	- 2023: 1,205 Jahre
## Beziehungszahl zwischen Vermögen und Kapital
- Anlagendeckung
	- 2022: 308,2%
	- 2023: 350,94%
## Kennzahl des Lagerumschlages
- Lagerumschlagshäufigkeit
	- 2022: 5,39 mal
	- 2023: 5,856 mal
- Lagerumschlagsdauer
	- 2022: 61,55 Tage
	- 2023: 62,329 Tage
## Kennzahlen des Debitorenumschlags
- Debitorenumschlagshäufigkeit
	- 2022: 14,56 mal
	- 2023: 16,29 mal
- Debitorenumschlagsdauer
	- 2022: 25,06 Tage
	- 2023: 22,41 Tage
## Kennzahlen des Kreditorenumschlags
- Kreditorenumschlagshäufigkeit
	- 2022: 9,57 mal
	- 2023: 10,47 mal
- Kreditorenumschlagsdauer
	- 2022: 38 Tage
	- 2023: 34,84 Tage
## Kennzahlen des Kapitalumschlags
- Kapitalumschlagshäufigkeit
	- 2022: 3,49 mal
	- 2023: 3,86 mal