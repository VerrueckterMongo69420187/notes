---
Fach: POS
Datum: Tuesday, 27.02.2024
Zeit: 08:54
Themen: 
Seiten:
---
---
# MQTT
- Downloaden von MQTT: [Hi](https://mqttx.app/downloads)
- Installieren von mosquitto
```cmd
sudo snap install mosquitto
```

- **Schickt eine Message in htlstp/4BHIF/pinz**
```cmd
 mosquitto_pub -h mqtt.htl.services -u htlstp -P nopass2day! -t htlstp/4BHIF/pinz -m "Seas"
```

- **Subscribed auf den Channel htlstp/4BHIF/pinz**
```cmd
mosquitto_sub -h mqtt.htl.services -u htlstp -P nopass2day! -t htlstp/4BHIF/pinz
```

- **Subscriben auf die Sensordaten aus unserer Klasse**
```cmd
 mosquitto_sub -h mqtt.htl.services -p 1883 -u htlstp -P nopass2day! -t sensorDa
ta/htl/316/#
```

**AUF SENSORDATA NICHTS PUBLISHEN!!!!! SONST ZUGRIFFSVERWEIGERUNG!!!!**

# MQTT in Unity
1) Clonen von MQTT in den Assets-Ordner, Projekt daraufhin umstrukturieren (weil sonst alles fucked ist)
```cmd
git clone https://github.com/CE-SDV-Unity/M2MqttUnity.git
```

2) Installieren von ProBuilder im Packet Manager in Unity (Window -> Packet Manager)
3) Installieren von Progrids (Window -> Packet Manager -> Add package by name -> com.unity.progrids)
4) Edit -> Project Settings -> Package Manager -> Enable Pre-release Packages aktivieren
5) Assets installieren im [Unity Asset Store](https://assetstore.unity.com)
6) Tools -> ProGrids/ProBuilder um Tool zu verwenden
7) Materials alle auf Standard setzen.