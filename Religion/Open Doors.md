---
Fach: Religion
Datum: Wednesday, 12.02.2025
Zeit: 10:00
Themen: Religionsverfolgung
Seiten:
---
---
Open Doors veröffentlicht jährlich ein Ranking an Ländern anhand der Extremität der Religionsverfolgung.

Nordkorea ist seit 23 Jahren auf Platz 1 und Somalia auf Platz 2.

Angriff auf christliche Häuser ist in einem Jahr um 33% gestiegen.

In Kirgisistan ist die Rate sehr stark angestiegen wegen der Autorität.

Es finden Razzien in Gottesdiensten statt.

In Sudan findet die größte Vertreibungskrise der Welt statt.