---
Fach: Religion
Datum: Wednesday, 09.10.2024
Zeit: 09:52
Themen: https://www.prosieben.at/serien/galileo/news/kastensystem-in-indien-so-funktioniert-die-strenge-rangordnung-344823
Seiten: spurensuche 1969
---
--- 
- Das Kastensystem basiert auf religiösen, hinduistischen Texten.
- Die Gesellschaft wird in Brahmanen, Kshatriyas, Vaishyas und Shudras eingeteilt, wobei Dalits außerhalb der Hierarchie stehen.
- Obwohl die Diskriminierung gesetzlich abgeschafft wurde, besteht sie auf dem Land weiterhin.
- Soziale Mobilität ist besonders durch Heirats- und Interaktionsregeln eingeschränkt.
- Bildung und Urbanisierung stellen das traditionelle Kastensystem zunehmend in Frage.