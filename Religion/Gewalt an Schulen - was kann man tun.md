---
Fach: Religion
Datum: Wednesday, 24.01.2024
Zeit: 10:46
Themen: Gewalt an Schulen, Was man tun kann
Seiten: schulranzen.at
---
Gewalt an Schulen, sei es physisch oder psychisch, beeinträchtigt viele Kinder und Eltern. Die Ursachen für Gewalt sind vielfältig, von Aggressionsabbau bis hin zu Aufmerksamkeitserlangung. Es ist wichtig, den individuellen Einzelfall zu betrachten. Gewalt kann sich durch Verleumdungen, Beschimpfungen, Ausschließungen, Drohungen und physische Übergriffe manifestieren.

Gewaltprävention ist entscheidend, um Gewalt zu verhindern. Workshops, Gesprächsrunden und Zusammenarbeit mit externen Stellen können helfen. Zusätzlich sind Gespräche mit gewalttätigen Schülern, stärkende Aktivitäten für die Klassengemeinschaft und spezielle Betreuung für aggressionsgefährdete Kinder wichtige Maßnahmen.

Bei akuten Gewaltvorfällen sollten Eltern den offiziellen Weg über Lehrer, Schulleitung und Schulpsychologie wählen, anstatt direkt mit den Eltern des Täters zu kommunizieren. Die Kommunikation zwischen Eltern und Kind ist entscheidend, und im Idealfall sollte eine gemeinsame Anstrengung von Opfern, Eltern und Schule unternommen werden, um einen sicheren Schulalltag zu gewährleisten. Gegebenenfalls kann auch psychologische Unterstützung für Opfer notwendig sein.