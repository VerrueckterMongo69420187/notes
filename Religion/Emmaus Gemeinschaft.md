---
Fach: Religion
Datum: Wednesday, 05.06.2024
Zeit: 09:55
Themen: 
Seiten:
---
---
**Gründung:**
Die Gemeinschaft wurde 1949 von Abbé Pierre, einem katholischen Priester, in Frankreich gegründet.

**Ziele:**
Unterstützung und Wiedereingliederung von Menschen in schwierigen Lebenssituationen.
Förderung der sozialen Gerechtigkeit und Bekämpfung von Armut und Ausgrenzung.

**Arbeitsweise**:
Die Gemeinschaft arbeitet nach dem Prinzip der gegenseitigen Hilfe. Betroffene, so genannte "Gefährten", arbeiten gemeinsam in Projekten wie Second-Hand-Läden, Recycling-Projekten und Werkstätten.
Die Einnahmen aus diesen Aktivitäten finanzieren die Gemeinschaft und soziale Projekte.

**Internationale Präsenz**:
Emmaus ist in über 40 Ländern vertreten und hat weltweit mehr als 350 Gruppen.

**Grundsätze und Werte:**
Zentrale Werte sind Solidarität, Gemeinschaft, Respekt und soziale Gerechtigkeit.
Die Gemeinschaft ist unabhängig von politischer, religiöser und ethnischer Zugehörigkeit.

**Aktivitäten und Projekte:**
Neben Second-Hand-Läden und Werkstätten engagiert sich Emmaus in Bildungsprojekten, in der Flüchtlingshilfe, im sozialen Wohnbau und in der Advocacy-Arbeit gegen Armut und soziale Ungerechtigkeit.

**Finanzierung:**
Die Gemeinschaft finanziert sich durch Einnahmen aus eigenen Aktivitäten und Spenden, ohne staatliche Mittel, um ihre Unabhängigkeit zu wahren.

**Bekanntes Wirken:**
Bekannt wurde Abbé Pierre durch seinen Einsatz für Obdachlose in den 1950er Jahren, insbesondere während des harten Winters 1954 in Paris, der nationale Aufmerksamkeit auf die Notlage der Obdachlosen lenkte.