---
Fach: Religion
Datum: Wednesday, 17.04.2024
Zeit: 10:43
Themen: 
Seiten:
---
---
Der Prophet Jona ist eine wichtige Figur im Tanach und im Koran. Seine Geschichte steht im Buch Jona im Tanach. Im Christentum, Judentum und Islam wird er verehrt.

Jona wird von Gott beauftragt, die Stadt Ninive zu warnen. Jona will aber nicht und flieht. Auf dem Schiff kommt es zu einem Sturm. Die Seeleute werfen Jona über Bord, um Gott zu besänftigen. Er wird von einem Fisch verschluckt. Drei Tage und Nächte bleibt er im Bauch des Fisches. Dann wird er von Gott wieder freigelassen.

Nach seiner Befreiung folgt Jona Gottes Befehl und predigt in Ninive. Er sagt den Bewohnern, dass sie sich von ihren Sünden abwenden müssen, sonst wird die Stadt zerstört. Die Menschen hören auf Jona und kehren zu Gott zurück. Deshalb zerstört Gott die Stadt nicht.

Die Geschichte von Jona zeigt uns, dass wir Gott gehorchen sollen. Wir sollen uns auch von unseren Sünden abwenden. Gott ist barmherzig und vergibt uns.