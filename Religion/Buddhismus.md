---
Fach: Religion
Datum: Wednesday, 25.09.2024
Zeit: 10:20
Themen: Buddhismus
Seiten:
---
---
Buddhisten haben keinen Gottesglauben.

Das Nirvana ist sozusagen das "höchste Ziel" für einen Buddhisten.

Dalai Lama ist das Oberhaupt

Es gibt die vier edlen Wahrheiten:
- Leiden existiert
- Die Ursache des Leidens ist Begierde
- Beendigung des Leidens ist möglich
- Weg zu Beendigung ist der Achtfache Pfad

Buddhisten glauben an Karma, heißt, wenn man etwas gutes tut, kommt gutes zurück, welches auch das nächste Leben beeinflusst.

**JAPAN:**
- kam im 6. Jahrhundert
- ist mittlerweile oft mit Shinto vermischt, wird z.B. bei wichtigen Beerdigungen praktiziert
- entwickelte Schulen wie Tendai und Nichiren
- Shingon-Schule betont esoterische Rituale und Mantras
- Mönche tragen traditionelle Roben
- Es gab einen Stock namens "Keisaku", mit dem Meditierende geschlagen werden, um die Achtsamkeit zu stärken.