**Name: Adrian Pinz
Klasse: 4BHIF**

In Seven geht es um zwei Detektive namens David Millis (Brad Pitt) und William Somerset (Morgan Freeman), die einem Serienmörder finden wollen. Doch es ist kein gewöhnlicher Serienmörder, denn er hat eine bestimmte Art des Tötens. Er sucht sich seine Opfer nicht per Zufall aus. Er bringt Menschen um, die eine der 7 Todsünden begehen, indem er sie mit ihrer Sünde Umbringt.

Die 7 Sünden lauten: 
* Maßlosigkeit (Gluttony)
* Habsucht (Greed)
* Trägheit (Sloth)
* Wollust (Lust)
* Hochmut (Pride)
* Neid (Envy)
* Zorn (Wrath)

Genau in dieser Reihenfolge wurden die Menschen auch vom Serienmörder umgebracht

## Maßlosigkeit (Gluttony):
Mit Maßlosigkeit versteht man, dass eine Person nicht mehr konsumieren soll als was sie wirklich braucht. Das hat man im Film sehr gut gesehen. Ein sehr übergewichtiger Mann ohne Namen wurde vom Serienmörder so sehr gefüttert mit Essen in einem Hinterzimmer, bis er an seinem Übergewicht starb und mit seinem Kopf in einen Teller Spaghetti flog.
	
	![[gluttony.webp]]

## Habsucht (Greed)
Mit Habsucht versteht man übersteigertes Streben nach materiellem Besitz, unabhängig von dessen Nutzen. Dies hat man am zweiten Mord des Serienkillers gesehen. Eli Gould, ein Verteidiger vor Gericht, musste von sich selbst einen Pfund Fleisch von seinem Körper runterschneiden. Der Serienkiller hat auch eine Nachricht mit einem Zitat von Napoleon hinterlassen, das lautet: *“One pound of flesh no more no less no cartilage no bone but onlyflesh”*
Das heißt, dass er genau einen Pfund Fleisch wollte, ohne Knochen, ohne irgendwas anderem, nur pures Fleisch.

![[greed.png]]

## Trägheit (Sloth)
Trägheit ist sehr selbsterklärend. Trägheit ist einfach sehr faul sein und nichts machen. Der Serienmörder hat sich etwas sehr kreatives mit seinem Opfer namens Victor einfallen lassen, und zwar hat er ihn an sein Bett gefesselt, seine linke Hand abgeschnitten, damit der Mörder beim vorherigen Mord Fingerabdrücke als nächsten Hinweis für die Detektive hinterlassen konnte. Victor war ein Jahr lang an sein Bett gefesselt und war eigentlich schon (fast) Tot, als ihn das SWAT-Team gefunden hat. 
Doch er hat noch gelebt, indem der Mörder ihn mit Steroiden gefüttert hat, obwohl sein Körper schon an dem Zeitpunkt am Verrotten war. Der Killer hat außerdem das ganze Jahr lang pünktlich die Miete von Victor gezahlt. Victor war außerdem in Drogen, bewaffnetem Raubzug und Vergewaltigung einer Minderjährigen im Gefängnis.

![[sloth.jpg]]

## Wollust (Lust)
Wollust ist eine sinnliche, sexuelle Begierde und Lust, die bei sexueller Aktivität, der Befriedigung oder bei sexuellen Phantasien erlebt wird. Deswegen ging der Serienmörder mit einem Fremden in einen Puff zu einer Prostituierten. Er zwingt den Fremden mit einer Pistole im Mund, einen Strap-On anzuziehen, der statt einem Dildo ein Messer dranmontiert hat, mit der Prostituierten Sex zu haben. Diese starb natürlich dran. Der Fremde war traumatisiert, aber starb nicht.

![[lust.webp]]

## Hochmut (Pride)
Unter Hochmut versteht man unter anderem Arroganz, oder z.B. wenn man sich zu sehr um seine eigene Schönheit kümmert. Genau das spielte eine Rolle beim nächsten Opfer. Im Film *The Beautiful Woman* genannt, hatte vom Serienmörder zweit Entscheidungen bekommen.	
1. Telefon
	Eine Hand von ihr wurde vom Mörder an ein Telefon geklebt, mit dem sie nach Hilfe rufen konnte.
2. Schlaftabletten
	Ihre andere Hand wurde vom Serienmörder an eine Packung Schlaftabletten geklebt, mit denen sie sich schließlich selbst umbringen würde.
	
Am Ende hat sich die Frau für das Telefon entschieden und der Mörder hat ihre Nase und ihr Gesicht aufgeschnitten, an dem sie am Ende starb.

![[pride.webp]] 

## Neid (Envy)
Unter Neid versteht man, wenn man einer anderen Person etwas nicht gönnt. Das war auch die Schlussfolgerung des Films, meiner Meinung nach eine der spannendsten jemals. Der Serienmörder, der an dem Zeitpunkt als John Doe identifiziert wurde, hat Detektiv Millis und Somerset mitten ins Nirgendwo geführt. Dort kam ein Postbote zufällig an, der ein Paket an Millis bringen soll. Somerset öffnete es und sah den Kopf der Frau von Detektiv Millis drinnen. Millis war mit Zorn gefüllt, wie er es herausfand und hat am Ende John Doe erschossen, da er neidisch war, dass er damit davonkommen konnte. 

![[envy.jpg]]
## Zorn (Wrath)
Zorn heißt, dass man wütend ist. Im Endeffekt hat der Zorn in Detektiv Millis die Überhand gewonnen, indem er John Doe erschoss. Doch er hat einen unbewaffneten Mann erschossen. Das heißt das er ins Gefängnis kommen würde. Egal was er gemacht hätte, John Doe hätte gewonnen. Hätte er ihn nicht erschossen, hätte er für immer Neid gegenüber John Doe. 

![[wrath.jpeg]]

## Fazit
Im Film Seven werden die 7 Todsünden in einer sehr grausamen Weise bildlich dargestellt, indem ein Serienmörder eben seine Opfer mit der begangenen Sünde ermordet bzw. zurückzahlt. 

## Religiöse Zusammenhänge
Der größte religiöse Zusammenhang zu Se7en sind natürlich die sieben Todsünden. Der Film basiert auch auf göttliche Gerechtigkeit und Buße. John Doe hat sah sich selbst als Werkzeug Gottes, der die Gesellschaft von sündige Individuen und "moralisch verdorbenen Menschen" bereinigen sollte.