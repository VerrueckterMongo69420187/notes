---
Fach: Religion
Datum: Wednesday, 17.01.2024
Zeit: 09:49
Themen: 
Seiten:
---
---
* Medienberichte mehr über Gewalt
* Falscher Eindruck, dass es mehr Gewalt gibt
* Gewalt ist halt spannender
* Gesichtsverlust wurde früher oft missbraucht
* Menschen lassen sich leicht verführen, auch in der Wissenschaft
* Empathie
* U-Bahn ist gefährlich (für Frauen)
* Wenn man sich die Informationen über eine andere Person durchliest, mit der man ein Experiment macht, steigt die Empathie
* Karl der Große setzte Christentum um mit Gewalt
* Institutionen verschlimmern Gewalt, sollten eigentlich weniger Gewalt machen