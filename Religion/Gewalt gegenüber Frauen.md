---
Fach: Religion
Datum: Wednesday, 06.03.2024
Zeit: 10:37
Themen: 
Seiten:
---
---
# Atmosphäre
- creepy
- alt
# Gefühle
- Rache
- Angst
# Umgebung
- weiß ich gerade nicht
# Folgen der Gewalt
- Straftatenbegehung
- Gewalt
# Ende
- moralisch vertretbar
- wäre ich eine Frau in so einer Situation, würde ich genau so handeln (Frauenpower)
# Mittel gegen den Anstieg an Frauengewalt in Österreich
- Mehr Schulung darüber
# Bewertung
**3/5**
