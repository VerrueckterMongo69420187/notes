---
Fach: Religion
Datum: Wednesday, 22.05.2024
Zeit: 10:55
Themen: 
Seiten:
---
---
- Spielt in den 1960er Jahren während der Bürgerrechtsbewegung in den USA, basierend auf der Ermordung von drei Bürgerrechtlern in Mississippi 1964.
- Thematisiert tief verwurzelten Rassismus und brutale Gewalt gegen Afroamerikaner in den Südstaaten.
- Beleuchtet die Rolle des FBI bei der Untersuchung von Bürgerrechtsverletzungen und zeigt die Herausforderungen in einem rassistischen Umfeld auf.
- Stellt ethische Fragen über die Methoden der Strafverfolgung und die Komplexität der Justiz.
- Kritisiert die Marginalisierung der Rolle der Afroamerikaner im Bürgerrechtskampf und die Förderung eines weißen Retter-Narrativs.
- Intensive und emotionale Filmsprache, unverblümte Darstellung von Gewalt und Rassismus, um den Zuschauer mit der Realität der Zeit zu konfrontieren.
- Verhältnismäßig geringe Bestrafung für die Rassisten 