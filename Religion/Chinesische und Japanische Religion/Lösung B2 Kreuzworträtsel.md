---
Fach: Chinesische und Japanische Religion
Datum: Wednesday, 23.10.2024
Zeit: 10:25
Themen: 
Seiten:
---
---
**Folgende Aufgaben sind zu lösen bzw. folgende Wörter sind im Kreuzworträtsel versteckt:**

1. **Welches Wort beschreibt den Vorgang der Versenkung und Konzentration?** → ZEN
2. **Wo hat Shinto seinen Ursprung?** → JAPAN
3. **Worauf baut der Shintoismus auf?** → NATURRELIGION
4. **Wo fand die erste Weltkonferenz der Religionen 1970 statt?** → KYOTO
5. **Warum müssen im Konfuzianismus alte Riten befolgt werden?** → ORDNUNG DER WELT
6. **Wie lautet der Fachbegriff für das Sitzen im Zen?** → ZAZEN
7. **Welche Tugend braucht man, um sich auf das Wu wei einzulassen?** → VERTRAUEN
8. **Wie nennt man das Verschmelzen von Elementen unterschiedlicher Religionen miteinander?** → SYNKRETISMUS
9. **Was sieht Konfuzius als positive Folge?** → EINGREIFEN
10. **Wie nennen die Daoisten die Idee des aktiven Nichthandelns?** → WU WEI
11. **Wie heißt die älteste Form des Shintoismus?** → JINJASHINTO
12. **Wo verbrachte Lassalle den größten Teil seines Lebens?** → JAPAN
13. **Was gibt es nach Kong Fuzi nicht?** → JENSEITS
14. **Was wird im Werk Kojiki geschildert?** → ENTSTEHUNG JAPANS
15. **Wie werden die Tage geistlicher Sammlung genannt?** → SESSHINS
16. **Wie bezeichnet man einen Verehrungswürdigen, geistlichen Lehrer oder Meister in Japan?** → ROSHI
17. **Wie hieß Hugo Lassalle mit zweitem Vornamen?** → ENOMIYA
18. **Welcher Begriff bedeutet Weg oder Methode?** → DAO
19. **Wie heißt der Gründer des Daoismus?** → LAOZI
20. **Welche Religion gründet Kong Fuzi?** → KONFUZIANISMUS
21. **Wie nennt man in Japan zentrale Stätten der Verehrung?** → JINJA
22. **Wer schützt laut Kong Fuzi die Familie?** → AHNEN
23. **Was bedeutet Shinto?** → WEG DER GOETTER
24. **Welchem Orden gehörte Lassalle an?** → JESUITEN
25. **Wie heißt das älteste überlieferte japanische Werk aus dem Jahr 712?** → KOJIKI
26. **Was sind Yin und Yang?** → UNIVERSELLE KRAEFTE