---
Fach: GGP
Datum: Wednesday, 13.12.2023
Zeit: 08:23
Themen: Wohlstandsgefälle Österreich
Seiten: 21, 22, 23
---
---
Osten war sovietisch besetzt, hinter eisernem Vorhang.

![[Nutzebene.png]]

NUTS 1 => BL Gruppen
* W-Ö => VOR, T; SLB, OÖ
* O-Ö => NÖ, W, BGL
* S-Ö => ST, K

NUTS 2 =>BL

NUTS 3 => Bezirksgruppen
* NÖ-Mitte => P, PL
* NÖ-N => ME, KR, KS, ZW, HO, GU, WT

