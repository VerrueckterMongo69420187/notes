## Tabelle für das Jahr 2023

|  Land/Zone   | Budgetdefizit | Staatsverschuldung |
| :----------: | :-----------: | :----------------: |
| Griechenland |     +1,3%     |       160,2%       |
|   Italien    |     -4,5%     |       140,4%       |
|   Portugal   |     -0,1%     |       160,2%       |
|    Irland    |     +1,7%     |       40,4%        |
|   Belgien    |      -5%      |        160%        |
|  Euro-Zone   |     -3,2%     |       90,8%        |
| Deutschland  |     -2,3%     |       65,2%        |
|  Österreich  |     -2,4%     |       75,4%        |
|   Estland    |     -3,1%     |       19,5%        |

**PIIGS -> € Rettungsschirm**  
* **P**ortugal
* **I**talien
* **I**rland
* **G**riechenland
* **S**panien
