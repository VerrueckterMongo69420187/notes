---
Fach: GGP
Datum: Wednesday, 28.02.2024
Zeit: 08:22
Themen: 
Seiten:
---
---
# Norwegen
 - reich
 - lange Küste
 - lange Grenze zu Schweden und Finnland
 - kurze Grenze zu Russland
 - Parlamentarische und konstitutionelle Erbmonarchie
 - 5,4 Mio. Einwohner (+0,6%/a)
 - BIP/Kopf 70825 USD (3. Platz weltweit - ca. doppelt so hoch wie Ö.)
 - HDI 0,961 (4. Platz weltweit)
 - Gini Koeffizient 0,258 (Ö=0,26)
 - Gründungsmitglied der NATO
 - EU Abstimmung 1972 u. 1994 abgelehnt
# Schweiz
# Island