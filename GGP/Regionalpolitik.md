---
Fach: GGP
Datum: Wednesday, 20.12.2023
Zeit: 08:06
Themen: Wohlstandsgefälle Österreich
Seiten: "22"
---
---
# Kofinanzierung
* Finanzierung der EU für Projekte in schlechteren Ländern
* Zahlen aber nicht alles, Grund: es kommen vielleicht auf einmal mega viele Ideen für Projekte usw.
* Mehrfinanzierung -> Es werden nicht 100% in ein Projekt gesteckt -> falls es schief geht, auf andere ausweichen

# Arbeitsaufgaben Wohlstandsgefälle Österreich
1) **Reiche Regionen EU:** Paris, Stockholm, Südirland, Bayern, Bratislava, Hamburg Umgebung

	**Arme Regionen EU:** West- und Südrumänien, Süditalien + Sizilien, Bulgarien, äußere Regionen Polens, Griechenland

	**Reiche Regionen Österreich:** Wien, Salzburg, Oberösterreich, Tirol, Vorarlberg

	**Arme Regionen Österreich:** Burgenland, Niederösterreich (Waldviertel)

	**Arme Regionen Deutschlands:** Sachsen, Thüringen, Mecklenburg-Vorpommern, Sachsen-Anhalt, Saarland

	**Reiche Regionen Italiens:** Norditalien

2) **Kaufkraft Wien-Bratislava-Prag**
	**Kaufkraft Wien:** 46.000
	**Kaufkraft Bratislava:** 35.800
	**Kaufkraft Prag:** 20.292

	**Fazit:** Wien ist bei weitem Stärker, wobei Prag weniger als die Hälfte von Wien hat und Bratislava liegt zwischen Wien und Prag.

3) Die drei **ärmsten** Regionen in der EU sind alle in Bulgarien zwischen 10.300 und 11.100. Die drei **reichsten** Regionen der EU sind: Luxemburg, Brüssel und Hamburg.

4) Die reicheren Regionen sind natürlich in erster Linie die, wo viele leben. Aber es gibt ein paar wichtige Punkte:
	* Geografische Lage
	* Historische Entwicklung
	* Investitionen und Infrastruktur
	* Bildung und Fachkräfte
	* Wirtschaftliche Struktur

5) Salzburg war schon immer ein sehr reiches Bundesland, es hat auch einen großen Tourismussektor. Vorarlberger gehen die meisten in der Schweiz arbeiten. Burgenland hat keinen so guten Arbeitsmarkt. Ich nehme einmal an, dass viele pendeln werden in größere Ortschaften für ihre Jobs. 

6) 
    6.1) **Osten-Westen**: 
	* Osten: War hauptsächlich unter sovietischer Kontrolle, sieht man am besten an Ost-West-Deutschland. Nach Zerfall des Ostblocks haben viele Länder die Richtung Marktwirtschaft eingeschlagen, doch der Schritt war Komplex, und hat nicht in jedem Land so hingehauen wie geplant.
	* Westen: viele wichtige Handelsregionen und Städte befanden sich im Westen, historische Gründe sind ebenfalls am mitspielen.
	6.2) **Süden-Zentrum**: 
	* Süden: Mittelmeerregion war immer eher Ärmer
	* Zentrum: Viele Unternehmen usw. haben sich in den Zentralen Regionen niedergelassen.
	