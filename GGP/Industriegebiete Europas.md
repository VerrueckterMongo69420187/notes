---
Fach: GGP
Datum: Wednesday, 15.05.2024
Zeit: 07:59
Themen: 
Seiten: 55, 56
---
---
# 1

| Kriterien                 | Alte Industriegebiete                                                                | Neue Industriegebiete                                                                                                                                                                         |
| ------------------------- | ------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Wichtige Standortfaktoren | Nähe zu Rohstoffquellen (Steinkohle, Eisenerz), Anbindung an das Eisenbahnnetz       | Intakte Umwelt, Positives Image,  Kultur- und Freizeitmöglichkeiten,  Angenehmes Klima,  Verkehrsinfrastruktur (Autobahnnetze, Flughäfen), Nähe zu Bildungseinrichtungen und Forschungslabors |
| Betriebsstruktur          | Großbetriebe mit stark hierarchischer Struktur und innerbetrieblicher Arbeitsteilung | Dynamische Klein- und Mittelbetriebe mit flexiblerer Struktur                                                                                                                                 |
| Dominante Branchen        | Schwerindustrie (Stahl, Kohle), Textilindustrie                                      | Hightech (Mikroelektronik, Informationstechnologie, Biotechnologie)                                                                                                                           |
# 2 
**Alte Industrieregionen:**
- Herausforderungen durch globalen Wettbewerb
- Notwendigkeit zur Neupositionierung und Modernisierung
- Kampf um Wettbewerbsfähigkeit und Marktanteile

**Neue Industrieregionen:**
- Flexiblere Strukturen ermöglichen Anpassung an Globalisierung
- Integration in globale Wertschöpfungsketten
- Herausforderung, wettbewerbsfähig und innovativ zu bleiben

# 3

# Präsentation
- allgemeiner Steckbrief der Regionen
- Ursprüngliche Wirtschaftsstruktur
- aktuelle Wirtschaftsstruktur
- Gründe für die wirtschaftliche Transformation
# B - Ruhrgebiet
## Steckbrief
- von Dortmund bis Duisburg (ca. 60km)
- 16 Stadtkreise zu einer Industrie/Stadtlandschaft zusammengewachsen
- ca. 5.5 Millionen €

## Ursprüngliche Wirtschaftsstruktur
- Stahl/Kohle/Koksindustrie

## Aktuelle Wirtschaftsstruktur
- Hochtechnologiesektor soll weiter gefördert werden
- Zusammenarbeit mit Unis wird vorangetrieben
- aktuelle Großunternehmen ->
	- ThyssenKrupp
	- RAG
	- Degussa
	- E.ON
	- Opel (zeit 1962 3 Werke)
	- Aldi (Sitz)

## Gründe für die wirtschaftliche Transformation
 - Beginn d. Kohlekrise 1950er -> 
	- anhaltende Phase des Strukturwandels
	- wirtschaftliche Anpassungsschwierigkeiten
- seit 1980 ->
	- \> 1 Mio. Arbeitsplätze verloren
	- Dienstleistungssektor konnte ca. 300k Arbeitsplätze schaffen