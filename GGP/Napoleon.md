Weltkarte 1815 (2.Teil) - Prinzipien
* Legitimität
* Restitution
* Solidarität -> **Heilige Allianz**

1. **Europäisches Gleichgewicht (Pentarchie):** Garantie für die Balance der Mächte in Europa nach den napoleonischen Kriegen.
    
2. **Heilige Allianz:** Monarchen von Österreich, Preußen und Russland gründeten sie, um das Verhalten der Staaten basierend auf der christlichen Religion zu regulieren und Interventionen gegen mögliche revolutionäre Bewegungen zu ermöglichen.
    
3. **Kongresssystem:** Eine Vereinbarung zwischen Staatsmännern zur Beilegung gemeinsamer Sicherheitsinteressen, funktionierte jedoch nur sieben Jahre lang aufgrund unterschiedlicher Großmachtinteressen.
    
4. **Neue Verhandlungstechnik:** Alle beteiligten Staatsmänner waren Vertreter des alten aristokratischen Regimes vor 1789. Verhandlungen konzentrierten sich ausschließlich auf europäische Angelegenheiten, und das besiegte Frankreich wurde in die neu geschaffene Ordnung integriert.

# Vormärz (Biedemaier)
* 1815 - 1848 -> "Märzrevolution"
* Polizeistaat
* Verbot von freien Medien/Vereine/Meinungen
* Kaiser Ferdinand 1. -> Absolutismus 

**"Träger" der Märzrevolution 1848\**
* Liberalismus -> Verfassung -> Menschenrechte -> Wahlrecht -> Absetzung des Kaisers + Metternich
* Nationalismus
* Sozialismus

Parlament der Monarchie (1848 - 1918)
-> Reichsrat (gewählt)
-> Herrenhaus (bestimmt von Kaiser)

Dualismus
->
->
->