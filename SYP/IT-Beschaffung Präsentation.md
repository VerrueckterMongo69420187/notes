---
marp: true
theme: default
paginate: true
title: IT-Ausschreibung: Dokumentation
author: Adrian Pinz
date: 2024-11-27
---

# IT-Ausschreibung: Dokumentation

## Übersicht
- **Ziel:** Beschaffung von 100 Notebooks inkl. 3 Jahre 3rd-Level-Support
- **Rahmenbedingungen:**
  - Auftragswert: 150.000–200.000 €
  - Verfahren: Nicht-offenes Verfahren
  - Ausschluss elektronischer Auktionen
- **Vorgaben:**  
  Bundesvergabegesetz (BVergG 2018)

---

# Inhaltsübersicht

1. Verfahrensauswahl  
2. Prozessdokumentation  
3. Notwendige Dokumente  
4. Zusammenfassung und Zeitleiste  

---

# Verfahrensauswahl

- **Gewähltes Verfahren:** Nicht-offenes Verfahren mit öffentlicher Bekanntmachung  
- **Begründung:**
  - Auftragswert im Oberschwellenbereich
  - Standardisierte Produkte (Notebooks)
  - Fokus auf qualifizierte Anbieter  
  - Elektronische Auktionen ausgeschlossen

---

# Prozessdokumentation

## **Schritte:**
1. **Vorbereitung:** Klärung der Anforderungen, Erstellung der Ausschreibungsunterlagen
2. **Bekanntmachung:** Veröffentlichung und Einladung von Anbietern
3. **Angebotseinholung:** Angebote fristgerecht einholen
4. **Angebotsprüfung:** Bewertung nach Preis und Qualität
5. **Vergabeentscheidung:** Bestbieter auswählen und Zuschlag erteilen
--- 
## **Zeitleiste:**
- Ausschreibung starten: November 2024  
- Angebotsfrist: mindestens 20 Tage  
- Zuschlagserteilung: Dezember 2024  
- Lieferung: Januar 2025

---

# Notwendige Dokumente

## **Erforderliche Unterlagen (Word-Dokumente):**
1. **VG-5 Aufforderung zur Angebotsabgabe**
   - Einladung für Unternehmen, ein Angebot abzugeben.

2. **VG-16a Eingangsverzeichnis**
   - Dokumentiert eingegangene Angebote.

3. **VG-16 Niederschrift der Angebotsöffnung**
   - Protokolliert die Angebotsöffnung.
--- 

4. **VG-7 Zuschlagsentscheidung (positiv)**
   - Für den erfolgreichen Bieter.

5. **VG-8 Mitteilung der Zuschlagsentscheidung**
   - Für die nicht erfolgreichen Bieter.

6. **VG-9 Zuschlagserteilung**
   - Abschluss des Verfahrens.

7. **VG-19 Information über die Erhebung personenbezogener Daten**
   - Dient zur Einhaltung der Datenschutzgrundverordnung (DSGVO). 
   - Enthält Hinweise zur Verarbeitung personenbezogener Daten der Bieter.
---

# Zusammenfassung

## **Schlüsselpunkte:**
- Verfahren: Nicht-offenes Verfahren
- Fokus: Wirtschaftlich günstigstes Angebot
- Klare Zeitplanung und Fristen
- Dokumentation gemäß BVergG

## **Nächste Schritte:**
1. Abschluss der Dokumentation und Weiterleitung an CIO  
2. Fertigstellung des Verfahrens bis Ende Dezember 2024  

---
# Fragen?  

Vielen Dank!