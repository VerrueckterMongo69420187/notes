---
Fach: SYP
Datum: Thursday, 25.01.2024
Zeit: 12:49
Themen: 
Seiten:
---
---
# Planungsarten
* Aufgabenplanung (PSP, Arbeitspakete, Funktionendiagramm)
* Zeitplanung
* Ressourcenplanung
* Kostenplanung
* Projektdreieck

# Aufgabenplanung
## PSP
- steht für Projektstrukturplanung
- Gliederung von Aufgaben in Arbeitspakete 
- Grundlange für andere Planungsarten
- Kommunikationsinstrument im Projekt (z.B. bei Zielvereinbarung)

## Arbeitspakete
- Spezifizierung von Arbeitszielen, Arbeitsereignissen und Arbeitsinhalten

## Funktionendiagramm
- man sieht die Festlegung von verantwortlichen PTMs von Arbeitspaketen
- Jede identifizierte Aufgabe wird klar definiert, inklusive ihrer Ziele, Umfang, Verantwortlichkeiten und Ressourcenanforderungen.

# Zeitplanung
- Meilensteinplanung
- Balkendiagramm
- Terminlistung
- Netzplan

## Meilensteinplanung
Beispiele: Projektstart gestartet, Systemarchitektur entworfen und genehmigt, Entwicklungsphase abgeschlossen, usw.

Erklärung: Die Meilensteinplanung im Projektmanagement 
- beinhaltet die Definition klarer, überprüfbarer Punkte im Projektablauf, die wichtige Phasen oder Ziele markieren.
- Diese Meilensteine dienen zur Überwachung des Fortschritts
- ermöglichen klare Kommunikation und erleichtern Anpassungen, um sicherzustellen, dass das Projekt erfolgreich verläuft.

## Terminlistung + Balkendiagramm
- Es werden wichtige Termine definiert 
- Balkendiagramm stellt es grafisch dar 
- Dauer der Aktivitäten wird geschätzt 
- Ressourcen werden für jede Aktivität festgelegt (Arbeitskräfte, Budget, Materialien)

## Netzplan
- CPM (Critical Path Method)
- längster Weg
- bestimmt Dauer von Projekt
- erleichtert Anpassung zur Erfüllung der Termingerechtigkeit

# Ressourcenplanung
- Festlegung Über/Unterdeckung Ressourcen
- Ressourcen wie Mitarbeiter, Budget, Zeit und Materialien effizient für die Durchführung von Projektaktivitäten geplant, zugewiesen und verwaltet werden

# Kostenplanung
- muss Gliederung von PSP entsprechen
- Kostenarten: fix/variabel
- variabel: ändern sich direkt mit Produktions-/Verkaufsmenge
- fix: bleiben gleich

# Projektdreieck
- **Aufwand-Zeit:**
    - Mehr Aufwand kann mehr Zeit erfordern.
    - Knapper Zeitrahmen kann den erforderlichen Aufwand erhöhen.
- **Aufwand-Leistung:**
    - Höherer Aufwand kann zu höherer Leistung führen.
    - Effiziente Ressourcennutzung beeinflusst die Leistung.
- **Zeit-Leistung:**
    - Kürzerer Zeitrahmen kann die Qualität der Leistung beeinträchtigen.
    - Weniger Zeit für gründliche Planung und Entwicklung.