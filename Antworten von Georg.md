---
Fach: Diplomarbeit
Datum: Friday, 15.11.2024
Zeit: 14:15
Themen: Reasoning
Seiten:
---
---
Retail Price == Wiederverkaufspreis -> soll sich nicht dauernd geändert werden

Der Einkaufspreis soll regelmäßig updaten, Verkaufspreis nicht

Rechnung in Odoo:
- Automatisch durchlaufen (z.b. pro Monat)
- Manuell

Menge abgleichen bei Rechnung erstellen

viel handle_automatic_invoice wahrscheinlich erweitern

sale_order_line benötigt

qty_to_invoice könnte dann von ingram kommen

können an bestehenden funktionen entwickeln, macht man in odoo normalerweise auch, danach drüberschauen

mapping machen, welches ingram produkt in odoo was ist

z.B. Ms 365 soll z.B. automatisch als Lieferant Ingram haben bei Kauf -> Produktcode hinterlegen -> Preis abändern

Integration in die Aufträge hinein

Bei Lieferant kann man eine Integrationszuweisung machen -> laut Georg schöne, coole Lösung, mit Vererbung

brauchen OAuth client in Odoo um Anbindungen zu machen, Lieferanten erweitern, um Preise von den Produkten zu bekommen. Wenn Subscriptions angelegt wurden in odoo, Preise holen.

Menge täglich anpassen bei Aufträgen, Ingram soll vllcht odoo bescheit sagen, dass es Änderungen gibt (nicht im scope)

Button zum synchronisieren, um aktuelle Menge von Ingram zu holen.

Wenn Aktion da ist, periodisch ändern

Nachrichten werden automatisch aktualisiert, bei manchen macht es keinen Sinn, bei den Meisten schon. Wenn man z.B. Zahlungsbedingung ändert, wird es nicht aktualisiert.

depends-Reihenfolge in manifest.py ist wichtig wegen nachladen der Funktionen

möglich an sales_amazon zu orientieren

aus amazon_account.py kann man viel übernehmen

wenn es geht zweisprachig, Felder in englisch schreiben im Code

